<?php
$this->breadcrumbs = array(
	'Loket Returs',
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' LoketRetur', 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' LoketRetur', 'url' => array('admin')),
);
?>

<h1>Loket Returs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 