<?php
$this->breadcrumbs = array(
	'Loket Returs' => array('index'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'List') . ' LoketRetur', 'url' => array('index')),
	array('label'=>Yii::t('app', 'Manage') . ' LoketRetur', 'url' => array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'Create'); ?> LoketRetur</h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model,
		'buttons' => 'create'));
?>