<?php
$this->breadcrumbs = array(
	'Loket Returs' => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' LoketRetur', 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' LoketRetur', 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' LoketRetur', 'url'=>array('update', 'id' => $model->loket_retur_id)),
	array('label'=>Yii::t('app', 'Delete') . ' LoketRetur', 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->loket_retur_id), 'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>Yii::t('app', 'Manage') . ' LoketRetur', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View'); ?> LoketRetur #<?php echo GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'loket_id',
'note_',
'total',
'sub_total',
'user_id',
'tdate',
'tgl',
'ref',
'loket_retur_id',
	),
        'itemTemplate' => "<tr class=\"{class}\"><td style=\"width: 120px\"><b>{label}</b></td><td>{value}</td></tr>\n",
        'htmlOptions' => array(
            'class' => 'table',
        ),
)); ?>

<!--h2>Loket Retur Detils</h2-->
<?php
/*
	echo GxHtml::openTag('ul');
	foreach($model->loketReturDetils as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('loketReturDetil/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?>