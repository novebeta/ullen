<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('loket_retur_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->loket_retur_id), array('view', 'id' => $data->loket_retur_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('loket_id')); ?>:
	<?php echo GxHtml::encode($data->loket_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('note_')); ?>:
	<?php echo GxHtml::encode($data->note_); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('total')); ?>:
	<?php echo GxHtml::encode($data->total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sub_total')); ?>:
	<?php echo GxHtml::encode($data->sub_total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
	<?php echo GxHtml::encode($data->user_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tdate')); ?>:
	<?php echo GxHtml::encode($data->tdate); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('tgl')); ?>:
	<?php echo GxHtml::encode($data->tgl); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('ref')); ?>:
	<?php echo GxHtml::encode($data->ref); ?>
	<br />
	*/ ?>

</div>