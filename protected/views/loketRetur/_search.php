<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="span-8 last">
		<?php echo $form->label($model, 'loket_id'); ?>
		<?php echo $form->textField($model, 'loket_id', array('maxlength' => 36)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'note_'); ?>
		<?php echo $form->textField($model, 'note_', array('maxlength' => 600)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'total'); ?>
		<?php echo $form->textField($model, 'total', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'sub_total'); ?>
		<?php echo $form->textField($model, 'sub_total', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->textField($model, 'user_id', array('maxlength' => 60)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'tdate'); ?>
		<?php echo $form->textField($model, 'tdate'); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'tgl'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'tgl',
			'value' => $model->tgl,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'ref'); ?>
		<?php echo $form->textField($model, 'ref', array('maxlength' => 50)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'loket_retur_id'); ?>
		<?php echo $form->textField($model, 'loket_retur_id', array('maxlength' => 36)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
