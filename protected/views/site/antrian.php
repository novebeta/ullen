<script>
    jun.ViewportUi = Ext.extend(Ext.Viewport, {
        layout: "border",
        initComponent: function () {
            this.items = [
                {
                    id: 'panel-utama-id',
                    // title: 'Main Content',
                    collapsible: false,
                    region: 'center',
                    // margins: '5 0 0 0',
                    // width: '80%'
                    layout: {
                        type: 'vbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [
                        {
                            xtype: 'label',
                            text: 'Tour 1',
                            cls: 'panel-utama-antrian',
                            id: 'panel-utama-antrian-id'
                        },
                        {
                            xtype: 'label',
                            text: 'Grup Domestik',
                            cls: 'panel-utama-tipe',
                            id: 'panel-utama-tipe-id'
                        }
                    ]
                },
                new jun.AntrianGrid({
                    region: "east",
                    // margins: '5 0 0 0',
                    // cmargins: '5 5 0 0',
                    hideHeaders: true,
                    frameHeader: !1,
                    header: !1,
                    width: 350,
                    // bodyStyle:'overflowY: auto',
                    // autoScroll:false,
                    // setAutoScroll:false,
                    // minSize: 100,
                    // maxSize: 250
                }),
                {
                    region: 'south',
                    height: 50,
                    html: "<marquee class='running-text'>Selamat Datang di Museum Ullen Sentalu.</marquee>"
                    // minSize: 75,
                    // maxSize: 250,
                    // cmargins: '5 0 0 0'
                }
            ];
            jun.ViewportUi.superclass.initComponent.call(this);
            jun.rztAntrian.reload();
        }
    });
    Ext.onReady(function () {
        jun.runner = new Ext.util.TaskRunner();
        Ext.USE_NATIVE_JSON = true;
        Ext.Ajax.timeout = 1800000;
        var b = new jun.ViewportUi({});
        jun.increment = 0;
        var updateSocket = function () {
            jun.grid = Ext.getCmp('docs-jun.AntrianGrid');
            jun.tbar = jun.grid.toolbars[0].items.items[0];
            jun.pageData = jun.tbar.getPageData();
            if (typeof jun.rztAntrian.data.items[jun.increment] === 'undefined') {
                if (jun.increment > 0) {
                    jun.increment = 0;
                }
                if (jun.pageData.pages == jun.pageData.activePage) {
                    jun.rztAntrian.reload();
                    jun.tbar.moveFirst();
                } else {
                    jun.tbar.moveNext();
                }
            } else {
                Ext.getCmp('panel-utama-antrian-id').setText('Tour '+jun.rztAntrian.data.items[jun.increment].json.no_urut);
                Ext.getCmp('panel-utama-tipe-id').setText(jun.rztAntrian.data.items[jun.increment].json.nama_tour);
                Ext.getCmp('panel-utama-id').doLayout();
                // console.log(jun.rztAntrian.data.items[jun.increment].json.nama_tour);
                jun.increment++;
            }
        };
        var task = {
            run: updateSocket,
            interval: 5000 //1 second
        };
        jun.runner.start(task);
    });
    // Ext.onReady(function() {
    //     Ext.QuickTips.init();
    //
    //     // create some portlet tools using built in Ext tool ids
    //     var tools = [{
    //         id:'gear',
    //         handler: function(){
    //             Ext.Msg.alert('Message', 'The Settings tool was clicked.');
    //         }
    //     },{
    //         id:'close',
    //         handler: function(e, target, panel){
    //             panel.ownerCt.remove(panel, true);
    //         }
    //     }];
    //
    //     var viewport = new Ext.Viewport({
    //         layout:'fit',
    //         items:[{
    //             xtype: 'grouptabpanel',
    //             tabWidth: 130,
    //             activeGroup: 0,
    //             items: [{
    //                 mainItem: 1,
    //                 items: [{
    //                     title: 'Tickets',
    //                     layout: 'fit',
    //                     iconCls: 'x-icon-tickets',
    //                     tabTip: 'Tickets tabtip',
    //                     style: 'padding: 10px;',
    //                     items: [new SampleGrid([0,1,2,3,4])]
    //                 },
    //                     {
    //                         xtype: 'portal',
    //                         title: 'Dashboard',
    //                         tabTip: 'Dashboard tabtip',
    //                         items:[{
    //                             columnWidth:.33,
    //                             style:'padding:10px 0 10px 10px',
    //                             items:[{
    //                                 title: 'Grid in a Portlet',
    //                                 layout:'fit',
    //                                 tools: tools,
    //                                 items: new SampleGrid([0, 2, 3])
    //                             },{
    //                                 title: 'Another Panel 1',
    //                                 tools: tools,
    //                                 html: Ext.example.shortBogusMarkup
    //                             }]
    //                         },{
    //                             columnWidth:.33,
    //                             style:'padding:10px 0 10px 10px',
    //                             items:[{
    //                                 title: 'Panel 2',
    //                                 tools: tools,
    //                                 html: Ext.example.shortBogusMarkup
    //                             },{
    //                                 title: 'Another Panel 2',
    //                                 tools: tools,
    //                                 html: Ext.example.shortBogusMarkup
    //                             }]
    //                         },{
    //                             columnWidth:.33,
    //                             style:'padding:10px',
    //                             items:[{
    //                                 title: 'Panel 3',
    //                                 tools: tools,
    //                                 html: Ext.example.shortBogusMarkup
    //                             },{
    //                                 title: 'Another Panel 3',
    //                                 tools: tools,
    //                                 html: Ext.example.shortBogusMarkup
    //                             }]
    //                         }]
    //                     }, {
    //                         title: 'Subscriptions',
    //                         iconCls: 'x-icon-subscriptions',
    //                         tabTip: 'Subscriptions tabtip',
    //                         style: 'padding: 10px;',
    //                         layout: 'fit',
    //                         items: [{
    //                             xtype: 'tabpanel',
    //                             activeTab: 1,
    //                             items: [{
    //                                 title: 'Nested Tabs',
    //                                 html: Ext.example.shortBogusMarkup
    //                             }]
    //                         }]
    //                     }, {
    //                         title: 'Users',
    //                         iconCls: 'x-icon-users',
    //                         tabTip: 'Users tabtip',
    //                         style: 'padding: 10px;',
    //                         html: Ext.example.shortBogusMarkup
    //                     }]
    //             }, {
    //                 expanded: true,
    //                 items: [{
    //                     title: 'Configuration',
    //                     iconCls: 'x-icon-configuration',
    //                     tabTip: 'Configuration tabtip',
    //                     style: 'padding: 10px;',
    //                     html: Ext.example.shortBogusMarkup
    //                 }, {
    //                     title: 'Email Templates',
    //                     iconCls: 'x-icon-templates',
    //                     tabTip: 'Templates tabtip',
    //                     style: 'padding: 10px;',
    //                     html: Ext.example.shortBogusMarkup
    //                 }]
    //             }]
    //         }]
    //     });
    // });
</script>
<div id="counterView" style="height: 100%"></div>