<?php
$this->breadcrumbs = array(
	'Reservasis',
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' Reservasi', 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' Reservasi', 'url' => array('admin')),
);
?>

<h1>Reservasis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 