<?php
$this->breadcrumbs = array(
	'Reservasis' => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' Reservasi', 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' Reservasi', 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' Reservasi', 'url'=>array('update', 'id' => $model->reservasi_id)),
	array('label'=>Yii::t('app', 'Delete') . ' Reservasi', 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->reservasi_id), 'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>Yii::t('app', 'Manage') . ' Reservasi', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View'); ?> Reservasi #<?php echo GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'reservasi_id',
'member_id',
'jenis_tour_id',
'lang_tour_id',
'waktu_reservasi',
'waktu_tour',
'note_',
'total',
'uang_muka',
'sub_total',
'user_id',
'tdate',
'kategori_cust_id',
	),
        'itemTemplate' => "<tr class=\"{class}\"><td style=\"width: 120px\"><b>{label}</b></td><td>{value}</td></tr>\n",
        'htmlOptions' => array(
            'class' => 'table',
        ),
)); ?>

<!--h2>Reservasi Detils</h2-->
<?php
/*
	echo GxHtml::openTag('ul');
	foreach($model->reservasiDetils as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('reservasiDetil/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');*/
?>