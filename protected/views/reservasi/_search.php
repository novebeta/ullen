<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="span-8 last">
		<?php echo $form->label($model, 'reservasi_id'); ?>
		<?php echo $form->textField($model, 'reservasi_id', array('maxlength' => 36)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'member_id'); ?>
		<?php echo $form->textField($model, 'member_id', array('maxlength' => 36)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'jenis_tour_id'); ?>
		<?php echo $form->textField($model, 'jenis_tour_id', array('maxlength' => 36)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'lang_tour_id'); ?>
		<?php echo $form->textField($model, 'lang_tour_id', array('maxlength' => 36)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'waktu_reservasi'); ?>
		<?php echo $form->textField($model, 'waktu_reservasi'); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'waktu_tour'); ?>
		<?php echo $form->textField($model, 'waktu_tour'); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'note_'); ?>
		<?php echo $form->textField($model, 'note_', array('maxlength' => 600)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'total'); ?>
		<?php echo $form->textField($model, 'total', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'uang_muka'); ?>
		<?php echo $form->textField($model, 'uang_muka', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'sub_total'); ?>
		<?php echo $form->textField($model, 'sub_total', array('maxlength' => 15)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'user_id'); ?>
		<?php echo $form->textField($model, 'user_id', array('maxlength' => 60)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'tdate'); ?>
		<?php echo $form->textField($model, 'tdate'); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'kategori_cust_id'); ?>
		<?php echo $form->textField($model, 'kategori_cust_id', array('maxlength' => 36)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
