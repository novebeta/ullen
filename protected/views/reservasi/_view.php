<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('reservasi_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->reservasi_id), array('view', 'id' => $data->reservasi_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('member_id')); ?>:
	<?php echo GxHtml::encode($data->member_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('jenis_tour_id')); ?>:
	<?php echo GxHtml::encode($data->jenis_tour_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('lang_tour_id')); ?>:
	<?php echo GxHtml::encode($data->lang_tour_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('waktu_reservasi')); ?>:
	<?php echo GxHtml::encode($data->waktu_reservasi); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('waktu_tour')); ?>:
	<?php echo GxHtml::encode($data->waktu_tour); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('note_')); ?>:
	<?php echo GxHtml::encode($data->note_); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('total')); ?>:
	<?php echo GxHtml::encode($data->total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('uang_muka')); ?>:
	<?php echo GxHtml::encode($data->uang_muka); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sub_total')); ?>:
	<?php echo GxHtml::encode($data->sub_total); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('user_id')); ?>:
	<?php echo GxHtml::encode($data->user_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('tdate')); ?>:
	<?php echo GxHtml::encode($data->tdate); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('kategori_cust_id')); ?>:
	<?php echo GxHtml::encode($data->kategori_cust_id); ?>
	<br />
	*/ ?>

</div>