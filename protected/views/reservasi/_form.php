<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'reservasi-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="span-8 last">
		<?php echo $form->labelEx($model,'member_id'); ?>
		<?php echo $form->textField($model, 'member_id', array('maxlength' => 36)); ?>
		<?php echo $form->error($model,'member_id'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'jenis_tour_id'); ?>
		<?php echo $form->textField($model, 'jenis_tour_id', array('maxlength' => 36)); ?>
		<?php echo $form->error($model,'jenis_tour_id'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'lang_tour_id'); ?>
		<?php echo $form->textField($model, 'lang_tour_id', array('maxlength' => 36)); ?>
		<?php echo $form->error($model,'lang_tour_id'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'waktu_reservasi'); ?>
		<?php echo $form->textField($model, 'waktu_reservasi'); ?>
		<?php echo $form->error($model,'waktu_reservasi'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'waktu_tour'); ?>
		<?php echo $form->textField($model, 'waktu_tour'); ?>
		<?php echo $form->error($model,'waktu_tour'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'note_'); ?>
		<?php echo $form->textField($model, 'note_', array('maxlength' => 600)); ?>
		<?php echo $form->error($model,'note_'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'total'); ?>
		<?php echo $form->textField($model, 'total', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'total'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'uang_muka'); ?>
		<?php echo $form->textField($model, 'uang_muka', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'uang_muka'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'sub_total'); ?>
		<?php echo $form->textField($model, 'sub_total', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'sub_total'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model, 'user_id', array('maxlength' => 60)); ?>
		<?php echo $form->error($model,'user_id'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'tdate'); ?>
		<?php echo $form->textField($model, 'tdate'); ?>
		<?php echo $form->error($model,'tdate'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'kategori_cust_id'); ?>
		<?php echo $form->textField($model, 'kategori_cust_id', array('maxlength' => 36)); ?>
		<?php echo $form->error($model,'kategori_cust_id'); ?>
		</div><!-- row -->
<!-- june -->
<div class="row"></div>
<!-- june -->
		<!--label--><!--/label-->
		                
<?php
echo GxHtml::Button(Yii::t('app', 'Cancel'), array(
			'submit' => array('reservasi/admin')
		));
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->