<?php
$this->breadcrumbs = array(
	'Reservasis' => array('index'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' Reservasi',
			'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' Reservasi',
		'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reservasi-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage'); ?> Reservasis</h1>

<p style="display:none">
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php //echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'reservasi-grid',
	'dataProvider' => $model->search(),
        'itemsCssClass' => 'table',
	'filter' => $model,
	'columns' => array(
		'reservasi_id',
		'member_id',
		'jenis_tour_id',
		'lang_tour_id',
		'waktu_reservasi',
		'waktu_tour',
		/*
		'note_',
		'total',
		'uang_muka',
		'sub_total',
		'user_id',
		'tdate',
		'kategori_cust_id',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>