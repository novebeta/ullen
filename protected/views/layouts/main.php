<html>
<head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <link rel="shortcut icon" href="<?php echo bu(); ?>/favicon.png?v=<?php echo md5_file( 'favicon.png' ) ?>">
    <title><?php echo CHtml::encode( Yii::app()->name ); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/examples.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/form/combos.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
    <style>
        #drop {
            border: 2px dashed #BBBBBB;
            border-radius: 5px;
            color: #BBBBBB;
            font: 20pt bold, "Vollkorn";
            padding: 25px;
            text-align: center;
        }

        * {
            font-size: 14px;
            font-family: Candara;
        }

        .container {
            display: table;
        }

        .search-item-table {
            display: table-row;
            color: #8B1D51;
        }

        .cell4 {
            display: table-cell;
            /*border: solid;*/
            /*border-width: thin;*/
            padding-left: 5px;
            padding-right: 5px;
        }

        .custom-sales-details .x-grid-row-selected .x-grid-cell-first {
            padding-left: 5px;
        }

        @media screen and (-webkit-min-device-pixel-ratio: 0) {
            .x-grid3-cell, /* Normal grid cell */
            .x-grid3-gcell { /* Grouped grid cell (esp. in head)*/
                box-sizing: border-box;
            }
        }

        .mfcombobox {
            width: 100%;
            border: 1px solid #bbb;
            border-collapse: collapse;
        }

        .mfcombobox th {
            font-weight: bold;
        }

        .mfcombobox td, .mfcombobox th {
            border: 1px solid #ccc;
            border-collapse: collapse;
            padding: 5px;
        }
    </style>
</head>
<body>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/jsrsasign-all-min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script>
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date( "Y-m-d H:i:s" )?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu(); ?>/images/logo.png" alt=""/>';
    EDIT_TGL = 1;
    // if(!notReady()){
    //     var fs = require('fs');
    //     var data = fs.readFileSync('./config.json'),myObj;
    //     try {
    //         myObj = JSON.parse(data);
    //         PRINTER_RECEIPT = myObj.PRINTER_RECEIPT;
    //         PRINTER_CARD = myObj.PRINTER_CARD ;
    //         COM_POSIFLEX = myObj.COM_POSIFLEX ;
    //     }
    //     catch (err) {
    //         console.log('There has been an error parsing your JSON.')
    //         console.log(err);
    //     }
    // }
    LANG_DEFAULT = '<?=LangTour::getDefault();?>';

    function nwis_round_up(e) {
        return round(Math.round(round(e / 0.05, 2)) * 0.05, 2);
    }

    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
    }

    window.onbeforeunload = goodbye;
    // if (!notReady()) {
    //     var gui = require('nw.gui');
    //     var win = gui.Window.get();
    //     win.maximize();
    //     win.on('new-win-policy', function (frame, url, policy) {
    //         policy.forceNewPopup();
    //     });
    // }
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/cc.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/examples.js"></script>
<?
$dir = array( '/js/view/' );
foreach ( $dir as $path ) {
	$templatePath = dirname( Yii::app()->basePath ) . $path;
	$files        = scandir( $templatePath );
	foreach ( $files as $file ) {
		if ( is_file( $templatePath . '/' . $file ) ) {
			?>
            <script type="text/javascript"
                    src="<?php echo( bu() . $path . $file . "?v=" . md5_file( dirname( Yii::app()->getBasePath() ) . $path . $file ) ); ?>"></script>
			<?
		}
	}
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/mainpanel.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<?php echo $content; ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/FileUploadField.js"></script>
<script type="text/javascript"></script>
<iframe id="myFrame" name="myFrame" style="border:none"></iframe>
</body>
</html>
