<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?php echo bu(); ?>/favicon.png?v=<?php echo md5_file( 'favicon.png' ) ?>">
    <title><?php echo CHtml::encode( Yii::app()->name ); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/form/combos.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/Portal.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/GroupTab.css"/>
    <style>
        #drop {
            border: 2px dashed #BBBBBB;
            border-radius: 5px;
            color: #BBBBBB;
            font: 20pt bold, "Vollkorn";
            padding: 25px;
            text-align: center;
        }

        * {
            font-size: 12px;
            font-family: Candara;
        }

        .container {
            display: table;
        }

        .search-item-table {
            display: table-row;
            color: #8B1D51;
        }

        .cell4 {
            display: table-cell;
            /*border: solid;*/
            /*border-width: thin;*/
            padding-left: 5px;
            padding-right: 5px;
        }

        .custom-sales-details .x-grid-row-selected .x-grid-cell-first {
            padding-left: 5px;
        }

        @media screen and (-webkit-min-device-pixel-ratio: 0) {
            .x-grid3-cell, /* Normal grid cell */
            .x-grid3-gcell { /* Grouped grid cell (esp. in head)*/
                box-sizing: border-box;
            }
        }

        .mfcombobox {
            width: 100%;
            border: 1px solid #bbb;
            border-collapse: collapse;
        }

        .mfcombobox th {
            font-weight: bold;
        }

        .mfcombobox td, .mfcombobox th {
            border: 1px solid #ccc;
            border-collapse: collapse;
            padding: 5px;
        }

        .panel-utama-antrian {
            font-weight: bold;
            font-size: 200px;
            font-family: "Vollkorn", serif;
        }

        .panel-utama-tipe {
            font-weight: bold;
            font-size: 50px;
            font-family: "Vollkorn", serif;
        }

        .grid-tour {
            font-weight: bold;
            font-size: 60px;
            margin: 0 auto;
            overflow: auto;
            font-family: "Vollkorn", serif;
        }

        .grid-jenis {
            font-weight: bold;
            font-size: 16px;
            display: block;
            margin-top: 10px;
            font-family: "Vollkorn", serif;
        }

        .running-text {
            font-weight: bold;
            font-size: 40px;
            font-family: "Vollkorn", serif;
        }

        .parent {
            width: 100%;
            height: 100%;
            display: table;
            text-align: center;
            font-family: "Vollkorn", serif;
        }

        .parent > .child {
            display: table-cell;
            vertical-align: middle;
            font-family: "Vollkorn", serif;
        }

        .x-grid3-row {
            height: 25%;
        }

        .x-grid3-row-table {
            height: 100%
        }

        .x-grid3-cell {
            height: 100%
        }

        .x-grid3-cell-inner {
            height: 100%
        }

        .x-grid3-row-table TD {
            line-height: 100%;
        }
    </style>
</head>
<body>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/grupTrans_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/grupTrans_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupTabPanel.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupTab.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/Portal.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/PortalColumn.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/Portlet.js"></script>
<!-- page specific -->
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/examples.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/portal/sample-grid.js"></script>
<?php echo $content; ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
</body>
</html>
