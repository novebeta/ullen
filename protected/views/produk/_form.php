<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'produk-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="span-8 last">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model, 'nama', array('maxlength' => 100)); ?>
		<?php echo $form->error($model,'nama'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'harga'); ?>
		<?php echo $form->textField($model, 'harga', array('maxlength' => 30)); ?>
		<?php echo $form->error($model,'harga'); ?>
		</div><!-- row -->
		<div class="span-8 last">
		<?php echo $form->labelEx($model,'jenis_tour_id'); ?>
		<?php echo $form->textField($model, 'jenis_tour_id', array('maxlength' => 36)); ?>
		<?php echo $form->error($model,'jenis_tour_id'); ?>
		</div><!-- row -->
<!-- june -->
<div class="row"></div>
<!-- june -->
		<!--label--><!--/label-->
		                
<?php
echo GxHtml::Button(Yii::t('app', 'Cancel'), array(
			'submit' => array('produk/admin')
		));
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->