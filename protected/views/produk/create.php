<?php
$this->breadcrumbs = array(
	'Produks' => array('index'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'List') . ' Produk', 'url' => array('index')),
	array('label'=>Yii::t('app', 'Manage') . ' Produk', 'url' => array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'Create'); ?> Produk</h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model,
		'buttons' => 'create'));
?>