<?php
$this->breadcrumbs = array(
	'Produks' => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' Produk', 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' Produk', 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' Produk', 'url'=>array('update', 'id' => $model->produk_id)),
	array('label'=>Yii::t('app', 'Delete') . ' Produk', 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->produk_id), 'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>Yii::t('app', 'Manage') . ' Produk', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View'); ?> Produk #<?php echo GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'produk_id',
'nama',
'harga',
'jenis_tour_id',
	),
        'itemTemplate' => "<tr class=\"{class}\"><td style=\"width: 120px\"><b>{label}</b></td><td>{value}</td></tr>\n",
        'htmlOptions' => array(
            'class' => 'table',
        ),
)); ?>

