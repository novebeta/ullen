<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('produk_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->produk_id), array('view', 'id' => $data->produk_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nama')); ?>:
	<?php echo GxHtml::encode($data->nama); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('harga')); ?>:
	<?php echo GxHtml::encode($data->harga); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('jenis_tour_id')); ?>:
	<?php echo GxHtml::encode($data->jenis_tour_id); ?>
	<br />

</div>