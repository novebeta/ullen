<?php
$this->breadcrumbs = array(
	'Produks',
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' Produk', 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' Produk', 'url' => array('admin')),
);
?>

<h1>Produks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 