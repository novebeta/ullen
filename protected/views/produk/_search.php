<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="span-8 last">
		<?php echo $form->label($model, 'produk_id'); ?>
		<?php echo $form->textField($model, 'produk_id', array('maxlength' => 36)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'nama'); ?>
		<?php echo $form->textField($model, 'nama', array('maxlength' => 100)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'harga'); ?>
		<?php echo $form->textField($model, 'harga', array('maxlength' => 30)); ?>
	</div>

	<div class="span-8 last">
		<?php echo $form->label($model, 'jenis_tour_id'); ?>
		<?php echo $form->textField($model, 'jenis_tour_id', array('maxlength' => 36)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
