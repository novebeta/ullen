<?php
/**
 * Created by PhpStorm.
 * User: MASTER
 * Date: 7/12/14
 * Time: 11:30 AM
 */
class BasePrint {
	static function fillWithChar( $char, $l = CHARLENGTHRECEIPT ) {
//        $res = "";
//        for ($i = 0; $i < $l; $i++) {
//            $res .= $char;
//        }
		return str_repeat( $char, $l );
//        return $res;
	}
	static function setCenter( $msg, $l = CHARLENGTHRECEIPT ) {
		$lmsg = strlen( $msg );
		if ( $lmsg > $l ) {
			return substr( $msg, 0, $l );
		}
		return str_pad( $msg, $l, " ", STR_PAD_BOTH );
//        $sisa = $l - $lmsg;
//        $awal = $sisa >> 1;
//        $res = self::fillWithChar(" ", $awal);
//        $res .= $msg . self::fillWithChar(" ", $sisa - $awal);
//        return $res;
	}
	static function addHeaderSales( $msg1, $msg2, $l = 15 ) {
		$lmsg1 = strlen( $msg1 );
		if ( $lmsg1 > $l ) {
			$msg1  = substr( $msg1, 0, $l );
			$lmsg1 = 15;
		}
		$res = $msg1 . self::fillWithChar( " ", $l - $lmsg1 );
		$res .= ":" . $msg2;
		return $res;
	}
	static function addLeftRight( $msg1, $msg2, $l = CHARLENGTHRECEIPT ) {
		$lmsg1 = strlen( $msg1 );
		$lmsg2 = strlen( $msg2 );
		$sisa  = $l - ( $lmsg1 + $lmsg2 );
		if ( $sisa <= 0 ) {
			return $msg1 . $msg2;
		}
		return $msg1 . self::fillWithChar( " ", $sisa ) . $msg2;
	}
	static function addPengjung( $msg1, $msg2, $indent = 3, $l = CHARLENGTHRECEIPT ) {
		$msg  = self::fillWithChar( " ", $indent );
		$msg1 = $msg . $msg1;
//		$lmsg1 = strlen( $msg1 );
//		$lmsg2 = strlen( $msg2 );
//		$sisa  = $l - ( $lmsg1 + $lmsg2 );
//		if ( $sisa <= 0 ) {
//			return $msg1 . $msg2;
//		}
//		return $msg1 . self::fillWithChar( " ", $sisa ) . $msg2;
		return $msg1 . "  " . $msg2;
	}
	static function addTiket( $msg1, $msg2, $msg3, $msg4, $msg5, $indent = 3, $l = CHARLENGTHRECEIPT ) {
		$msg  = self::fillWithChar( " ", $indent );
		$msg1 = $msg . $msg1;
		$raw  = self::addLeftRight( $msg1, $msg2, 16 );
		$raw  = self::addLeftRight( $raw, $msg3, 26 );
		$raw  = self::addLeftRight( $raw, $msg4, 35 );
		$raw  = self::addLeftRight( $raw, $msg5, 45 );
		return $raw;
	}
	static function addTotalPengunjung( $msg1, $msg2, $msg3, $l = CHARLENGTHRECEIPT ) {
		$raw = self::addLeftRight( $msg1, $msg2, 24 );
		$raw = self::addLeftRight( $raw, $msg3, $l );
		return $raw;
	}
	static function addItemCodeReceipt( $itemKode, $qty, $harga, $subtotal, $l = CHARLENGTHRECEIPT ) {
		$litemKode = strlen( $itemKode ); // max 39
		$mitemKode = 19;
//		$lqty      = strlen( $qty ); //max 7
		$mqty = 5;
//		$lharga = strlen( $harga ); //max 17
		$mharga = 11;
//		$lsubtotal = strlen( $subtotal ); //max 17
		$msubtotal = 13;
		$itemKode  = substr( $itemKode, 0, $mitemKode );
		$qty       = substr( $qty, 0, $mitemKode );
		$harga     = substr( $harga, 0, $mharga );
		$subtotal  = substr( $subtotal, 0, $msubtotal );
		$msg1      = self::addLeftRight( $itemKode, $qty, $mitemKode + $mqty );
		$msg2      = self::addLeftRight( $msg1, $harga, $mitemKode + $mqty + $mharga );
		return self::addLeftRight( $msg2, $subtotal, $l );
	}
	static function addItemPembayaranDP( $tgl, $tipe_bayar, $nama, $nominal, $l = CHARLENGTHRECEIPT ) {
		$tipe_bayar  = ( $tipe_bayar == 0 ? 'CASH' : 'TRF' );
		$mtgl        = 10;
		$mtipe_bayar = 10;
		$mnama       = 13;
		$mnominal    = 14;
		$tgl         = substr( str_pad( $tgl, $mtgl, ' ', STR_PAD_RIGHT ), 0, $mtgl );
		$tipe_bayar  = substr( str_pad( $tipe_bayar, $mtipe_bayar, ' ', STR_PAD_RIGHT ), 0, $mtipe_bayar );
		$nama        = substr( str_pad( $nama, $mnama, ' ', STR_PAD_RIGHT ), 0, $mnama );
		$nominal     = substr( $nominal, 0, $mnominal );
		$msg1        = $tgl . $tipe_bayar . $nama;
//		$msg2        = self::addLeftRight( $msg1, $nama, $mtgl + $mtipe_bayar + $mnama );
		return self::addLeftRight( $msg1, $nominal, $l );
	}
	static function addItemSerahTerima( $counter, $itemKode, $qty, $harga, $subtotal, $l = CHARLENGTHRECEIPT ) {
		$litemKode = strlen( $itemKode ); // max 39
		$mcounter  = 3;
		$mitemKode = 14;
		$mqty      = 5;
		$mharga    = 10;
		$msubtotal = 14;
		$counter   = substr( str_pad( $counter, 3, "0", STR_PAD_LEFT ), 0, $mcounter );
		$itemKode  = substr( $itemKode, 0, $mitemKode );
		$qty       = substr( $qty, 0, $mqty );
		$harga     = substr( str_pad( $harga, $mharga, ' ', STR_PAD_LEFT ), 0, $mharga );
		$subtotal  = substr( $subtotal, 0, $msubtotal );
		$msg1      = self::addLeftRight( $counter . ' ' . $itemKode, $qty, $mitemKode + $mqty + 4 );
		$msg2      = self::addLeftRight( $msg1, $harga, $mitemKode + $mqty + $mharga );
		return self::addLeftRight( $msg2, $subtotal, $l );
	}
	static function addItemNameReceipt( $itemName, $l, $prefix = 5 ) {
		$itemName = self::fillWithChar( " ", $prefix ) . $itemName;
		if ( strlen( $itemName ) > $l ) {
			return substr( $itemName, 0, $l );
		}
		return $itemName;
	}
	static function addItemDiscReceipt( $disc, $subtotal, $prefix = 6, $l = CHARLENGTHRECEIPT ) {
		$lbldisc = self::fillWithChar( " ", $prefix ) . "Disc: $disc";
		return self::addLeftRight( $lbldisc, $subtotal, $l );
	}
} 