<?php
class MenuTree {
	var $security_role;
	var $menu_users = array(
		'text'  => 'User Manajement',
		'class' => 'jun.UsersGrid',
		'leaf'  => true
	);
	var $security = array(
		'text'  => 'Security Roles',
		'class' => 'jun.SecurityRolesGrid',
		'leaf'  => true
	);
	function __construct( $id ) {
		$role                = SecurityRoles::model()->findByPk( $id );
		$this->security_role = explode( ",", $role->sections );
	}
	function getChildMaster() {
		/** @var TODO 120 dan 112 kosong */
		$child = array();
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Kategori Member',
				'class' => 'jun.MemberKategoriGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Member',
				'class' => 'jun.MemberGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Member Point',
				'class' => 'jun.MemberPointGrid ',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Guide',
				'class' => 'jun.GuideGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Kategori Jenis Tour',
				'class' => 'jun.JenisTourGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Kategori Pengunjung',
				'class' => 'jun.KategoriCustGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Kategori Bahasa Tour',
				'class' => 'jun.LangTourGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Produk',
				'class' => 'jun.ProdukGrid',
				'leaf'  => true
			);
		}
//		if ( in_array( 105, $this->security_role ) ) {
//			$child[] = array(
//				'text'  => 'Tour',
//				'class' => 'jun.GrupGrid',
//				'leaf'  => true
//			);
//		}
		return $child;
	}
	function getMaster( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Master',
			'expanded' => false,
			'children' => $child
		);
	}
	function getTransaction( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Transaction',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildTransaction() {
		$child                      = array();
		$childSales                 = array();
		$childPurchase              = array();
		$childExpenses              = array();
		$childAcc                   = array();
		$childSuppliesManagement    = array();
		$childTreatment             = array();
		$childMarketing             = array();
		$childTenderTransferFunding = array();
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Barang Masuk',
				'class' => 'jun.TransferBarangGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Reservasi',
				'class' => 'jun.ReservasiGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Nota',
				'class' => 'jun.LoketGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Nota Retur',
				'class' => 'jun.LoketReturGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Regular Adult',
				'class' => 'jun.RegularAdultGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Regular Child',
				'class' => 'jun.RegularChildGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Regular Free',
				'class' => 'jun.RegularFreeGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Exclusive Adult',
				'class' => 'jun.ExclusiveAdultGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Exclusive Child',
				'class' => 'jun.ExclusiveChildGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Vorstenlanden Adult',
				'class' => 'jun.VorstenlandenAdultGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Vorstenlanden Child',
				'class' => 'jun.VorstenlandenChildGrid',
				'leaf'  => true
			);
		}
		return $child;
	}
	function getReport( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Report',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildReport() {
		// terakhir 334
		$child             = array();
		$childBalanceSheet = array();
		$childProfitLoss   = array();
		$childAssets       = array();
		$childStock        = array();
		$childoLiabilities = array();
		$childooEquity     = array();
		$childSales        = array();
		$childPurchases    = array();
		$childExpenses     = array();
		$childCashFlow     = array();
		$childAcc          = array();
		$childInvSupp      = array();
		$childMarketing    = array();
		$childCust         = array();
		$childSalesPrice   = array();
		$childPromo        = array();
		$childProduct      = array();
		if ( in_array( 319, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Balance Sheet Movement',
				'class' => 'jun.ReportBalanceSheet',
				'leaf'  => true
			);
		}
		if ( in_array( 320, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Neraca',
				'class' => 'jun.ReportNeraca',
				'leaf'  => true
			);
		}
		if ( in_array( 323, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Rekening Koran Kas / Bank',
				'class' => 'jun.ReportRekeningKoran',
				'leaf'  => true
			);
		}
//		if ( in_array( 303, $this->security_role ) ) {
//			$child[] = array(
//				'text'  => 'Inventory Card',
//				'class' => 'jun.ReportInventoryCard',
//				'leaf'  => true
//			);
//		}
//		if ( in_array( 302, $this->security_role ) ) {
//			$child[] = array(
//				'text'  => 'Inventory Movements Summary',
//				'class' => 'jun.ReportInventoryMovements',
//				'leaf'  => true
//			);
//		}
		if ( in_array( 335, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Fixed Asset',
				'class' => 'jun.ReportFixedAsset',
				'leaf'  => true
			);
		}
		if ( in_array( 336, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Account Receivable',
				'class' => 'jun.ReportAccountReceivable',
				'leaf'  => true
			);
		}
//        if (!empty($childStock)) {
//            $childAssets[] = array(
//                'text' => 'Stock Inventories',
//                'expanded' => false,
//                'children' => $childStock
//            );
//        }
//        if (!empty($childAssets)) {
//            $childBalanceSheet[] = array(
//                'text' => 'Asset',
//                'expanded' => false,
//                'children' => $childAssets
//            );
//        }
		if ( in_array( 337, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Loan',
				'class' => 'jun.ReportLoan',
				'leaf'  => true
			);
		}
		if ( in_array( 324, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Account Payable',
				'class' => 'jun.ReportKartuHutang',
				'leaf'  => true
			);
		}
		if ( in_array( 338, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Tax',
				'class' => 'jun.ReportSalesTax',
				'leaf'  => true
			);
		}
		if ( in_array( 339, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Tax',
				'class' => 'jun.ReportTax',
				'leaf'  => true
			);
		}
		if ( ! empty( $childoLiabilities ) ) {
			$child[] = array(
				'text'     => 'Liabilities',
				'expanded' => false,
				'children' => $childoLiabilities
			);
		}
		if ( in_array( 340, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Capital',
				'class' => 'jun.ReportCapital',
				'leaf'  => true
			);
		}
		if ( in_array( 341, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Retained Earning',
				'class' => 'jun.ReportRetainedEarning',
				'leaf'  => true
			);
		}
//        if (!empty($childooEquity)) {
//            $childBalanceSheet[] = array(
//                'text' => 'Equity',
//                'expanded' => false,
//                'children' => $childooEquity
//            );
//        }
//        if (!empty($childBalanceSheet)) {
//            $child[] = array(
//                'text' => 'Balance Sheet',
//                'expanded' => false,
//                'children' => $childBalanceSheet
//            );
//        }
		if ( in_array( 318, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Profit Lost Report',
				'class' => 'jun.ReportLabaRugi',
				'leaf'  => true
			);
		}
		if ( in_array( 322, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Price',
				'class' => 'jun.ReportSalesPrice',
				'leaf'  => true
			);
		}
		if ( in_array( 331, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales and Sales Return by Group',
				'class' => 'jun.ReportSalesNReturnDetails',
				'leaf'  => true
			);
		}
		if ( in_array( 301, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Details by Group',
				'class' => 'jun.ReportSalesDetails',
				'leaf'  => true
			);
		}
		if ( in_array( 300, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Summary by Group',
				'class' => 'jun.ReportSalesSummary',
				'leaf'  => true
			);
		}
		if ( in_array( 313, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Summary',
				'class' => 'jun.ReportSalesSummaryReceipt',
				'leaf'  => true
			);
		}
		if ( in_array( 316, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Details',
				'class' => 'jun.ReportSalesSummaryReceiptDetails',
				'leaf'  => true
			);
		}
		if ( in_array( 310, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Return Details by Group',
				'class' => 'jun.ReportReturSalesDetails',
				'leaf'  => true
			);
		}
		if ( in_array( 309, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Return Sales Summary',
				'class' => 'jun.ReportReturSalesSummary',
				'leaf'  => true
			);
		}
		if ( in_array( 321, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Payments',
				'class' => 'jun.ReportPayments',
				'leaf'  => true
			);
		}
//        if (!empty($childSales)) {
//            $childProfitLoss[] = array(
//                'text' => 'Sales',
//                'expanded' => false,
//                'children' => $childSales
//            );
//        }
		if ( in_array( 330, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Purchase Price',
				'class' => 'jun.ReportPurchasePrice',
				'leaf'  => true
			);
		}
		if ( in_array( 342, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Purchase and Purchase Return by Group',
				'class' => 'jun.ReportPurchasenReturnGroup',
				'leaf'  => true
			);
		}
		if ( in_array( 343, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Purchase',
				'class' => 'jun.ReportPurchase',
				'leaf'  => true
			);
		}
		if ( in_array( 344, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Purchase Return',
				'class' => 'jun.ReportPurchaseReturn',
				'leaf'  => true
			);
		}
		if ( in_array( 345, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Debt Payment',
				'class' => 'jun.ReportDebtPayment',
				'leaf'  => true
			);
		}
//        if (!empty($childPurchases)) {
//            $childProfitLoss[] = array(
//                'text' => 'Purchases',
//                'expanded' => false,
//                'children' => $childPurchases
//            );
//        }
		if ( in_array( 346, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'List of Expenses Accounts',
				'class' => 'jun.ReportListofExpensesaccounts',
				'leaf'  => true
			);
		}
//        if (!empty($childExpenses)) {
//            $childProfitLoss[] = array(
//                'text' => 'Expenses',
//                'expanded' => false,
//                'children' => $childExpenses
//            );
//        }
//        if (!empty($childProfitLoss)) {
//            $child[] = array(
//                'text' => 'Profit and Loss',
//                'expanded' => false,
//                'children' => $childProfitLoss
//            );
//        }
//        if (!empty($childCashFlow)) {
//            $child[] = array(
//                'text' => 'Cash Flow',
//                'expanded' => false,
//                'children' => $childCashFlow
//            );
//        }
		if ( in_array( 314, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'General Ledger',
				'class' => 'jun.ReportGeneralLedger',
				'leaf'  => true
			);
		}
		if ( in_array( 315, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'General Journal',
				'class' => 'jun.ReportGeneralJournal',
				'leaf'  => true
			);
		}
//        if (!empty($childAcc)) {
//            $child[] = array(
//                'text' => 'Accounting',
//                'expanded' => false,
//                'children' => $childAcc
//            );
//        }
		if ( in_array( 303, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Laporan Serah Terima Loket',
				'class' => 'jun.ReportSerahTerimaLoket',
				'leaf'  => true
			);
		}
//		if ( in_array( 303, $this->security_role ) ) {
//			$child[] = array(
//				'text'  => 'Inventory Card',
//				'class' => 'jun.ReportInventoryCard',
//				'leaf'  => true
//			);
//		}
//		if ( in_array( 302, $this->security_role ) ) {
//			$child[] = array(
//				'text'  => 'Inventory Movements Summary',
//				'class' => 'jun.ReportInventoryMovements',
//				'leaf'  => true
//			);
//		}
//		if ( in_array( 333, $this->security_role ) ) {
//			$child[] = array(
//				'text'  => 'Supplies Inventory Movements',
//				'class' => 'jun.ReportInventoryMovementsClinical',
//				'leaf'  => true
//			);
//		}
		if ( in_array( 347, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Supplies Card',
				'class' => 'jun.ReportSuppliesCard',
				'leaf'  => true
			);
		}
//        if (!empty($childInvSupp)) {
//            $child[] = array(
//                'text' => 'Inventories & Supplies',
//                'expanded' => false,
//                'children' => $childInvSupp
//            );
//        }
		if ( in_array( 306, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'New Customers',
				'class' => 'jun.ReportNewCustomers',
				'leaf'  => true
			);
		}
		if ( in_array( 334, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Customers Birthday List',
				'class' => 'jun.ReportBirthDayCustomers',
				'leaf'  => true
			);
		}
		if ( in_array( 332, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Category Customers',
				'class' => 'jun.ReportCategoryCustomers',
				'leaf'  => true
			);
		}
		if ( in_array( 325, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Chart Customers Attendance',
				'class' => 'jun.ChartCustAtt',
				'leaf'  => true
			);
		}
		if ( in_array( 326, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Chart New Customers',
				'class' => 'jun.ChartNewCustomers',
				'leaf'  => true
			);
		}
		if ( in_array( 328, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Chart Top Customers',
				'class' => 'jun.ChartTopCust',
				'leaf'  => true
			);
		}
//        if (!empty($childCust)) {
//            $childMarketing[] = array(
//                'text' => 'Customers',
//                'expanded' => false,
//                'children' => $childCust
//            );
//        }
		if ( in_array( 322, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Price',
				'class' => 'jun.ReportSalesPrice',
				'leaf'  => true
			);
		}
		if ( in_array( 330, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Purchase Price',
				'class' => 'jun.ReportPurchasePrice',
				'leaf'  => true
			);
		}
		if ( in_array( 331, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales and Sales Return by Group',
				'class' => 'jun.ReportSalesNReturnDetails',
				'leaf'  => true
			);
		}
		if ( in_array( 301, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Details by Group',
				'class' => 'jun.ReportSalesDetails',
				'leaf'  => true
			);
		}
		if ( in_array( 310, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Sales Return Details by Group',
				'class' => 'jun.ReportReturSalesDetails',
				'leaf'  => true
			);
		}
//        if (!empty($childSalesPrice)) {
//            $childMarketing[] = array(
//                'text' => 'Sales and Pricing',
//                'expanded' => false,
//                'children' => $childSalesPrice
//            );
//        }
		if ( in_array( 348, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Member Get Member',
				'class' => 'jun.ReportMemberGetMember',
				'leaf'  => true
			);
		}
		if ( in_array( 349, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'My Special Day',
				'class' => 'jun.ReportMySpecialDay',
				'leaf'  => true
			);
		}
		if ( in_array( 350, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Events & Sponsorship',
				'class' => 'jun.ReportEventsSponsorship',
				'leaf'  => true
			);
		}
		if ( in_array( 351, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Treatment Packages',
				'class' => 'jun.ReportTreatmentPackages',
				'leaf'  => true
			);
		}
		if ( in_array( 352, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Product Packages',
				'class' => 'jun.ReportProductPackages',
				'leaf'  => true
			);
		}
//        if (!empty($childPromo)) {
//            $childMarketing[] = array(
//                'text' => 'Promotion',
//                'expanded' => false,
//                'children' => $childPromo
//            );
//        }
		if ( in_array( 327, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Chart Sales Group',
				'class' => 'jun.ChartSalesGrup',
				'leaf'  => true
			);
		}
		if ( in_array( 329, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Chart Top Sales Group',
				'class' => 'jun.ChartTopSalesGrup',
				'leaf'  => true
			);
		}
//        if (!empty($childProduct)) {
//            $childMarketing[] = array(
//                'text' => 'Product',
//                'expanded' => false,
//                'children' => $childProduct
//            );
//        }
//        if (!empty($childMarketing)) {
//            $child[] = array(
//                'text' => 'Marketing Analysis',
//                'expanded' => false,
//                'children' => $childMarketing
//            );
//        }
		if ( in_array( 304, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Beautician Services Summary',
				'class' => 'jun.ReportBeautySummary',
				'leaf'  => true
			);
		}
		if ( in_array( 305, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Beautician Services Details',
				'class' => 'jun.ReportBeautyDetails',
				'leaf'  => true
			);
		}
		if ( in_array( 311, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Doctors Services Summary',
				'class' => 'jun.ReportDokterSummary',
				'leaf'  => true
			);
		}
		if ( in_array( 312, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Doctors Services Details',
				'class' => 'jun.ReportDokterDetails',
				'leaf'  => true
			);
		}
		if ( in_array( 307, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Tenders',
				'class' => 'jun.ReportTender',
				'leaf'  => true
			);
		}
		if ( in_array( 308, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Daily Report',
				'class' => 'jun.ReportLaha',
				'leaf'  => true
			);
		}
		return $child;
	}
	function getAdministration( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Administration',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildAdministration() {
		$child = array();
		if ( in_array( 400, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'User Management',
				'class' => 'jun.UsersGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 401, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Security Roles',
				'class' => 'jun.SecurityRolesGrid',
				'leaf'  => true
			);
		}
		if ( in_array( 402, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Backup / Restore',
				'class' => 'jun.BackupRestoreWin',
				'leaf'  => true
			);
		}
		if ( in_array( 403, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Import',
				'class' => 'jun.ImportXlsx',
				'leaf'  => true
			);
		}
		return $child;
	}
	function getGeneral() {
		$username = Yii::app()->user->name;
		$child    = array();
		if ( in_array( 000, $this->security_role ) ) {
			$child[] = array(
				'text'  => 'Change Password',
				'class' => 'jun.PasswordWin',
				'leaf'  => true
			);
		}
		if ( in_array( 001, $this->security_role ) ) {
			$child[] = array(
				'text'  => "Logout ($username)",
				'class' => 'logout',
				'leaf'  => true
			);
		}
		return $child;
	}
	public function get_menu() {
		$data   = array();
		$master = self::getMaster( self::getChildMaster() );
		if ( ! empty( $master ) ) {
			$data[] = $master;
		}
		$trans = self::getTransaction( self::getChildTransaction() );
		if ( ! empty( $trans ) ) {
			$data[] = $trans;
		}
		$report = self::getReport( self::getChildReport() );
		if ( ! empty( $report ) ) {
			$data[] = $report;
		}
		$adm = self::getAdministration( self::getChildAdministration() );
		if ( ! empty( $adm ) ) {
			$data[] = $adm;
		}
		$username = Yii::app()->user->name;
		if ( in_array( 000, $this->security_role ) ) {
			$data[] = array(
				'text'  => 'Change Password',
				'class' => 'jun.PasswordWin',
				'leaf'  => true
			);
		}
		if ( in_array( 001, $this->security_role ) ) {
			$data[] = array(
				'text'  => "Logout ($username)",
				'class' => 'logout',
				'leaf'  => true
			);
		}
		return CJSON::encode( $data );
	}
}
