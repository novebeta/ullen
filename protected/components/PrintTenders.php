<?php
/**
 * Created by PhpStorm.
 * User: novebeta
 * Date: 8/16/14
 * Time: 1:54 PM
 */
class PrintTenders extends BasePrint
{
    private $tender;
    private $store;
    private $tgl;
    function __construct($tgl, $store = STOREID)
    {
        $this->store = $store;
        $this->tgl = $tgl;
        $print_z = Tender::is_exist($tgl, $store);
        if ($print_z) {
            $this->tender = Tender::model()->find('DATE(tgl) = :tgl AND store = :store',
                array(':tgl' => $tgl, ':store' => $store));
        }
    }
    public function buildTxt()
    {
//        $this->tender = new Tender();
        $z = false;
        $printz = new Printz;
        if ($this->tender != null) {
            $z = true;
            $printz = $this->tender->printzs[0];
        }
        $newLine = "\r\n";
        $raw = parent::fillWithChar("-");
        $raw .= $newLine;
        $report = $this->tender == null ? "X-Report" : "Z-Report";
        $raw .= $report;
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Branch", $z ? $printz->store : $this->store);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", date('m/d/Y', strtotime($z ? $printz->date_ : $this->tgl)));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Print Date", date('m/d/Y H:i:s'));
        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Employee", $z ? $printz->employee : $user->name);
        $raw .= $newLine;
        $raw .= $newLine;
        $raw .= "Total amounts";
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $sales = Salestrans::get_disc_tax_bayar_change_all($this->tgl, $this->store);
//        $purchase = TransferItem::get_total_all_cash($this->tgl, $this->store);
        $tendered = $sales->bayar;# - $purchase;
        $started = U::get_balance_before_for_bank_account($this->tgl, Bank::get_bank_cash_id($this->store), $this->store);
        $added = Kas::get_cash_in($this->tgl, $this->store);
        $removed = abs(Kas::get_cash_out($this->tgl, $this->store));
        $collected = $tendered - $sales->kembali + $started + $added - $removed;
        $counted = 0;
        $filename = $report . date('Y-m-d-H-i-s');
        if ($this->tender != null) {
            $counted = $this->tender->total;
        }
        $raw .= parent::addLeftRight("Sales:", number_format($z ? $printz->sales : Salestrans::get_total_sales($this->tgl, $this->store), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Return Sales:", number_format($z ? $printz->return_sales : abs(Salestrans::get_total_returnsales($this->tgl, $this->store)), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Taxes Sales:", number_format($z ? $printz->tax_sales : $sales->vat, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Discounts Sales:", number_format($z ? $printz->disc_sales : $sales->totalpot, 2));
        $raw .= $newLine;
//        $raw .= parent::addLeftRight("Purchase:", number_format(TransferItem::get_total_purchase($this->tgl, $this->store), 2));
//        $raw .= $newLine;
//        $raw .= parent::addLeftRight("Return Purchase:", number_format(abs(TransferItem::get_total_returnpurchase($this->tgl, $this->store)), 2));
//        $raw .= $newLine;
//        $raw .= parent::addLeftRight("Taxes Purchase:", number_format(TransferItem::get_total_tax($this->tgl, $this->store), 2));
//        $raw .= $newLine;
        $raw .= "Tender totals";
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Tendered:", number_format($z ? $printz->tendered : $tendered, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Change:", number_format($z ? $printz->change_ : $sales->kembali, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Started:", number_format($z ? $printz->started : $started, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Added:", number_format($z ? $printz->added : $added, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Removed:", number_format($z ? $printz->removed : $removed, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Counted:", number_format($z ? $printz->counted : $counted, 2));
        $raw .= $newLine;
        $status = $z ? $printz->status_ : $collected - $counted;
        if ($status >= 0) {
            $raw .= parent::addLeftRight("Short:", number_format($status, 2));
        } else {
            $raw .= parent::addLeftRight("Over:", number_format(-$status, 2));
        }
        $raw .= $newLine;
        if ($this->tender != null) {
            $raw .= $newLine;
            $raw .= "Tenders";
            $raw .= $newLine;
            $raw .= parent::fillWithChar("-");
            $raw .= $newLine;
//            $t = $this->tender;
            foreach ($printz->printzDetails as $td) {
                if ($td->cash == 1) {
                    $raw .= parent::addLeftRight($td->nama_bank . " [Started] : ", number_format($td->started, 2));
                    $raw .= $newLine;
                }
                $raw .= parent::addLeftRight($td->nama_bank . " [Added] : ", number_format($td->added, 2));
                $raw .= $newLine;
                $raw .= parent::addLeftRight($td->nama_bank . " [Collected] : ", number_format($td->collected, 2));
                $raw .= $newLine;
                $raw .= parent::addLeftRight($td->nama_bank . " [Removed] : ", number_format($td->removed, 2));
                $raw .= $newLine;
                $raw .= parent::addLeftRight($td->nama_bank . " [Counted] : ", number_format($td->counted, 2));
                $raw .= $newLine;
                $raw .= parent::addLeftRight($td->nama_bank . ($td->status_ >= 0 ? " [Over] : " : " [Short] : "),
                    number_format(abs($td->status_), 2));
                $raw .= $newLine;
                $raw .= $newLine;
            }
        }
        $raw .= $newLine;
        $raw .= $newLine;
        U::save_file(ReportPath . $filename . '.txt', $raw);
        return $raw;
    }
}