<?php
class GrupTransDetailController extends GxController {
	public function actionCreate() {
		$model = new GrupTransDetail;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['GrupTransDetail'][ $k ] = $v;
			}
			$model->attributes = $_POST['GrupTransDetail'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->grup_trans_detail_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'GrupTransDetail' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['GrupTransDetail'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['GrupTransDetail'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->grup_trans_detail_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->grup_trans_detail_id ) );
			}
		}
	}
	public function actionDelete( $grup_trans_id, $loket_id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg                  = 'Data berhasil dihapus.';
			$status               = true;
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				/** @var LoketDetil[] $ld */
				$ld = LoketDetil::model()->findAllByAttributes( [ 'loket_id' => $loket_id ] );
				foreach ( $ld as $r ) {
					/** @var GrupTransDetail[] $gtd */
					$d                  = GrupTransDetail::model()->findByAttributes( [
						'grup_trans_id'=> $grup_trans_id,
						'loket_detil_id' => $r->loket_detil_id
					] );
					$d->delete();
				}
				$transaction->commit();
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
//		if ( isset( $_POST['limit'] ) ) {
//			$limit = $_POST['limit'];
//		} else {
//			$limit = 20;
//		}
//		if ( isset( $_POST['start'] ) ) {
//			$start = $_POST['start'];
//		} else {
//			$start = 0;
//		}
		$criteria = new CDbCriteria();
		$criteria->addCondition( 'grup_trans_id = :grup_trans_id' );
		$criteria->params[':grup_trans_id'] = $_POST['grup_trans_id'];
//		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
//		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
//			$criteria->limit  = $limit;
//			$criteria->offset = $start;
//		}
		$model = GrupTransDetailView::model()->findAll( $criteria );
		$total = GrupTransDetailView::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}