<?php
class ReservasiViewController extends GxController {
	public function actionCreate() {
		$model = new ReservasiView;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['ReservasiView'][ $k ] = $v;
			}
			$model->attributes = $_POST['ReservasiView'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->reservasi_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'ReservasiView' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['ReservasiView'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['ReservasiView'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->reservasi_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->reservasi_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'ReservasiView' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$param    = [];
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		if ( isset( $_POST['reservasi_id'] ) ) {
			unset( $_POST['query'] );
			$criteria->addCondition( 'reservasi_id = :reservasi_id' );
			$param[':reservasi_id'] = $_POST['reservasi_id'];
		}
		if ( isset( $_POST['query'] ) ) {
			$criteria->addCondition( 'loket_id is not null' );
			$criteria->addCondition( 'nama like :nama', 'OR' );
			$criteria->addCondition( 'no_telp like :no_telp', 'OR' );
			$criteria->addCondition( 'ref like :ref', 'OR' );
			$criteria->addCondition( 'alamat like :alamat', 'OR' );
			$param[':nama']    = '%' . $_POST['query'] . '%';
			$param[':no_telp'] = '%' . $_POST['query'] . '%';
			$param[':ref']     = '%' . $_POST['query'] . '%';
			$param[':alamat']  = '%' . $_POST['query'] . '%';
		}
		$criteria->params = $param;
		$model            = ReservasiView::model()->findAll( $criteria );
		$total            = ReservasiView::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}