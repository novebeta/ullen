<?php
class GrupTransController extends GxController {
	public function actionCreate() {
		$model = new GrupTrans;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				foreach ( $_POST as $k => $v ) {
					if ( is_angka( $v ) ) {
						$v = get_number( $v );
					}
					$_POST['GrupTrans'][ $k ] = $v;
				}
				$model->attributes = $_POST['GrupTrans'];
				$command           = $model->dbConnection->createCommand( 'SELECT 1 + coalesce((SELECT max(no_urut)
			 FROM grup_trans WHERE date(tdate)=date(now()) AND jenis_tour_id = :jenis_tour_id ), 0)' );
				$no_urut           = $command->queryScalar( [
					':jenis_tour_id' => $model->jenis_tour_id
				] );
				$model->no_urut    = $no_urut;
				/** @var JenisTour $jenisTour */
				$jenisTour = JenisTour::model()->findByPk( $_POST['jenis_tour_id'] );
				if ( $jenisTour != null ) {
					$model->qty = $jenisTour->maks_grup;
				}
				$msg = "Data gagal disimpan.";
				if ( $model->save() ) {
					$status = true;
					$msg    = "Data berhasil di simpan dengan id " . $model->grup_trans_id;
				} else {
					$msg    .= " " . CHtml::errorSummary( $model );
					$status = false;
				}
				$transaction->commit();
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionPrint( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg      = 'Data berhasil diprint.';
			$status   = true;
			$no_urut  = '';
			$add_info = '';
			try {
				/** @var GrupTrans $loket */
				$loket = GrupTrans::model()->findByPk( $id );
				/** @var JenisTour $jenis */
				$jenis = JenisTour::model()->findByPk( $loket->jenis_tour_id );
				if ( $jenis != null ) {
					$no_urut = $jenis->kode . '-' . str_pad( $loket->no_urut, 3, '0', STR_PAD_LEFT );
				}
				$msg      = $loket->printGuide();
				$add_info = $loket->printAddInfo();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success'     => $status,
				'no_urut'     => $no_urut,
				'doublebaris' => BasePrint::fillWithChar( "=", 33 ),
				'msg'         => $msg,
				'addinfo'     => $add_info,
				'note'        => $loket->printNote()
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'GrupTrans' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['GrupTrans'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['GrupTrans'];
			/** @var JenisTour $jenisTour */
			$jenisTour = JenisTour::model()->findByPk( $_POST['jenis_tour_id'] );
			if ( $jenisTour != null ) {
				$model->qty = $jenisTour->maks_grup;
			}
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->grup_trans_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->grup_trans_id ) );
			}
		}
	}
	public function actionWaiting( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil diupdate.';
			$status = true;
			try {
				/** @var GrupTrans $grupTrans */
				$grupTrans = $this->loadModel( $id, 'GrupTrans' );
				if ( $grupTrans == null ) {
					throw new Exception( 'Setting grup tidak ditemukan.' );
				}
				$grupTrans->waiting = new CDbExpression( 'NOW()' );
				$grupTrans->prepare = new CDbExpression( 'null' );
				$grupTrans->start   = new CDbExpression( 'null' );
				$grupTrans->end_    = new CDbExpression( 'null' );
				if ( ! $grupTrans->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'GrupTrans' ) ) . CHtml::errorSummary( $grupTrans ) );
				}
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionPrepare( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil diupdate.';
			$status = true;
			try {
				/** @var GrupTrans $grupTrans */
				$grupTrans = $this->loadModel( $id, 'GrupTrans' );
				if ( $grupTrans == null ) {
					throw new Exception( 'Setting grup tidak ditemukan.' );
				}
				$grupTrans->prepare = new CDbExpression( 'NOW()' );
				$grupTrans->start   = new CDbExpression( 'null' );
				$grupTrans->end_    = new CDbExpression( 'null' );
				if ( ! $grupTrans->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'GrupTrans' ) ) . CHtml::errorSummary( $grupTrans ) );
				}
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionStart( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil diupdate.';
			$status = true;
			try {
				/** @var GrupTrans $grupTrans */
				$grupTrans = $this->loadModel( $id, 'GrupTrans' );
				if ( $grupTrans == null ) {
					throw new Exception( 'Setting grup tidak ditemukan.' );
				}
				$grupTrans->start = new CDbExpression( 'NOW()' );
				$grupTrans->end_  = new CDbExpression( 'null' );
				if ( ! $grupTrans->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'GrupTrans' ) ) . CHtml::errorSummary( $grupTrans ) );
				}
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionEnd( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil diupdate.';
			$status = true;
			try {
				/** @var GrupTrans $grupTrans */
				$grupTrans = $this->loadModel( $id, 'GrupTrans' );
				if ( $grupTrans == null ) {
					throw new Exception( 'Setting grup tidak ditemukan.' );
				}
				$grupTrans->end_ = new CDbExpression( 'NOW()' );
				if ( ! $grupTrans->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'GrupTrans' ) ) . CHtml::errorSummary( $grupTrans ) );
				}
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
//		$criteria->select = 'gt.no_urut,gt.guide_id,gt.grup_trans_id,gt.jenis_tour_id,gt.lang_tour_id,
//		gt.qty,gt.`start`,gt.end_,TIME_FORMAT(gt.waktu_masuk,"%H:%i") AS waktu_masuk';
//		$criteria->alias  = "gt";
		if ( isset( $_POST['showended'] ) && $_POST['showended'] == 0 ) {
			$criteria->addCondition( 'end_ is NULL AND date(tdate) = date(now())' );
		} else {
			$criteria->addCondition( 'end_ is NOT NULL AND start is NOT NULL' );
			$criteria->order = 'tdate desc';
		}
//		$criteria->addCondition( 'tdate = now()' );
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = GrupTransView::model()->findAll( $criteria );
		$total = GrupTransView::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}