<?php
class TiketTransController extends GxController {
	public function actionCreate() {
		$model = new TiketTrans;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['TiketTrans'][ $k ] = $v;
			}
			$model->attributes = $_POST['TiketTrans'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->tiket_trans_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'TiketTrans' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['TiketTrans'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['TiketTrans'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->tiket_trans_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->tiket_trans_id ) );
			}
		}
	}
	public function actionVoucher() {
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$id          = $_POST['id'];
			$msg         = 'Data berhasil ditambahakan.';
			$status      = true;
			$transaction = Yii::app()->db->beginTransaction();
			try {
				/** @var GrupTrans $grupTrans */
				$grupTrans = $this->loadModel( $id, 'GrupTrans' );
				if ( $grupTrans == null ) {
					$msg = "Grup transaksi tidak ditemukan.";
				}
				$vouchers = CJSON::decode( $_POST['vouchers'] );
				foreach ( $vouchers as $vou ) {
					$gtd                 = new GrupTransDetail;
					$gtd->grup_trans_id  = $id;
					$gtd->loket_detil_id = $vou['loket_detil_id'];
					$gtd->qty            = $vou['qty'];
					if ( ! $gtd->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'GrupTransDetail' ) ) . CHtml::errorSummary( $gtd ) );
					}
				}
				$transaction->commit();
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria        = new CDbCriteria();
		$criteria->alias = "tt";
		$criteria->join  = "INNER JOIN loket_detil AS ld ON tt.loket_detil_id = ld.loket_detil_id";
		if ( isset( $_POST['loket_id'] ) ) {
			$criteria->addCondition( 'ld.loket_id = :loket_id' );
			$criteria->params[':loket_id'] = $_POST['loket_id'];
		}
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = TiketTrans::model()->findAll( $criteria );
		$total = TiketTrans::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionListTicket() {
		$criteria = new CDbCriteria();
		if ( isset( $_POST['loket_id'] ) ) {
			$criteria->addCondition( 'loket_id = :loket_id' );
			$criteria->params[':loket_id'] = $_POST['loket_id'];
		}
		$model = ListTicket::model()->findAll( $criteria );
		$total = ListTicket::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}