<?php
class ReservasiController extends GxController {
	public function actionCreate() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$is_new               = $_POST['mode'] == 0;
			$detils               = CJSON::decode( $_POST['detil'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				if ( ! $is_new ) {
					ReservasiDetil::model()->deleteAll( 'reservasi_id = :reservasi_id', [ ':reservasi_id' => $_POST['id'] ] );
				}
				/** @var Reservasi $model */
				$model                   = $is_new ? new Reservasi : $this->loadModel( $_POST['id'], 'Reservasi' );
				$model->member_id        = $_POST['member_id'];
				$model->nama             = $_POST['nama'];
				$model->no_telp          = $_POST['no_telp'];
				$model->alamat           = $_POST['alamat'];
				$model->jenis_tour_id    = $_POST['jenis_tour_id'];
				$model->lang_tour_id     = $_POST['lang_tour_id'];
				$model->note_            = $_POST['note_'];
				$model->status_bayar            = $_POST['status_bayar'];
				$model->total            = get_number( $_POST['total'] );
				$model->uang_muka        = get_number( $_POST['uang_muka'] );
				$model->sub_total        = get_number( $_POST['sub_total'] );
				$model->kategori_cust_id = $_POST['kategori_cust_id'];
				$model->waktu_tour_date  = $_POST['waktu_tour_date'];
				$model->waktu_tour_time  = $_POST['waktu_tour_time'];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $model ) );
				}
				$model->refresh();
				foreach ( $detils as $detil ) {
					$reservasi_detil               = new ReservasiDetil;
					$reservasi_detil->reservasi_id = $model->reservasi_id;
					$reservasi_detil->produk_id    = $detil['produk_id'];
					$reservasi_detil->qty          = get_number( $detil['qty'] );
					$reservasi_detil->harga        = get_number( $detil['harga'] );
//					$reservasi_detil->discrp         = get_number( $detil['discrp'] );
					$reservasi_detil->sub_total = get_number( $detil['sub_total'] );
//					$reservasi_detil->vatrp          = get_number( $detil['vatrp'] );
//					$reservasi_detil->vat            = get_number( $detil['vat'] );
//					$reservasi_detil->tot_qty_harga  = get_number( $detil['tot_qty_harga'] );
//					$reservasi_detil->tot_after_disc = get_number( $detil['tot_after_disc'] );
					if ( ! $reservasi_detil->save() ) {
						throw new Exception( t( 'save.model.fail', 'app', array( '{model}' => 'Reservasi Detail' ) ) . CHtml::errorSummary( $reservasi_detil ) );
					}
				}
				$msg = t( 'save.success', 'app' );
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg//,
				//'print' => $print
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'Reservasi' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Reservasi'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['Reservasi'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->reservasi_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->reservasi_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'Reservasi' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria         = new CDbCriteria();
		$criteria->select = 'r.reservasi_id,r.nama,r.member_id,r.jenis_tour_id,r.lang_tour_id,
r.waktu_reservasi,r.waktu_tour_date,r.note_,r.total,r.uang_muka,r.sub_total,r.user_id,r.tdate,
r.kategori_cust_id,TIME_FORMAT(r.waktu_tour_time,"%H:%i") as waktu_tour_time,r.status_bayar';
		$criteria->alias  = 'r';
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$criteria->order = 't.tdate DESC';
		$model = Reservasi::model()->findAll( $criteria );
		$total = Reservasi::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}