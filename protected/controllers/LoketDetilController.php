<?php
class LoketDetilController extends GxController {
	public function actionCreate() {
		$model = new LoketDetil;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['LoketDetil'][ $k ] = $v;
			}
			$model->attributes = $_POST['LoketDetil'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->loket_detil_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'LoketDetil' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['LoketDetil'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['LoketDetil'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->loket_detil_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->loket_detil_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'LoketDetil' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		$criteria = new CDbCriteria();
		if ( isset( $_POST['reservasi_id'] ) ) {
			$criteria->addCondition( 'reservasi_id = :reservasi_id' );
			$criteria->params[':reservasi_id'] = $_POST['reservasi_id'];
			$model                             = ReservasiDetil::model()->findAll( $criteria );
			$total                             = ReservasiDetil::model()->count( $criteria );
			$this->renderJson( $model, $total );
		} else if ( isset( $_POST['loket_id'] ) ) {
			$criteria->addCondition( 'loket_id = :loket_id' );
			$criteria->params[':loket_id'] = $_POST['loket_id'];
			$model                         = LoketDetailView::model()->findAll( $criteria );
			$total                         = LoketDetailView::model()->count( $criteria );
			$this->renderJson( $model, $total );
		} else if ( isset( $_POST['parent_loket_id'] ) ) {
			$criteria->addCondition( 'loket_id = :loket_id' );
			$criteria->params[':loket_id'] = $_POST['parent_loket_id'];
			$model                         = LoketDetailSum::model()->findAll( $criteria );
			$total                         = LoketDetailSum::model()->count( $criteria );
			$this->renderJson( $model, $total );
		}
	}
}