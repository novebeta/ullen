<?php
class LoketController extends GxController {
	public function actionCreate() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$is_new               = $_POST['mode'] == 0;
			$detils               = CJSON::decode( $_POST['detil'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				if ( ! $is_new ) {
					LoketDetil::model()->deleteAll( 'loket_id = :loket_id', [ ':loket_id' => $_POST['id'] ] );
					MemberPointTrans::model()->deleteAll( 'trans_no = :trans_no AND trans_tipe = :trans_tipe',
						[ ':trans_no' => $_POST['id'], ':trans_tipe' => PENJUALAN ] );
				}
				/** @var Loket $model */
				$model = $is_new ? new Loket : $this->loadModel( $_POST['id'], 'Loket' );
				if ( $is_new ) {
					$model->ref = new CDbExpression( 'fnc_ref_nota(NOW())' );
				}
				$model->reservasi_id     = $_POST['reservasi_id'];
				$model->member_id        = $_POST['member_id'];
				$model->nama             = $_POST['nama'];
				$model->no_telp          = $_POST['no_telp'];
				$model->alamat           = $_POST['alamat'];
				$model->jenis_tour_id    = $_POST['jenis_tour_id'];
				$model->lang_tour_id     = $_POST['lang_tour_id'];
				$model->note_            = $_POST['note_'];
				$model->total            = get_number( $_POST['total'] );
				$model->uang_muka        = get_number( $_POST['uang_muka'] );
				$model->sub_total        = get_number( $_POST['sub_total'] );
				$model->bayar            = get_number( $_POST['bayar'] );
				$model->kembalian        = get_number( $_POST['kembalian'] );
				$model->tipe_tamu        = $_POST['tipe_tamu'];
				$model->kategori_cust_id = $_POST['kategori_cust_id'];
				$model->waktu_tour_date  = $_POST['waktu_tour_date'];
				$model->waktu_tour_time  = $_POST['waktu_tour_time'];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $model ) );
				}
				$model->refresh();
				if ( $model->reservasi_id != null ) {
					$reservasi = Reservasi::model()->findByPk( $model->reservasi_id );
					if ( $reservasi != null ) {
						$reservasi->loket_id = $model->loket_id;
						if ( ! $reservasi->save() ) {
							throw new Exception( t( 'save.model.fail', 'app',
									array( '{model}' => 'Reservasi' ) ) . CHtml::errorSummary( $reservasi ) );
						}
					}
				}
				$point_all = 0;
				foreach ( $detils as $detil ) {
					$reservasi_detil            = new LoketDetil;
					$reservasi_detil->loket_id  = $model->loket_id;
					$reservasi_detil->produk_id = $detil['produk_id'];
					$reservasi_detil->qty       = get_number( $detil['qty'] );
					$reservasi_detil->harga     = get_number( $detil['harga'] );
//					$reservasi_detil->discrp         = get_number( $detil['discrp'] );
					$reservasi_detil->sub_total = get_number( $detil['sub_total'] );
//					$reservasi_detil->vatrp          = get_number( $detil['vatrp'] );
//					$reservasi_detil->vat            = get_number( $detil['vat'] );
//					$reservasi_detil->tot_qty_harga  = get_number( $detil['tot_qty_harga'] );
//					$reservasi_detil->tot_after_disc = get_number( $detil['tot_after_disc'] );
					if ( ! $reservasi_detil->save() ) {
						throw new Exception( t( 'save.model.fail', 'app', array( '{model}' => 'Reservasi Detail' ) ) . CHtml::errorSummary( $reservasi_detil ) );
					}
					U::add_stock_moves( PENJUALAN, $model->loket_id, date( $model->tdate ),
						$reservasi_detil->produk_id, $reservasi_detil->qty, $model->ref,
						0, '' );
					/** @var Produk $produk */
					$produk = Produk::model()->findByPk( $reservasi_detil->produk_id );
					if ( $produk->checkStockMove() < 0 ) {
						throw new Exception( 'Stok tiket tidak cukup.' );
					}
					if ( $model->member_id != null ) {
						/** @var Member $member */
						$member                 = Member::model()->findByPk( $model->member_id );
						$point                  = $produk->getPoint( $member->member_kategori_id );
						$reservasi_detil->point = $point * $reservasi_detil->qty;
						$point_all              += $reservasi_detil->point;
					}
					if ( ! $reservasi_detil->save() ) {
						throw new Exception( t( 'save.model.fail', 'app', array( '{model}' => 'Reservasi Detail' ) ) . CHtml::errorSummary( $reservasi_detil ) );
					}
				}
				$model->total_point = $point_all;
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $model ) );
				}
				if($point_all > 0) {
					$pt             = new MemberPointTrans;
					$pt->ref        = $model->ref;
					$pt->tgl        = $model->tdate;
					$pt->trans_tipe = PENJUALAN;
					$pt->trans_no   = $model->loket_id;
					$pt->point      = $model->total_point;
					if ( ! $pt->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $pt ) );
					} else {
						$pt->uploadPoint();
					}
				}
				$msg = t( 'save.success', 'app' );
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg//,
				//'print' => $print
			) );
			Yii::app()->end();
		}
	}
	public function actionCreateRetur() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$is_new               = $_POST['mode'] == 0;
			$detils               = CJSON::decode( $_POST['detil'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				if ( ! $is_new ) {
					LoketDetil::model()->deleteAll( 'loket_id = :loket_id', [ ':loket_id' => $_POST['id'] ] );
				}
				/** @var Loket $model */
				$model = $is_new ? new Loket : $this->loadModel( $_POST['id'], 'Loket' );
				if ( $is_new ) {
					$model->ref = new CDbExpression( 'fnc_ref_nota_retur(NOW())' );
				}
				$model->parent_loket_id  = $_POST['parent_loket_id'];
				$model->member_id        = $_POST['member_id'];
				$model->nama             = $_POST['nama'];
				$model->no_telp          = $_POST['no_telp'];
				$model->alamat           = $_POST['alamat'];
				$model->jenis_tour_id    = $_POST['jenis_tour_id'];
				$model->lang_tour_id     = $_POST['lang_tour_id'];
				$model->note_            = $_POST['note_'];
				$model->total            = - ( get_number( $_POST['total'] ) );
				$model->uang_muka        = 0;
				$model->sub_total        = - ( get_number( $_POST['sub_total'] ) );
				$model->bayar            = - ( get_number( $_POST['total'] ) );
				$model->kembalian        = 0;
				$model->tipe_tamu        = $_POST['tipe_tamu'];
				$model->kategori_cust_id = $_POST['kategori_cust_id'];
				$model->waktu_tour_date  = $_POST['waktu_tour_date'];
				$model->waktu_tour_time  = $_POST['waktu_tour_time'];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $model ) );
				}
				$model->refresh();
				foreach ( $detils as $detil ) {
					$reservasi_detil            = new LoketDetil;
					$reservasi_detil->loket_id  = $model->loket_id;
					$reservasi_detil->produk_id = $detil['produk_id'];
					$reservasi_detil->qty       = - ( get_number( $detil['qty'] ) );
					$reservasi_detil->harga     = get_number( $detil['harga'] );
//					$reservasi_detil->discrp         = get_number( $detil['discrp'] );
					$reservasi_detil->sub_total = - ( get_number( $detil['sub_total'] ) );
//					$reservasi_detil->vatrp          = get_number( $detil['vatrp'] );
//					$reservasi_detil->vat            = get_number( $detil['vat'] );
//					$reservasi_detil->tot_qty_harga  = get_number( $detil['tot_qty_harga'] );
//					$reservasi_detil->tot_after_disc = get_number( $detil['tot_after_disc'] );
					if ( ! $reservasi_detil->save() ) {
						throw new Exception( t( 'save.model.fail', 'app', array( '{model}' => 'Reservasi Detail' ) ) . CHtml::errorSummary( $reservasi_detil ) );
					}
				}
				$msg = t( 'save.success', 'app' );
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg//,
				//'print' => $print
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'Loket' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Loket'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['Loket'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->loket_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->loket_id ) );
			}
		}
	}
	public function actionVoucher( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg                  = 'Tiket berhasil di buat..';
			$status               = true;
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				/** @var Loket $loket */
				$loket = $this->loadModel( $id, 'Loket' );
				if ( $loket == null ) {
					throw new Exception( 'Nota tidak ditemukan.' );
				}
				if ( $loket->tgl_voucher == null ) {
//					throw new Exception( 'Tiket sudah di buat.' );
					foreach ( $loket->loketDetils as $detil ) {
						$nama_produk = '';
						/** @var Produk $produk */
						$produk = $detil->getProduk();
						if ( ! $produk->voucher ) {
							break;
						}
						$tahun = SysPrefs::get_val( 'tahun_tiket' );
						for ( $i = 0; $i < $detil->qty; $i ++ ) {
							$min = TiketTrans::getRefMinTiket( $detil->produk_id, $tahun );
							if ( $min === false ) {
								throw new Exception( 'Stok tiket habis.' );
							}
							$tt = TiketTrans::model()->find( 'ref = :ref AND produk_id = :produk_id AND tahun = :tahun', [
								':ref'       => $min,
								':produk_id' => $detil->produk_id,
								':tahun'     => $tahun
							] );
							if ( $tt != null ) {
								$tt->loket_detil_id = $detil->loket_detil_id;
								if ( ! $tt->save() ) {
									throw new Exception( t( 'save.model.fail', 'app',
											array( '{model}' => 'TiketTrans' ) ) . CHtml::errorSummary( $tt ) );
								}
							} else {
								if ( $produk != null ) {
									$nama_produk = $produk->nama;
								}
								if ( $min == null ) {
									throw new Exception( $nama_produk . ' => tidak ada stok tiket.' );
								} else {
									throw new Exception( $nama_produk . ' => Tiket trans ref ' . $min . ' tidak ditemukan.' );
								}
							}
						}
					}
					$loket->tgl_voucher = new CDbExpression( 'NOW()' );
					if ( ! $loket->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Nota' ) ) . CHtml::errorSummary( $loket ) );
					}
				}
				$transaction->commit();
				$msg = '';
				foreach ( $loket->loketDetils as $detil ) {
					/** @var Produk $produk */
					$produk = $detil->getProduk();
					if ( ! $produk->voucher ) {
						break;
					}
					$tiket = $detil->getTicketMinMax();
					if ( $tiket[0]['min'] == $tiket[0]['max'] ) {
						$tiket_label = $tiket[0]['min'];
					} else {
						$tiket_label = $tiket[0]['min'] . " - " . $tiket[0]['max'];
					}
					$msg .= $detil->getProduk()->nama . " : " . $tiket_label . "</br>";
				}
				if ( $msg == '' ) {
					$msg = 'Tidak ada tiket yang di keluarkan.';
				}
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = Loket::model()->findAll( $criteria );
		$total = Loket::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionPrint( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil diprint.';
			$kode   = '';
			$status = true;
			try {
				/** @var Loket $loket */
				$loket = Loket::model()->findByPk( $id );
				/** @var JenisTour $jenis */
				$jenis         = JenisTour::model()->findByPk( $loket->jenis_tour_id );
				$kode          = $jenis->kode;
				$cust_kat      = KategoriCust::model()->findByPk( $loket->kategori_cust_id );
				$cust_kat_nama = $cust_kat->nama;
				$msg           = $loket->printReceipt();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success'     => $status,
				'tgl'         => 'Tgl ' . sql2date( $loket->tdate ),
				'masuk'       => 'Perkiraan Masuk ' . $loket->waktu_tour_time,
				'ref'         => $loket->ref,
				'kat'         => $cust_kat_nama,
				'doublebaris' => BasePrint::fillWithChar( "=" ),
				'singlebaris' => BasePrint::fillWithChar( "-" ),
				'tour'        => '   No Tour ' . $kode . '-',
				'tamu'        => 'Jumlah ' . $loket->getTotalTamu() . ' Orang',
				'msg'         => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
}