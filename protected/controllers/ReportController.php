<?php
class ReportController extends GxController {
	public function actionPrintSerahTerimaLoket() {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil diprint.';
			$status = true;
			try {
				$newLine = "\r\n";
				$raw     = "ULLEN SENTALU";
				$raw     .= $newLine;
				$raw     .= "LAPORAN & SERAH TERIMA LOKET";
				$raw     .= $newLine;
				$raw     .= BasePrint::fillWithChar( "=" );
				$raw     .= $newLine;
				$raw     .= BasePrint::addHeaderSales( "Tanggal", sql2date( $_POST['tgl'], "dd-MMM-yyyy" ) );
				$raw     .= $newLine;
				$raw     .= BasePrint::fillWithChar( "-" );
				$raw     .= $newLine;
				$raw     .= $newLine;
				$raw     .= "PENGUNJUNG";
				$raw     .= $newLine;
				$raw     .= "NO. KATG TIKET     INDV      GRUP         TOTAL";
				$raw     .= $newLine;
				$raw     .= BasePrint::fillWithChar( "-" );
				$raw     .= $newLine;
				$disc    = 0;
				$comm    = Yii::app()->db->createCommand( "
				SELECT
					p.nama,
					p.kode_depan,
					ld.produk_id,
					ld.harga,
					sum( ld.qty ) qty,
					Sum( ld.sub_total ) total,
					sum( IF ( l.tipe_tamu = 1, ld.qty, 0 ) ) AS grup,
					sum( IF ( l.tipe_tamu = 0, ld.qty, 0 ) ) AS indv	
				FROM
					loket AS l
					INNER JOIN loket_detil AS ld ON ld.loket_id = l.loket_id
					INNER JOIN produk AS p ON ld.produk_id = p.produk_id
				GROUP BY					
					ld.produk_id,
					ld.harga" );
				$detils  = $comm->queryAll();
				$no_urut = 1;
				$qty     = $total = $totaldp = $indv_tot = $grup_tot = 0;
				foreach ( $detils as $d ) {
					$raw .= BasePrint::addItemSerahTerima( $no_urut . '.', $d['nama'],
						number_format( $d['indv'] ), number_format( $d['grup'] ),
						number_format( $d['indv'] + $d['grup'] ) );
//					$qty      += $d['qty'];
//					$total    += $d['total'];
//					$raw      .= $newLine;
//					$raw      .= BasePrint::addPengjung( "Tamu INDV: " . number_format( $d['indv'] ),
//						"    GRUP: " . number_format( $d['grup'] ) );
					$indv_tot += $d['indv'];
					$grup_tot += $d['grup'];
					$raw      .= $newLine;
					$no_urut ++;
				}
				$raw .= $newLine;
				$raw .= BasePrint::fillWithChar( "-" );
				$raw .= $newLine;
				$raw .= BasePrint::addItemSerahTerima( '   ', 'TOTAL',
					number_format( $indv_tot ), number_format( $grup_tot ),
					number_format( $indv_tot + $grup_tot ) );
//				$raw .= BasePrint::addTotalPengunjung( "Total", number_format( $qty ), number_format( $total ) );
//				$raw .= $newLine;
//				$raw .= BasePrint::addPengjung( "Tamu INDV: " . number_format( $indv_tot ),
//					"    GRUP: " . number_format( $grup_tot ) );
				$raw     .= $newLine;
				$raw     .= $newLine;
				$raw     .= "PEMBAYARAN TIKET";
				$raw     .= $newLine;
				$raw     .= "NO. KATG TIKET      QTY     HARGA         TOTAL";
				$raw     .= $newLine;
				$raw     .= BasePrint::fillWithChar( "-" );
				$raw     .= $newLine;
				$no_urut = 1;
				foreach ( $detils as $d ) {
					$raw   .= BasePrint::addItemSerahTerima( $no_urut . '.', $d['nama'],
						number_format( $d['qty'] ), number_format( $d['harga'] ),
						number_format( $d['total'] ) );
					$qty   += $d['qty'];
					$total += $d['total'];
					$raw   .= $newLine;
//					$raw      .= BasePrint::addPengjung( "Tamu INDV: " . number_format( $d['indv'] ),
//						"    GRUP: " . number_format( $d['grup'] ) );
//					$indv_tot += $d['indv'];
//					$grup_tot += $d['grup'];
//					$raw      .= $newLine;
					$no_urut ++;
				}
				$raw .= $newLine;
				$raw .= BasePrint::fillWithChar( "-" );
				$raw .= $newLine;
				$raw .= BasePrint::addTotalPengunjung( "Total", number_format( $qty ), number_format( $total ) );
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= "PEMBAYARAN DP";
				$raw .= $newLine;
				$raw .= BasePrint::fillWithChar( "=" );
				$raw .= $newLine;
				$raw .= "TGL       CASH/TRF  ROMBONGAN           NOMINAL";
				$raw .= $newLine;
				$raw .= BasePrint::fillWithChar( "-" );
				$raw .= $newLine;
				/** @var ReservasiView[] $rsv */
				$rsv = ReservasiView::model()->findAll('date(waktu_reservasi) = :dt',[':dt' => $_POST['tgl']]);
				foreach ( $rsv as $line ) {
					$raw     .= BasePrint::addItemPembayaranDP( sql2date( $line->waktu_reservasi, 'dd/MM/yy' ),
						$line->tipe_bayar, $line->nama, number_format( $line->uang_muka ) );
					$raw     .= $newLine;
					$totaldp += floatval( $line->uang_muka );
				}
				$raw     .= $newLine;
				$raw     .= BasePrint::addLeftRight( "", BasePrint::fillWithChar( "-", 17 ) );
				$raw     .= $newLine;
				$raw     .= BasePrint::addLeftRight( "Total DP", number_format( $totaldp ) );
				$raw     .= $newLine;
				$raw     .= BasePrint::addLeftRight( "GRAND TOTAL", number_format( $totaldp + $total ) );
				$raw     .= $newLine;
				$raw     .= $newLine;
				$no_urut = 1;
				$raw     .= "TIKET FISIK";
				$raw     .= $newLine;
				$raw     .= BasePrint::fillWithChar( "=" );
				$raw     .= $newLine;
				foreach ( $detils as $d ) {
					$comm = Yii::app()->db->createCommand( "
				SELECT
					ld.produk_id,
					Min( tt.ref ) AS awal,
					Max( tt.ref ) AS akhir 
				FROM
					loket AS l
					INNER JOIN loket_detil AS ld ON ld.loket_id = l.loket_id
					LEFT JOIN tiket_trans AS tt ON tt.loket_detil_id = ld.loket_detil_id 
				WHERE ld.produk_id = :produk_id" );
					$tt   = $comm->queryAll( true, [ ':produk_id' => $d['produk_id'] ] );
					$comm1 = Yii::app()->db->createCommand( "
				SELECT
						tt.produk_id,
					Min( tt.ref ) AS awal,
					Max( tt.ref ) AS akhir 
				FROM
					tiket_trans AS tt 
				WHERE tt.produk_id = :produk_id and tt.loket_detil_id is null" );
					$tt1   = $comm1->queryAll( true, [ ':produk_id' => $d['produk_id'] ] );
					$raw  .= substr( str_pad( $no_urut . '.', 3, "0", STR_PAD_LEFT ), 0, 3 );
					$raw  .= $d['nama'];
					$raw  .= $newLine;
					$raw  .= BasePrint::addTiket( 'KELUAR', 'AWAL:', $tt[0]['awal'], '   AKHIR:', $tt[0]['akhir'] );
					$raw  .= $newLine;
					$raw  .= BasePrint::addTiket( 'STOK', 'AWAL:', $tt1[0]['awal'], '   AKHIR:', $tt1[0]['akhir'] );
					$raw  .= $newLine;
					$raw  .= BasePrint::addTiket( 'PAX', 'TDK UTUH:','', '   UTUH:', '');
					$raw  .= $newLine;
					$no_urut ++;
				}
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= "NOTE :";
				$raw .= $newLine;
				$raw .= BasePrint::fillWithChar( "_" );
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= BasePrint::fillWithChar( "_" );
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= BasePrint::fillWithChar( "_" );
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= BasePrint::fillWithChar( "_" );
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= BasePrint::setCenter( 'Menyerahkan,', CHARLENGTHRECEIPT / 2 );
				$raw .= BasePrint::setCenter( 'Penerima,', CHARLENGTHRECEIPT / 2 );
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= $newLine;
				$raw .= BasePrint::setCenter( '..............', CHARLENGTHRECEIPT / 2 );
				$raw .= BasePrint::setCenter( '..............', CHARLENGTHRECEIPT / 2 );
				$raw .= $newLine;
				$raw .= $newLine;
				$msg = $raw;
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
//			file_put_contents( 'printSerahTerima', $raw );
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
}