<?php
class LoketViewController extends GxController {
	public function actionCreate() {
		$model = new LoketView;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['LoketView'][ $k ] = $v;
			}
			$model->attributes = $_POST['LoketView'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->loket_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'LoketView' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['LoketView'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['LoketView'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->loket_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->loket_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'LoketView' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
//		if ( isset( $_POST['limit'] ) ) {
//			$limit = $_POST['limit'];
//		} else {
//			$limit = 20;
//		}
//		if ( isset( $_POST['start'] ) ) {
//			$start = $_POST['start'];
//		} else {
//			$start = 0;
//		}
		$criteria = new CDbCriteria();
//		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
//		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
//			$criteria->limit  = $limit;
//			$criteria->offset = $start;
//		}
		if ( isset ( $_POST['tgl'] ) ) {
			$criteria->addCondition( 'date(tdate) = :tgl' );
			$criteria->params['tgl'] = $_POST['tgl'];
		}
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'nota' ) ) {
			$criteria->addCondition( 'parent_loket_id is null' );
		}
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'retur' ) ) {
			$criteria->addCondition( 'parent_loket_id is not null' );
		}
		$criteria->order = 'tdate DESC';
		$model           = LoketView::model()->findAll( $criteria );
		$total           = LoketView::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}