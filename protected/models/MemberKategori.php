<?php
Yii::import('application.models._base.BaseMemberKategori');

class MemberKategori extends BaseMemberKategori
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->member_kategori_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->member_kategori_id = $uuid;
        }
        return parent::beforeValidate();
    }
}