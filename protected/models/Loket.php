<?php
Yii::import( 'application.models._base.BaseLoket' );
class Loket extends BaseLoket {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->loket_id == null ) {
			$command        = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid           = $command->queryScalar();
			$this->loket_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
	public function getTotalTamu() {
		$command = $this->dbConnection->createCommand( "SELECT Sum(ld.qty) FROM loket_detil AS ld
							WHERE loket_id = :loket_id;" );
		$uuid    = $command->queryScalar( [ ':loket_id' => $this->loket_id ] );
		return ( $uuid === false ) ? 0 : $uuid;
	}
	public function printReceipt() {
		$newLine = "\r\n";
//		$raw     = BasePrint::setCenter( SysPrefs::get_val( 'receipt_header0' ) );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_header1' ) );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_header2' ) );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_header3' ) );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_header4' ) );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_header5' ) );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::fillWithChar( "=" );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::addHeaderSales( "No. Nota", $this->ref );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::addHeaderSales( "Tgl Nota", sql2date( $this->tdate, "dd-MMM-yyyy" ) );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::addHeaderSales( "Nama", $this->nama );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::addHeaderSales( "Alamat", $this->alamat );
//		$raw     .= $newLine;
//		$raw     .= BasePrint::addHeaderSales( "Tour",
//			sql2date( $this->waktu_tour_date, "dd-MMM-yyyy" ) . ' ' . $this->waktu_tour_time );
//		$raw     .= $newLine;
//		/** @var LangTour $lang */
//		$lang = LangTour::model()->findByPk( $this->lang_tour_id );
//		$raw  .= BasePrint::addHeaderSales( "Bahasa", $lang->nama );
//		$raw  .= $newLine;
//		/** @var JenisTour $jenis */
//		$jenis = JenisTour::model()->findByPk( $this->jenis_tour_id );
//		$raw   .= BasePrint::addHeaderSales( "Jenis Tour", $jenis->nama );
//		$raw   .= $newLine;
//		$user  = Users::model()->findByPk( Yii::app()->getUser()->id );
//		$raw   .= BasePrint::addHeaderSales( "Kasir", $user->name );
//		$raw   .= $newLine;
//		$raw   .= BasePrint::fillWithChar( "-" );
//		$raw   .= $newLine;
//		$raw   .= "DESKRIPSI            QTY      HARGA        TOTAL";
//		$raw   .= $newLine;
//		$raw   .= "   NO TIKET";
//		$raw   .= $newLine;
//		$raw   .= BasePrint::fillWithChar( "-" );
//		$raw   .= $newLine;
//		$disc  = 0;
		$raw = '';
		foreach ( $this->loketDetils as $d ) {
			/** @var Produk $produk */
			$produk = Produk::model()->findByPk( $d->produk_id );
//			$raw    .= BasePrint::addItemCodeReceipt( $produk->nama,
//				number_format( $d->qty ), number_format( $d->harga ),
//				number_format( $d->sub_total ) );
			$raw .= '   ' . $produk->nama . ' ' . $d->qty;
			$raw .= $newLine;
//			$tiket = $d->getTicketMinMax();
//			if ( sizeof( $tiket ) == 1 ) {
//				$raw .= BasePrint::addItemNameReceipt( "[" . $tiket[0]['min'] . ' - ' . $tiket[0]['max'] . "]", 43, 3 );
//				$raw .= $newLine;
//			}
		}
//		$raw .= $newLine;
//		$raw .= BasePrint::fillWithChar( "-" );
//		$raw .= $newLine;
//		$raw .= $newLine;
//		$raw .= BasePrint::addLeftRight( "Total", number_format( ( $this->sub_total ), 2 ) );
//		$raw .= $newLine;
//		$raw .= BasePrint::addLeftRight( "Uang Muka", number_format( ( $this->uang_muka ), 2 ) );
//		$raw .= $newLine;
//		$raw .= BasePrint::addLeftRight( "Bayar", number_format( ( $this->total ), 2 ) );
//		$raw .= $newLine;
//		$raw .= BasePrint::addLeftRight( "", BasePrint::fillWithChar( "-", 17 ) );
//		$raw .= $newLine;
//		$raw .= $newLine;
//		$raw .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_footer0' ) );
//		$raw .= $newLine;
//		$raw .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_footer1' ) );
//		$raw .= $newLine;
//		$raw .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_footer2' ) );
//		$raw .= $newLine;
//		$raw .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_footer3' ) );
//		$raw .= $newLine;
//		$raw .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_footer4' ) );
//		$raw .= $newLine;
//		$raw .= BasePrint::setCenter( SysPrefs::get_val( 'receipt_footer5' ) );
//		$raw .= $newLine;
//		U::save_file( '/usr/local/lsws/default/html/ullenjs/protected/runtime/' . $this->nama . '.txt', $raw );
		return $raw;
	}
}