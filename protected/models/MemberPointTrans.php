<?php
Yii::import( 'application.models._base.BaseMemberPointTrans' );
class MemberPointTrans extends BaseMemberPointTrans {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->member_point_trans_id == null ) {
			$command                     = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                        = $command->queryScalar();
			$this->member_point_trans_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public function uploadPoint() {
		/** @var Users $user */
		$user   = Users::model()->findByPk( user()->getId() );
		$client = new \GuzzleHttp\Client();
		try {
			$response = $client->request( 'POST',
				'http://member.ullensentalu.com/index.php', [
					'query'       => [
						'access-token' => $user->access_token,
						'r'            => 'api/member/point'
					],
					'form_params' => $this->getAttributes()
				] );
			if ( $response->getStatusCode() == 200 ) {
				Yii::log( $response->getBody(), 'error', 'system.web.user' );
			}
		} catch ( \GuzzleHttp\Exception\GuzzleException $e ) {
			$response = $e->getResponse();
			if ( $response->getStatusCode() == '401' ) {
				$user->loginMember();
				$this->afterSave();
			}
		}
	}
}