<?php
Yii::import( 'application.models._base.BaseReservasi' );
class Reservasi extends BaseReservasi {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->reservasi_id == null ) {
			$command            = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid               = $command->queryScalar();
			$this->reservasi_id = $uuid;
		}
		if ( $this->waktu_reservasi == null ) {
			$this->waktu_reservasi = new CDbExpression( 'NOW()' );
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->ref == null ) {
			$this->ref = new CDbExpression( 'fnc_ref_reservasi(date(waktu_reservasi))' );
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
}