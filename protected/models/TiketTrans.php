<?php
Yii::import( 'application.models._base.BaseTiketTrans' );
class TiketTrans extends BaseTiketTrans {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->tiket_trans_id == null ) {
			$command              = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                 = $command->queryScalar();
			$this->tiket_trans_id = $uuid;
		}
		return parent::beforeValidate();
	}
	static public function generateTicket( $product_id ) {
		/** @var Produk $produk */
		$produk = Produk::model()->findByPk( $item_details->produk_id );
		if ( $produk == null ) {
			throw new Exception( "Produk tidak ditemukan." );
		}
		if ( ! $produk->voucher ) {
			countinue;
		}
		$kode_inc = trim( $produk->kode_belakang );
		$len      = strlen( $kode_inc );
		for ( $i = 0; $i < $item_details->qty; $i ++ ) {
			$tiketTrans                            = new TiketTrans;
			$tiketTrans->transfer_barang_detail_id = $item_details->transfer_barang_detail_id;
			$tiketTrans->ref                       = $produk->kode_depan . $kode_inc;
			$tiketTrans->produk_id                 = $produk->produk_id;
			if ( ! $tiketTrans->save() ) {
				throw new Exception( t( 'save.model.fail', 'app',
						array( '{model}' => 'Tiket Trans' ) ) . CHtml::errorSummary( $tiketTrans ) );
			}
			$valinc = intval( $kode_inc );
			$valinc ++;
			$kode_inc = str_pad( $valinc, $len, '0', STR_PAD_LEFT );
		}
		$produk->kode_belakang = $kode_inc;
		if ( ! $produk->save() ) {
			throw new Exception( t( 'save.model.fail', 'app',
					array( '{model}' => 'Produk' ) ) . CHtml::errorSummary( $produk ) );
		}
	}
	static public function getRefMinTiket( $product_id, $tahun ) {
		$command = Yii::app()->db->createCommand( "SELECT Min(tt.ref) AS ref
								FROM tiket_trans AS tt 
								WHERE loket_detil_id is null AND produk_id = :produk_id
								AND tahun = :tahun" );
		return $command->queryScalar( [ ':produk_id' => $product_id, ':tahun' => $tahun ] );
	}
}