<?php

/**
 * This is the model base class for the table "produk".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Produk".
 *
 * Columns in table "produk" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $produk_id
 * @property string $nama
 * @property string $harga
 * @property string $jenis_tour_id
 * @property string $kode_depan
 * @property string $kode_belakang
 * @property integer $voucher
 *
 */
abstract class BaseProduk extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'produk';
	}

	public static function representingColumn() {
		return 'nama';
	}

	public function rules() {
		return array(
			array('produk_id, nama', 'required'),
			array('voucher', 'numerical', 'integerOnly'=>true),
			array('produk_id, jenis_tour_id', 'length', 'max'=>36),
			array('nama', 'length', 'max'=>100),
			array('harga', 'length', 'max'=>30),
			array('kode_depan, kode_belakang', 'length', 'max'=>10),
			array('harga, jenis_tour_id, kode_depan, kode_belakang, voucher', 'default', 'setOnEmpty' => true, 'value' => null),
			array('produk_id, nama, harga, jenis_tour_id, kode_depan, kode_belakang, voucher', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'produk_id' => Yii::t('app', 'Produk'),
			'nama' => Yii::t('app', 'Nama'),
			'harga' => Yii::t('app', 'Harga'),
			'jenis_tour_id' => Yii::t('app', 'Jenis Tour'),
			'kode_depan' => Yii::t('app', 'Kode Depan'),
			'kode_belakang' => Yii::t('app', 'Kode Belakang'),
			'voucher' => Yii::t('app', 'Voucher'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('produk_id', $this->produk_id, true);
		$criteria->compare('nama', $this->nama, true);
		$criteria->compare('harga', $this->harga, true);
		$criteria->compare('jenis_tour_id', $this->jenis_tour_id, true);
		$criteria->compare('kode_depan', $this->kode_depan, true);
		$criteria->compare('kode_belakang', $this->kode_belakang, true);
		$criteria->compare('voucher', $this->voucher);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}