<?php

/**
 * This is the model base class for the table "loket_detil".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "LoketDetil".
 *
 * Columns in table "loket_detil" available as properties of the model,
 * followed by relations of table "loket_detil" available as properties of the model.
 *
 * @property string $loket_detil_id
 * @property string $loket_id
 * @property string $produk_id
 * @property integer $qty
 * @property string $harga
 * @property string $sub_total
 * @property integer $point
 *
 * @property Loket $loket
 */
abstract class BaseLoketDetil extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'loket_detil';
	}

	public static function representingColumn() {
		return 'harga';
	}

	public function rules() {
		return array(
			array('loket_detil_id', 'required'),
			array('qty, point', 'numerical', 'integerOnly'=>true),
			array('loket_detil_id, loket_id, produk_id', 'length', 'max'=>36),
			array('harga, sub_total', 'length', 'max'=>15),
			array('loket_id, produk_id, qty, harga, sub_total, point', 'default', 'setOnEmpty' => true, 'value' => null),
			array('loket_detil_id, loket_id, produk_id, qty, harga, sub_total, point', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'loket' => array(self::BELONGS_TO, 'Loket', 'loket_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'loket_detil_id' => Yii::t('app', 'Loket Detil'),
			'loket_id' => Yii::t('app', 'Loket'),
			'produk_id' => Yii::t('app', 'Produk'),
			'qty' => Yii::t('app', 'Qty'),
			'harga' => Yii::t('app', 'Harga'),
			'sub_total' => Yii::t('app', 'Sub Total'),
			'point' => Yii::t('app', 'Point'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('loket_detil_id', $this->loket_detil_id, true);
		$criteria->compare('loket_id', $this->loket_id);
		$criteria->compare('produk_id', $this->produk_id, true);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('harga', $this->harga, true);
		$criteria->compare('sub_total', $this->sub_total, true);
		$criteria->compare('point', $this->point);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}