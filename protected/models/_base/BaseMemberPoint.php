<?php

/**
 * This is the model base class for the table "member_point".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "MemberPoint".
 *
 * Columns in table "member_point" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $member_point_id
 * @property string $produk_id
 * @property string $member_kategori_id
 * @property integer $point
 *
 */
abstract class BaseMemberPoint extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'member_point';
	}

	public static function representingColumn() {
		return 'member_point_id';
	}

	public function rules() {
		return array(
			array('member_point_id', 'required'),
			array('point', 'numerical', 'integerOnly'=>true),
			array('member_point_id, produk_id, member_kategori_id', 'length', 'max'=>36),
			array('produk_id, member_kategori_id, point', 'default', 'setOnEmpty' => true, 'value' => null),
			array('member_point_id, produk_id, member_kategori_id, point', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'member_point_id' => Yii::t('app', 'Member Point'),
			'produk_id' => Yii::t('app', 'Produk'),
			'member_kategori_id' => Yii::t('app', 'Member Kategori'),
			'point' => Yii::t('app', 'Point'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('member_point_id', $this->member_point_id, true);
		$criteria->compare('produk_id', $this->produk_id, true);
		$criteria->compare('member_kategori_id', $this->member_kategori_id, true);
		$criteria->compare('point', $this->point);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}