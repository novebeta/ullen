<?php

/**
 * This is the model base class for the table "loket_retur_detil".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "LoketReturDetil".
 *
 * Columns in table "loket_retur_detil" available as properties of the model,
 * followed by relations of table "loket_retur_detil" available as properties of the model.
 *
 * @property string $loket_retur_detil_id
 * @property string $produk_id
 * @property integer $qty
 * @property string $harga
 * @property string $sub_total
 * @property string $loket_retur_id
 *
 * @property LoketRetur $loketRetur
 */
abstract class BaseLoketReturDetil extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'loket_retur_detil';
	}

	public static function representingColumn() {
		return 'harga';
	}

	public function rules() {
		return array(
			array('loket_retur_detil_id', 'required'),
			array('qty', 'numerical', 'integerOnly'=>true),
			array('loket_retur_detil_id, produk_id, loket_retur_id', 'length', 'max'=>36),
			array('harga, sub_total', 'length', 'max'=>15),
			array('produk_id, qty, harga, sub_total, loket_retur_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('loket_retur_detil_id, produk_id, qty, harga, sub_total, loket_retur_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'loketRetur' => array(self::BELONGS_TO, 'LoketRetur', 'loket_retur_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'loket_retur_detil_id' => Yii::t('app', 'Loket Retur Detil'),
			'produk_id' => Yii::t('app', 'Produk'),
			'qty' => Yii::t('app', 'Qty'),
			'harga' => Yii::t('app', 'Harga'),
			'sub_total' => Yii::t('app', 'Sub Total'),
			'loket_retur_id' => Yii::t('app', 'Loket Retur'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('loket_retur_detil_id', $this->loket_retur_detil_id, true);
		$criteria->compare('produk_id', $this->produk_id, true);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('harga', $this->harga, true);
		$criteria->compare('sub_total', $this->sub_total, true);
		$criteria->compare('loket_retur_id', $this->loket_retur_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}