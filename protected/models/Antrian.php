<?php
Yii::import( 'application.models._base.BaseAntrian' );
class Antrian extends BaseAntrian {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function primaryKey() {
		return 'grup_trans_id';
	}
}