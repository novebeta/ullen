<?php
Yii::import( 'application.models._base.BaseUsers' );
class Users extends BaseUsers {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public static function is_audit() {
		$id   = Yii::app()->user->getId();
		$user = Users::model()->findByPk( $id );
		return strtolower( substr( $user->user_id, 0, 3 ) ) === strtolower( SysPrefs::get_val( 'audit' ) ) ? 1 : 0;
	}
	public static function get_override( $user, $pass ) {
		$comm = Yii::app()->db->createCommand( "SELECT ns.id FROM nscc_users AS ns
        INNER JOIN nscc_security_roles AS nsc ON ns.security_roles_id = nsc.security_roles_id
        WHERE ns.`password` = :pass AND ns.user_id = :user AND nsc.sections LIKE '%209%'" );
		return $comm->queryScalar( array( ':pass' => $pass, ':user' => $user ) );
	}
	public static function get_access( $user, $pass, $section ) {
		$comm = Yii::app()->db->createCommand( "SELECT ns.id FROM nscc_users AS ns
        INNER JOIN nscc_security_roles AS nsc ON ns.security_roles_id = nsc.security_roles_id
        WHERE ns.`password` = :pass AND ns.user_id = :user AND nsc.sections LIKE :section" );
		return $comm->queryScalar( array( ':pass' => $pass, ':user' => $user, ':section' => '%' . $section . '%' ) );
	}
	public function beforeValidate() {
		if ( $this->id == null ) {
			$command  = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid     = $command->queryScalar();
			$this->id = $uuid;
		}
		return parent::beforeValidate();
	}
	public function is_available_role( $role ) {
		$sections = explode( ',', $this->securityRoles->sections );
		return in_array( $role, $sections );
	}
	public function loginMember() {
		$client = new \GuzzleHttp\Client();
		try {
			$response = $client->request( 'POST',
				'http://member.ullensentalu.com/index.php', [
					'query'       => [
						'r' => 'api/member/login'
					],
					'form_params' => [
						'email'    => $this->email,
						'password' => $this->password
					]
				] );
		} catch ( \GuzzleHttp\Exception\GuzzleException $e ) {
			Yii::log( $e->getMessage(), 'error', 'system.web.rest' );
			return false;
		}
		$jtoken = $response->getBody();
		Yii::log( $jtoken, 'error', 'system.web.rest' );
		$token              = CJSON::decode( $jtoken );
		$this->access_token = $token['access_token'];
		if ( ! $this->save() ) {
			Yii::log( CHtml::errorSummary( $this ), 'error', 'system.web.user' );
			return false;
		}
		return true;
	}
	public function sync() {
		$client = new \GuzzleHttp\Client();
		try {
			$response = $client->request( 'POST',
				'http://member.ullensentalu.com/index.php', [
					'query'       => [
						'r' => 'api/member/sync'
					],
					'form_params' => [
						'id'       => $this->id,
						'email'    => $this->email,
						'password' => $this->password
					]
				] );
			if ( $response->getStatusCode() == 200 ) {
				$hasil = CJSON::decode( $response->getBody() );
				return $hasil['status'];
			}
			return $response->getBody();
		} catch ( \GuzzleHttp\Exception\GuzzleException $e ) {
			$response = $e->getResponse();
			Yii::log( $e->getMessage(), 'error', 'system.web.rest' );
			return $e->getMessage();
		}
	}
}