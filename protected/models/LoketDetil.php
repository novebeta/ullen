<?php
Yii::import( 'application.models._base.BaseLoketDetil' );
class LoketDetil extends BaseLoketDetil {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->loket_detil_id == null ) {
			$command              = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                 = $command->queryScalar();
			$this->loket_detil_id = $uuid;
		}
		return parent::beforeValidate();
	}
	/**
	 * @return Produk|null
	 */
	public function getProduk(){
		return Produk::model()->findByPk($this->produk_id);
	}

	public function getTicketMinMax() {
		$command = $this->dbConnection
			->createCommand( "SELECT MIN(ref) as min,MAX(ref) as max FROM tiket_trans WHERE loket_detil_id = :loket_detil_id;" );
		return $command->queryAll( true, [ ':loket_detil_id' => $this->loket_detil_id ] );
	}
}