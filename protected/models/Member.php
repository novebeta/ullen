<?php
Yii::import( 'application.models._base.BaseMember' );
class Member extends BaseMember {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->member_id == null ) {
			$command         = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid            = $command->queryScalar();
			$this->member_id = $uuid;
		}
		if ( $this->no_member == null ) {
			$this->no_member = $this->generateNoMember( 16 );
		}
		return parent::beforeValidate();
	}
	function generateNoMember( $len = "15" ) {
		$better_token = $code = sprintf( "%0" . $len . "d", mt_rand( 1, str_pad( "", $len, "9" ) ) );
		return $better_token;
	}
	public function addMember() {
		/** @var Users $user */
		$user   = Users::model()->findByPk( user()->getId() );
		$client = new \GuzzleHttp\Client();
		try {
			$response = $client->request( 'POST',
				'http://member.ullensentalu.com/index.php', [
					'query'       => [
						'access-token' => $user->access_token,
						'r'            => 'api/member/add'
					],
					'form_params' => $this->getAttributes()
				] );
			if ( $response->getStatusCode() == 200 ) {
				$hasil = CJSON::decode( $response->getBody() );
				return $hasil['password'];
			}
			return $response->getBody();
		} catch ( \GuzzleHttp\Exception\GuzzleException $e ) {
			$response = $e->getResponse();
			Yii::log( $e->getMessage(), 'error', 'system.web.rest' );
			if ( $response->getStatusCode() == '401' || $response->getStatusCode() == '500' ) {
				$user->loginMember();
				$this->addMember();
			}
		}
	}
	public function editMember() {
		/** @var Users $user */
		$user   = Users::model()->findByPk( user()->getId() );
		$client = new \GuzzleHttp\Client();
		try {
			$response = $client->request( 'POST',
				'http://member.ullensentalu.com/index.php', [
					'query'       => [
						'access-token' => $user->access_token,
						'r'            => 'api/member/edit'
					],
					'form_params' => $this->getAttributes()
				] );
			if ( $response->getStatusCode() == 200 ) {
				$hasil = CJSON::decode( $response->getBody() );
				return $hasil['status'];
			}
		} catch ( \GuzzleHttp\Exception\GuzzleException $e ) {
			$response = $e->getResponse();
			if ( $response->getStatusCode() == '401' ) {
				$user->loginMember();
				$this->addMember();
			}
		}
	}
	public function resetPass() {
		/** @var Users $user */
		$user   = Users::model()->findByPk( user()->getId() );
		$client = new \GuzzleHttp\Client();
		try {
			$response = $client->request( 'POST',
				'http://member.ullensentalu.com/index.php', [
					'query'       => [
						'access-token' => $user->access_token,
						'r'            => 'api/member/resetpass'
					],
					'form_params' => $this->getAttributes()
				] );
			if ( $response->getStatusCode() == 200 ) {
				$hasil = CJSON::decode( $response->getBody() );
				return $hasil['password'];
			}
		} catch ( \GuzzleHttp\Exception\GuzzleException $e ) {
			$response = $e->getResponse();
			if ( $response->getStatusCode() == '401' ) {
				$user->loginMember();
				$this->addMember();
			}
		}
	}
}