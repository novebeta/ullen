<?php
Yii::import( 'application.models._base.BaseLoketView' );
class LoketView extends BaseLoketView {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function primaryKey() {
		return 'loket_id';
	}
}