<?php
Yii::import('application.models._base.BaseListTicket');

class ListTicket extends BaseListTicket
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function primaryKey() {
		return 'loket_detil_id';
	}
}