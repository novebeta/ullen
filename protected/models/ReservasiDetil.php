<?php
Yii::import('application.models._base.BaseReservasiDetil');

class ReservasiDetil extends BaseReservasiDetil
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->reservasi_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->reservasi_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}