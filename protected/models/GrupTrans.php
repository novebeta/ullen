<?php
Yii::import( 'application.models._base.BaseGrupTrans' );
class GrupTrans extends BaseGrupTrans {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->grup_trans_id == null ) {
			$command             = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                = $command->queryScalar();
			$this->grup_trans_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
	public function getTotalTamu() {
		$command = $this->dbConnection->createCommand( "SELECT SUM(qty) 
		FROM grup_trans_detail where grup_trans_id = :grup_trans_id;" );
		return $command->queryScalar( [ ':grup_trans_id' => $this->grup_trans_id ] );
	}
	public function printGuide() {
		$width   = 33;
		$newLine = "\r\n";
		$raw = "Jumlah " . $this->getTotalTamu() . " Orang";
		$raw .= $newLine;
		/** @var Guide $guide */
		$guide = Guide::model()->findByPk( $this->guide_id );
//		$raw   .= BasePrint::addHeaderSales( "ET", $guide->nama );
		$raw .= "ET " . $guide->nama;
		return $raw;
	}
	public function printAddInfo() {
		$width   = 33;
		$newLine = "\r\n";
		$raw = "Tgl " . sql2date( $this->tdate );
		$raw .= $newLine;
//		$raw .= BasePrint::addHeaderSales( "Masuk", $this->waktu_masuk );
		$raw .= "Masuk " . $this->waktu_masuk;
		$raw .= $newLine;
		/** @var LangTour $lang */
		$lang = LangTour::model()->findByPk( $this->lang_tour_id );
//		$raw  .= BasePrint::addHeaderSales( "Bahasa", $lang->nama );
		$raw .= 'Bahasa ' . $lang->nama;
		return $raw;
	}
	public function printNote() {
		$width   = 33;
		$newLine = "\r\n";
		$raw     = "NOTE :";
		$raw     .= $newLine;
		$raw     .= BasePrint::fillWithChar( "_", $width );
		$raw     .= $newLine;
		$raw     .= $newLine;
		$raw     .= BasePrint::fillWithChar( "_", $width );
		$raw     .= $newLine;
		$raw     .= $newLine;
		$raw     .= $newLine;
		$raw     .= $newLine;
		$raw     .= $newLine;
		$raw     .= BasePrint::setCenter( '(..................)', $width );
		$raw     .= $newLine;
		return $raw;
	}
}