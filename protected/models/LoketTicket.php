<?php
Yii::import( 'application.models._base.BaseLoketTicket' );
class LoketTicket extends BaseLoketTicket {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function primaryKey() {
		return 'loket_id';
	}
}