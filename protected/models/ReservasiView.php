<?php
Yii::import('application.models._base.BaseReservasiView');

class ReservasiView extends BaseReservasiView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function primaryKey()
	{
		return 'reservasi_id';
	}
}