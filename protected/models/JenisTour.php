<?php
Yii::import('application.models._base.BaseJenisTour');

class JenisTour extends BaseJenisTour
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->jenis_tour_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->jenis_tour_id = $uuid;
        }
        return parent::beforeValidate();
    }
}