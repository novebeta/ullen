<?php
Yii::import('application.models._base.BaseKategoriCust');

class KategoriCust extends BaseKategoriCust
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->kategori_cust_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kategori_cust_id = $uuid;
        }
        return parent::beforeValidate();
    }
}