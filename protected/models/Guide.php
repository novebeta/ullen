<?php
Yii::import('application.models._base.BaseGuide');

class Guide extends BaseGuide
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->guide_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->guide_id = $uuid;
        }
        return parent::beforeValidate();
    }
}