<?php
Yii::import('application.models._base.BaseGrupTransDetail');

class GrupTransDetail extends BaseGrupTransDetail
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->grup_trans_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->grup_trans_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}