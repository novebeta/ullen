<?php
Yii::import( 'application.models._base.BaseProduk' );
class Produk extends BaseProduk {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->produk_id == null ) {
			$command         = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid            = $command->queryScalar();
			$this->produk_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public function checkStockMove() {
		$comm = Yii::app()->db->createCommand( "SELECT
        IFNULL(Sum(nsm.qty),0) FROM stock_moves AS nsm WHERE nsm.visible = 1 AND nsm.barang_id = :barang_id" );
		$qty  = $comm->queryScalar( [ ':barang_id' => $this->produk_id ] );
		return $qty;
	}
	public function getPoint( $member_kategori_id ) {
		/** @var MemberPoint $point */
		$point = MemberPoint::model()->findByAttributes( [
			'produk_id'          => $this->produk_id,
			'member_kategori_id' => $member_kategori_id
		] );
		return $point == null ? 0 : $point->point;
	}
}