<?php
Yii::import( 'application.models._base.BaseLangTour' );
class LangTour extends BaseLangTour {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->lang_tour_id == null ) {
			$command            = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid               = $command->queryScalar();
			$this->lang_tour_id = $uuid;
		}
		return parent::beforeValidate();
	}
	public static function getDefault() {
		$l = LangTour::model()->findByAttributes( [ 'default_' => 1 ] );
		if ( $l != null ) {
			return $l->lang_tour_id;
		}
		return '';
	}
}