<?php
Yii::import('application.models._base.BaseMemberPoint');

class MemberPoint extends BaseMemberPoint
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->member_point_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->member_point_id = $uuid;
        }
        return parent::beforeValidate();
    }
}