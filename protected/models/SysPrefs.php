<?php

Yii::import('application.models._base.BaseSysPrefs');
class SysPrefs extends BaseSysPrefs
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_val($index)
    {
        $pref = SysPrefs::model()->find('name_ = :name_ ',
            array(':name_' => $index));
        return $pref->value_;
    }
    public static function save_value($name_, $value_)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_sys_prefs (name_, value_)
                VALUES (:name_, :value_)"
        );
        return $comm->execute(array(':name_' => $name_, ':value_' => $value_));
    }
    public function beforeValidate()
    {
        if ($this->sys_prefs_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sys_prefs_id = $uuid;
        }
        return parent::beforeValidate();
    }
}