// function launchQZ() {
//     if (!qz.websocket.isActive()) {
//         startConnection({retries: 5, delay: 1});
//     }
// }
function chr(i) {
    return String.fromCharCode(i);
}

var __cutPaper = [{type: 'raw', data: chr(27) + chr(105), options: {language: 'ESCPOS', dotDensity: 'double'}}];
var __feedPaper = [{
    type: 'raw',
    data: chr(27) + chr(100) + chr(6),
    options: {language: 'ESCPOS', dotDensity: 'double'}
}];
var __feedReversePaper = [{
    type: 'raw',
    data: chr(27) + chr(75) + chr(3),
    options: {language: 'ESCPOS'}
}];
var __openCashDrawer = [{
    type: 'raw',
    data: chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r",
    options: {language: 'ESCP', dotDensity: 'double'}
}];

function __getPrintData(tipe_) {
    var __printDataTM82 = [{
        type: 'raw', data: chr(27) + chr(64) + //initial
            chr(27) + chr(51) + chr(50), //+ //space vertical
        //chr(27) + chr(77) + chr(49),// font B
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __printDataTM81 = [{
        type: 'raw', data: chr(27) + chr(64) + //initial
            chr(27) + chr(51) + chr(25), //+ //space vertical
        //chr(27) + chr(77) + chr(49),// font B
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __printDataTMU220 = [{
        type: 'raw', data: chr(27) + chr(64) + //initial
            chr(27) + chr(51) + chr(25), //+ //space vertical
        //chr(27) + chr(77) + chr(49),// font B
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __print = '';
    switch (tipe_) {
        case 'tmt82' :
            __print = __printDataTM82;
            break;
        case 'tmt81' :
            __print = __printDataTM81;
            break;
        case 'tmtu220' :
            __print = __printDataTMU220;
            break;
        default :
            __print = __printDataTM82;
            break;
    }
    return __print;
}

PRINTER_RECEIPT = 'default';
__printData = [{
    type: 'raw', data: chr(27) + chr(64) + //initial
        chr(27) + chr(51) + chr(50), //+ //space vertical
    //chr(27) + chr(77) + chr(49),// font B
    options: {language: 'ESCP', dotDensity: 'double'}
}];
PRINTER_STOCKER = 'default';
__printDataStocker = [{
    type: 'raw', data: chr(27) + chr(64) + //initial
        chr(27) + chr(51) + chr(50) + //space vertical
        chr(27) + chr(77) + chr(49),// font B
    options: {language: 'ESCP', dotDensity: 'double'}
}];
PRINTER_BEAUTY = 'default';
__printDataBeauty = [{
    type: 'raw', data: chr(27) + chr(64) + //initial
        chr(27) + chr(51) + chr(25),// + //space vertical
    //  chr(27) + chr(77) + chr(49),// font B
    options: {language: 'ESCP', dotDensity: 'double'}
}];
PRINTER_CARD = 'default';
COM_POSIFLEX = 'COM3';
POSCUSTOMERREADONLY = false;
POSCUSTOMERDEFAULT = '';
var local = localStorage.getItem("settingClient");
if (local !== null) {
    var data_ = JSON.parse(local);
    var __value = JSON.search(data_, '//data[name_="PRINTER_RECEIPT"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        PRINTER_RECEIPT = __value.name;
        __printData = __getPrintData(__value.tipe);
    }
    __value = JSON.search(data_, '//data[name_="PRINTER_STOCKER"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        PRINTER_STOCKER = __value.name;
        __printDataStocker = __getPrintData(__value.tipe);
    }
    __value = JSON.search(data_, '//data[name_="PRINTER_BEAUTY"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        PRINTER_BEAUTY = __value.name;
        __printDataBeauty = __getPrintData(__value.tipe);
    }
    __value = JSON.search(data_, '//data[name_="PRINTER_CARD"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        PRINTER_CARD = __value;
    }
    __value = JSON.search(data_, '//data[name_="COM_POSIFLEX"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        COM_POSIFLEX = __value;
    }
    __value = JSON.search(data_, '//data[name_="POSCUSTOMERREADONLY"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        POSCUSTOMERREADONLY = __value;
    }
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function openSerialPort(serialBegin, serialEnd, serialWidth, serialPort) {
    var widthVal = serialWidth;
    if (!widthVal) {
        widthVal = null;
    }
    var bounds = {
        begin: serialBegin,
        end: serialEnd,
        width: widthVal
    };
    qz.serial.openPort(serialPort, bounds).then(function () {
        displayMessage("Serial port opened");
    }).catch(displayError);
}

function __sendSerialData(serialPort, serialCmd) {
    var properties = {
        baudRate: 9600,
        dataBits: 8,
        stopBits: 1,
        parity: "NONE",
        flowControl: "NONE"
    };
    qz.serial.sendDataNwis(serialPort, serialCmd, properties).catch(displayError);
}

function closeSerialPort(serialPort) {
    qz.serial.closePort(serialPort).then(function () {
        displayMessage("Serial port closed");
    }).catch(displayError);
}

window.qzVersion = 0;
var cfg = null;

function getUpdatedConfig() {
    // if (cfg == null) {
    cfg = qz.configs.create(null);
    // }
    updateConfig();
    return cfg;
}

function updateConfig() {
    var pxlSize = null;
    var pxl = false;
    if (pxl) {
        pxlSize = {
            width: "",
            height: ""
        };
    }
    var pxlMargins = "0";
    var margin = false;
    if (margin) {
        pxlMargins = {
            top: "0",
            right: "0",
            bottom: "0",
            left: "0"
        };
    }
    var copies = 1;
    var jobName = null;
    var rawTab = true;
    if (rawTab) {
        copies = "1";
        jobName = "";
    } else {
        copies = "1";
        jobName = "";
    }
    cfg.reconfigure({
        language: "escp",
        altPrinting: false,
        encoding: "",
        endOfDoc: "",
        perSpool: "1",
        colorType: "grayscale",
        copies: copies,
        jobName: jobName,
        density: "",
        duplex: false,
        interpolation: "",
        margins: pxlMargins,
        orientation: "",
        paperThickness: "",
        printerTray: "",
        rotation: "0",
        scaleContent: false,
        size: pxlSize,
        units: "in"
    });
}

function updateState(text, css) {
    console.log(text);
}

function displayMessage(msg, css) {
    console.log(msg);
}

function displayError(err) {
    console.log(err);
}

function findVersion() {
    qz.api.getVersion().then(function (data) {
        displayMessage("qzVersion : " + data);
        window.qzVersion = data;
    }).catch(displayError);
}

function handleConnectionError(err) {
    updateState('Error', 'danger');
    if (err.target != undefined) {
        if (err.target.readyState >= 2) { //if CLOSING or CLOSED
            displayError("Connection to QZ Tray was closed");
        } else {
            displayError("A connection error occurred, check log for details");
            console.error(err);
        }
    } else {
        displayError(err);
    }
}

function setPrinter(printer) {
    var cf = getUpdatedConfig();
    cf.setPrinter(printer);
    if (typeof printer === 'object' && printer.name == undefined) {
        var shown;
        if (printer.file != undefined) {
            shown = "FILE: " + printer.file;
        }
        if (printer.host != undefined) {
            shown = "HOST: " + printer.host + ":" + printer.port;
        }
        console.log(shown);
    } else {
        if (printer.name != undefined) {
            printer = printer.name;
        }
        if (printer == undefined) {
            printer = 'NONE';
        }
        console.log(printer);
    }
}

var certificate = "-----BEGIN CERTIFICATE-----\n" +
    "MIIEODCCAyCgAwIBAgIJAM6YuNIvTVVUMA0GCSqGSIb3DQEBCwUAMIGvMQswCQYD\n" +
    "VQQGEwJJRDETMBEGA1UECAwKWW9neWFrYXJ0YTEOMAwGA1UEBwwFWW9neWExHzAd\n" +
    "BgNVBAoMFlBUIEV1bmlrZSBOYXRoYW4gQWJhZGkxHzAdBgNVBAsMFmluZm9ybWF0\n" +
    "aW9uIHRlY2hub2xvZ3kxFjAUBgNVBAMMDSp1bGxlbi50aWNrZXQxITAfBgkqhkiG\n" +
    "9w0BCQEWEm5vdmViZXRhQGdtYWlsLmNvbTAgFw0xOTA1MDUwNTQ5MDNaGA8yMDUw\n" +
    "MTAyODA1NDkwM1owga8xCzAJBgNVBAYTAklEMRMwEQYDVQQIDApZb2d5YWthcnRh\n" +
    "MQ4wDAYDVQQHDAVZb2d5YTEfMB0GA1UECgwWUFQgRXVuaWtlIE5hdGhhbiBBYmFk\n" +
    "aTEfMB0GA1UECwwWaW5mb3JtYXRpb24gdGVjaG5vbG9neTEWMBQGA1UEAwwNKnVs\n" +
    "bGVuLnRpY2tldDEhMB8GCSqGSIb3DQEJARYSbm92ZWJldGFAZ21haWwuY29tMIIB\n" +
    "IjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxk/ueX7mAYtXGMrlbZ1SxQtH\n" +
    "hZ2LXM5WBedngBUefmf+f89JQEftQ2d/Yx6Q6BLHFkJD0ldF6mnLFTjZXGzSoaYe\n" +
    "esq4h2JBkGOm7ZzuiK4FCoijVvJARaEgeeRzcv3C+PAtqG6gIu6jzt98FNLEnBKb\n" +
    "fHPgXrAFd8G9kRSAuK6DP9vw88+ZwA8x9J6aGaRDtZq56+Zgi4vLGg+UQ14K2ERk\n" +
    "TaWTJOgJxqSB3bF4PEo4QGyobtuM3CWzm7ZAgfmwYXbBBQyhz3+iuYXIu5ZCpK+z\n" +
    "4uax+VwBz4WHuAtpKGndxnvifaSI9eFFX75dFfR0RARVNV8GrWM7ekrQr/NM0QID\n" +
    "AQABo1MwUTAdBgNVHQ4EFgQUtRZVJ1dWA8/4W6U9tcqnlD/Ho+AwHwYDVR0jBBgw\n" +
    "FoAUtRZVJ1dWA8/4W6U9tcqnlD/Ho+AwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG\n" +
    "9w0BAQsFAAOCAQEAga2hR8MVoefL70Rb7LADLJ4jNWQE1ztDj4J3JbKVp71xBtUJ\n" +
    "enD9W7/kWokjQBajerXwE6nYxsis0ZRG8gFfhFxQk0lxUfA4pnFJTYOkYU6vEHwG\n" +
    "rElG9lihbELNY/jq+b/A7zS61ov6DQ+jK1tNWKM9AfuIQDBOtQ9NL1ff8Km8qV8F\n" +
    "HMbrIXcosV3kHsjIEvBTXEZD3Ki68tCEqHjegjy6kvdsMJkNM2sR17APxcON4UtA\n" +
    "CoskP+92iIAsLxUMphlvHCH3jpWGq0Xu+m0EWf6T8uQuWjzxcPguKdPFCIBxbRkG\n" +
    "YxkRlac0EbBtL720WlBNM6EqMrkVkNyv7QDXew==\n" +
    "-----END CERTIFICATE-----\n";
/// Authentication setup ///
qz.security.setCertificatePromise(function (resolve, reject) {
    //Preferred method - from server
//        $.ajax({ url: "assets/signing/digital-certificate.txt", cache: false, dataType: "text" }).then(resolve, reject);
    //Alternate method 1 - anonymous
//        resolve();
    //Alternate method 2 - direct
    resolve(certificate);
});
var privateKey = "-----BEGIN PRIVATE KEY-----\n" +
    "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDGT+55fuYBi1cY\n" +
    "yuVtnVLFC0eFnYtczlYF52eAFR5+Z/5/z0lAR+1DZ39jHpDoEscWQkPSV0XqacsV\n" +
    "ONlcbNKhph56yriHYkGQY6btnO6IrgUKiKNW8kBFoSB55HNy/cL48C2obqAi7qPO\n" +
    "33wU0sScEpt8c+BesAV3wb2RFIC4roM/2/Dzz5nADzH0npoZpEO1mrnr5mCLi8sa\n" +
    "D5RDXgrYRGRNpZMk6AnGpIHdsXg8SjhAbKhu24zcJbObtkCB+bBhdsEFDKHPf6K5\n" +
    "hci7lkKkr7Pi5rH5XAHPhYe4C2koad3Ge+J9pIj14UVfvl0V9HREBFU1XwatYzt6\n" +
    "StCv80zRAgMBAAECggEBAJ2nBapIm4k2wLLQ+bs+ioJjCSTLrfLFhdWDtOA56dgL\n" +
    "zGLG4zDa/V/Mc5TUw9X6t5KWc1xQTSChofTpEMNNL9WaPjjX2AKotf6XeikDzQ2T\n" +
    "aiaJoXCJfVbdVPgSCUWtPsawCVwRNfUPSeI4AGh9dhetIza41/HudSpTNuSTLBGk\n" +
    "I9tqQHfhr6DWJoj/0p4IZoU0pxMDynSeyWtHS9OBEGNsjWn3cmJGq4+3KzD1rSg7\n" +
    "Zcu8AcyUgFi8qQvGJHSMZ0RXAXOSipeD8Gs2NA/mjDiYZYECJ9vjyZJGHdfzzR/f\n" +
    "KKu95t4sFTiVjMB1L+ZPajBKuMsxMjkWyzGK0zvtL8ECgYEA6vW9I44LVj6q/GL6\n" +
    "laqYqVveNLO+74MDtIQsjMMubXMajxpGZ1KDEZ34Li0p4ZmDkBnaUIKHnW1N/UdV\n" +
    "aDScm0Cn/YBkMtQ6YLpdg7f1Oo1FEIVrfypM4Rl578NT7gkF3FjFtemZY5uMkM+z\n" +
    "f6lCjJ87nct7YtVFinJcS9d42w0CgYEA2BITL+U8N7NJFtBTtfvGxSZxM/MX/GG8\n" +
    "wJY8spRY3sOAHKv3tUbruzfaSNbzL7wvwlq50mM06nlVS7it47VP5S/pK6ynqAdJ\n" +
    "YrS7gU3K1g4n/8ENe5RVSYX91bhKaavIC1wHkf7FzsqGXZ7zYERjDGsxHaqpJChD\n" +
    "F7lh4xhad9UCgYEA2siiKxZcRh2ql8MOtQiaiLELsyVgeKQVciTVWulwNOhGZdRA\n" +
    "lHtHosLm0xCo+/I3iJoaKHezEgsy4NJom9TJsuM8yhegtZfbGDDRXRKRncjn859Y\n" +
    "3puj5MMVQQYwA+mQ9IIQ7/gqJY81D67n5MrWNh0bpS5RoVEYJPeiK9jjJ40CgYEA\n" +
    "pVjvpe9eraFdhT/XT6Rk7+oDJyYYbOK+dwKBYId1msMw/r/6x5aGPrb3LFaRsy3n\n" +
    "vmt/WCjjDavcQh4Js//ELDcF0I5HnqY02nxEISdO2Y/rvCQYFUHmImWPYkWtbEAw\n" +
    "zcCXann1FdKWEH7a8AhlY9ODupgm56yH61fKOOciFCECgYEAoWyrTp9BLDMEJfe7\n" +
    "C+P/9lnipvo9LRg23wzyzFv8Ia9MkvwbqIOPhc33ZGJU3mGoyDb49oangHPEr0Go\n" +
    "BWV1Y6ikymoDPCJgczmmkPPLv+9hL1Z8bGRjIXp72zFYu48ivIELSF9UyPG4g3Et\n" +
    "l3aq9gaEy07F9KROjXDSdAKEXww=\n" +
    "-----END PRIVATE KEY-----";
qz.security.setSignaturePromise(function (toSign) {
    return function (resolve, reject) {
        try {
            var pk = KEYUTIL.getKey(privateKey);
            var sig = new KJUR.crypto.Signature({"alg": "SHA1withRSA"});
            sig.init(pk);
            sig.updateString(toSign);
            var hex = sig.sign();
            console.log("DEBUG: \n\n" + stob64(hextorstr(hex)));
            resolve(stob64(hextorstr(hex)));
        } catch (err) {
            console.error(err);
            reject(err);
        }
    };
});

/// Connection ///
function launchQZ() {
    if (!qz.websocket.isActive()) {
        window.location.assign("qz:launch");
        //Retry 5 times, pausing 1 second between each attempt
        startConnection({retries: 5, delay: 1});
    }
}

function startConnection(config) {
    if (!qz.websocket.isActive()) {
        updateState('Waiting', 'default');
        qz.websocket.connect(config).then(function () {
            updateState('Active', 'success');
            findVersion();
        }).catch(handleConnectionError);
    } else {
        displayMessage('An active connection with QZ already exists.', 'alert-warning');
    }
}

function endConnection() {
    if (qz.websocket.isActive()) {
        qz.websocket.disconnect().then(function () {
            updateState('Inactive', 'default');
        }).catch(handleConnectionError);
    } else {
        displayMessage('No active connection with QZ exists.', 'alert-warning');
    }
}

/**
 * Optionally used to deploy multiple versions of the applet for mixed
 * environments.  Oracle uses document.write(), which puts the applet at the
 * top of the page, bumping all HTML content down.
 */
function is_enable_tools() {
    try {
        var gui = require('nw.gui');
        if (gui != null) {
            var win = gui.Window.get();
            win.maximize();
            // win.showDevTools();
            return true;
        }
    } catch (err) {
        console.log(err.message);
        return false;
    }
}

function notReady() {
    // If applet is not loaded, display an error
    if (!isLoaded()) {
        return true;
    }
    // If a printer hasn't been selected, display a message.
    //        else if (!qz.getPrinter()) {
    //            console.log('Please select a printer first by using the "Detect Printer" button.');
    //            return true;
    //        }
    return false;
}

function isLoaded() {
    // if (!is_enable_tools()) {
    //     return false;
    // }
    if (!qz) {
        console.log('Error:\n\n\tPrint plugin is NOT loaded!');
        return false;
    } else {
        try {
            if (!qz.websocket.isActive()) {
                //console.log('Error:\n\n\tPrint plugin is loaded but NOT active!');
                startConnection();
                return false;
            }
        } catch (err) {
            console.log('Error:\n\n\t' + err);
            return false;
        }
    }
    return true;
}

function findPrinter(query, set) {
    qz.printers.find(query).then(function (data) {
        displayMessage("Found: " + data);
        if (set) {
            setPrinter(data);
        }
    }).catch(displayError);
}

function findDefaultPrinter(set) {
    qz.printers.getDefault().then(function (data) {
        displayMessage("Found: " + data);
        if (set) {
            setPrinter(data);
        }
    }).catch(displayError);
}

function findPrinterReceipt() {
    var name = PRINTER_RECEIPT;
    findPrinter(name, true);
    // if (isLoaded()) {
    //     qz.findPrinter(name);
    //     window['qzDoneFinding'] = function () {
    //         var printer = qz.getPrinter();
    //         console.log(printer !== null ? 'Printer found: "' + printer +
    //         '" after searching for "' + name + '"' : 'Printer "' +
    //         name + '" not found.');
    //         window['qzDoneFinding'] = null;
    //     };
    //     while (!qz.isDoneFinding()) {
    //     }
    // }
}

function findPrinterCard() {
    var name = PRINTER_CARD;
    findPrinter(name, true);
    // if (isLoaded()) {
    //     qz.findPrinter(name);
    //     window['qzDoneFinding'] = function () {
    //         var printer = qz.getPrinter();
    //         console.log(printer !== null ? 'Printer found: "' + printer +
    //         '" after searching for "' + name + '"' : 'Printer "' +
    //         name + '" not found.');
    //         window['qzDoneFinding'] = null;
    //     };
    //     while (!qz.isDoneFinding()) {
    //     }
    // }
}

function appendEPCL(data) {
    if (data == null || data.length == 0) {
        return;
    }
    qz.appendHex('x1b');
    qz.append(data);
    qz.appendHex('x0D');
}

function printHTML(printer, msg) {
    if (notReady()) {
        return;
    }
    if (printer == 'default') {
        findDefaultPrinter(true);
    } else {
        findPrinter(printer, true);
    }
    var config = getUpdatedConfig();
    var printData = [
        {type: 'raw', format: 'hex', data: '1B4D'},
        {type: 'raw', data: msg}
    ];
    setTimeout(function () {
        qz.print(config, printData).catch(displayError);
    }, 500);
    // var exec = require('child_process').exec,
    //     child;
    // msg = base64_encode(msg);
    // child = exec('java -jar ./qz.jar printHTML "' + printer + '" ' + msg, 'shell',
    //     function (error, stdout, stderr) {
    //         console.log('stdout: ' + stdout);
    //         console.log('stderr: ' + stderr);
    //         if (error !== null) {
    //             console.log('exec error: ' + error);
    //         }
    //     });
}

function printRawQZ(printer, printData) {
    if (printer == 'default') {
        qz.printers.getDefault().then(function (data) {
            var cf = getUpdatedConfig();
            cf.setPrinter(data);
            qz.print(cf, printData).catch(displayError);
        }).catch(displayError);
    } else {
        qz.printers.find(printer).then(function (data) {
            var cf = getUpdatedConfig();
            cf.setPrinter(data);
            qz.print(cf, printData).catch(displayError);
        }).catch(displayError);
    }
}

function opencashdrawer(printer) {
    if (notReady()) {
        return;
    }
    printRaw(printer, 'hex', '1B401B70301919');
}

function printCard(type, cust_no, cust_name, since, valid) {
    switch (type) {
        case 'MEN':
            cust_no = 'T 50 450 0 1 0 75 1 ' + cust_no;
            cust_name = 'T 50 520 0 1 0 65 1 ' + cust_name;
            since = 'T 535 575 0 1 0 45 1 ' + since;
            valid = 'T 785 575 0 1 0 45 1 ' + valid;
            break;
        case 'TEEN':
            cust_no = 'T 50 450 0 1 0 75 1 ' + cust_no;
            cust_name = 'T 50 520 0 1 0 65 1 ' + cust_name;
            since = 'T 170 575 0 1 0 45 1 ' + valid;
            valid = 'T 785 575 0 1 0 45 1 ' + since;
            break;
        case 'WOMEN':
            cust_no = 'T 50 400 0 1 0 75 1 ' + cust_no;
            cust_name = 'T 50 470 0 1 0 65 1 ' + cust_name;
            since = 'T 170 580 0 1 0 45 1 ' + since;
            valid = 'T 650 580 0 1 0 45 1 ' + valid;
            break;
    }
    msg = '{"cust_no":"' + cust_no + '","cust_name":"' + cust_name +
        '","since":"' + since + '","valid":"' + valid + '"}';
    var exec = require('child_process').exec,
        child;
    msg = base64_encode(msg);
    child = exec('java -jar ./qz.jar printCard "' + PRINTER_CARD + '" ' + msg, 'shell',
        function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
}

function sendSerialData(msg) {
    if (notReady()) {
        return;
    }
    __sendSerialData(COM_POSIFLEX, msg);
}