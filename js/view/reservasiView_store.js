jun.ReservasiViewstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReservasiViewstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReservasiViewStoreId',
            url: 'ReservasiView',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'nama'},
                {name: 'nama_member'},
                {name: 'ref'},
                {name: 'reservasi_id'},
                {name: 'member_id'},
                {name: 'jenis_tour_id'},
                {name: 'lang_tour_id'},
                {name: 'waktu_reservasi'},
                {name: 'waktu_tour_date'},
                {name: 'note_'},
                {name: 'total'},
                {name: 'uang_muka'},
                {name: 'sub_total'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'kategori_cust_id'},
                {name: 'waktu_tour_time'},
                {name: 'loket_id'},
                {name: 'alamat'},
                {name: 'email'},
                {name: 'no_telp'},
                {name: 'tipe_bayar'},
                {name: 'kat_cust'}
            ]
        }, cfg));
    }
});
jun.rztReservasiViewLoketCmp = new jun.ReservasiViewstore({baseParams: {mode: "view_loket"}});
jun.rztReservasi = new jun.ReservasiViewstore();
//jun.rztReservasiView.load();
