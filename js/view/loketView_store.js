jun.LoketViewstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.LoketViewstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LoketViewStoreId',
            url: 'LoketView',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'loket_id'},
                {name: 'member_id'},
                {name: 'jenis_tour_id'},
                {name: 'lang_tour_id'},
                {name: 'note_'},
                {name: 'total'},
                {name: 'uang_muka'},
                {name: 'sub_total'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'kategori_cust_id'},
                {name: 'reservasi_id'},
                {name: 'waktu_tour_date'},
                {name: 'waktu_tour_time'},
                {name: 'tgl_voucher'},
                {name: 'nama_member'},
                {name: 'alamat_member'},
                {name: 'ref'},
                {name: 'nama'},
                {name: 'no_telp'},
                {name: 'alamat'},
                {name: 'parent_loket_id'},
                {name: 'tipe_tamu'},
                {name: 'bayar'},
                {name: 'kembalian'}
            ]
        }, cfg));
    }
});
jun.rztLoketView = new jun.LoketViewstore({baseParams: {mode: "nota"}});
jun.rztLoketReturView = new jun.LoketViewstore({baseParams: {mode: "retur"}});
jun.rztLoketViewCmp = new jun.LoketViewstore();
//jun.rztLoketView.load();
