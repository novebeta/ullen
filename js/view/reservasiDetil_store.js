jun.ReservasiDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReservasiDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReservasiDetilStoreId',
            url: 'ReservasiDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'reservasi_detil_id'},
                {name: 'reservasi_id'},
                {name: 'produk_id'},
                {name: 'qty', type: 'float'},
                {name: 'harga', type: 'float'},
                {name: 'sub_total', type: 'float'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var sub_total = this.sum("sub_total");
        var uang_muka = Ext.getCmp('uang_mukareservasiid').getValue();
        var total = sub_total - uang_muka;
        Ext.getCmp('sub_totalreservasiid').setValue(sub_total);
        Ext.getCmp('totalreservasiid').setValue(total);
    }
});
jun.rztReservasiDetil = new jun.ReservasiDetilstore();
//jun.rztReservasiDetil.load();
