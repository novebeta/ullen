jun.ReservasiWin = Ext.extend(Ext.Window, {
    title: 'Reservasi',
    modez: 1,
    width: 840,
    height: 530,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Reservasi',
                layout: 'table',
                layoutConfig: {columns: 2},
                defaults: {width: 400},
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        layout: 'form',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 145,
                        items: [
                            {
                                xtype: 'mfcombobox',
                                style: 'margin-bottom:2px',
                                searchFields: [
                                    'nama'
                                ],
                                mode: 'local',
                                store: jun.rztMemberCmp,
                                valueField: 'member_id',
                                itemSelector: 'tr.search-item',
                                tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                    '<th>Nama</th></tr></thead>',
                                    '<tbody><tpl for="."><tr class="search-item">',
                                    '<td>{nama}</td>',
                                    '</tr></tpl></tbody></table>'),
                                // width: 175,
                                allowBlank: false,
                                enableKeyEvents: true,
                                // listWidth: 400,
                                displayField: 'nama',
                                fieldLabel: 'Member',
                                hideLabel: false,
                                //hidden:true,
                                hiddenName: 'member_id',
                                ref: '../../member_id',
                                maxLength: 36,
                                allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama',
                                hideLabel: false,
                                //hidden:true,
                                name: 'nama',
                                id: 'namaid',
                                ref: '../../nama',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Telp',
                                hideLabel: false,
                                //hidden:true,
                                name: 'no_telp',
                                id: 'no_telpid',
                                ref: '../../no_telp',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Alamat',
                                hideLabel: false,
                                //hidden:true,
                                name: 'alamat',
                                id: 'alamatid',
                                ref: '../../alamat',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Status  Bayar',
                                store: jun.status_bayar,
                                hiddenName: 'status_bayar',
                                valueField: 'val',
                                displayField: 'label',
                                ref: '../status_bayar',
                                allowBlank: false,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 150,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 145,
                        items: [
                            {
                                xtype: 'compositefield',
                                fieldLabel: 'Tgl/Jam Tour',
                                msgTarget: 'side',
                                defaults: {
                                    flex: 1
                                },
                                items: [
                                    {
                                        xtype: 'xdatefield',
                                        ref: '../waktu_tour',
                                        fieldLabel: 'Tgl/Jam Tour',
                                        name: 'waktu_tour_date',
                                        format: 'd-m-Y',
                                        ref: '../../../waktu_tour_date',
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'timefield',
                                        ref: '../../../waktu_tour_time',
                                        format: 'H:i',
                                        altFormats: 'H:i',
                                        name: 'waktu_tour_time'
                                    }
                                ]
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Bahasa',
                                store: jun.rztLangTourCmp,
                                hiddenName: 'lang_tour_id',
                                valueField: 'lang_tour_id',
                                displayField: 'nama',
                                ref: '../lang_tour_id',
                                value: LANG_DEFAULT,
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Kategori Jenis Tour',
                                store: jun.rztJenisTourCmp,
                                hiddenName: 'jenis_tour_id',
                                valueField: 'jenis_tour_id',
                                displayField: 'nama',
                                ref: '../lang_tour_id',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Kategori Pengunjung',
                                store: jun.rztKategoriCustCmp,
                                hiddenName: 'kategori_cust_id',
                                valueField: 'kategori_cust_id',
                                displayField: 'nama',
                                ref: '../kategori_cust_id',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Note',
                                hideLabel: false,
                                // height: 76,
                                // hidden: true,
                                name: 'note_',
                                id: 'note_id',
                                ref: '../note_',
                                maxLength: 600,
                                //allowBlank: 1,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        colspan: 2,
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        height: 210,
                        width: 800,
                        layout: 'form',
                        border: false,
                        items: [
                            new jun.ReservasiDetilGrid({
                                // colspan: 2,
                                height: 200,
                                // width: 700,
                                frameHeader: !1,
                                header: !1,
                                ref: "../griddetil"
                            })
                        ]
                    },
                    {
                        xtype: 'panel',
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        frame: false,
                        border: false,
                        height: 75,
                        layout: {
                            type: 'vbox',
                            // padding: '5',
                            align: 'stretchmax'
                        },
                        items: [
                            {
                                xtype: 'label',
                                text: 'TOTAL'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'total',
                                readOnly: true,
                                id: 'totalreservasiid',
                                ref: '../total',
                                maxLength: 15,
                                value: 0,
                                //allowBlank: 1,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Sub Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'sub_total',
                                id: 'sub_totalreservasiid',
                                ref: '../sub_total',
                                maxLength: 15,
                                value: 0,
                                readOnly: true,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Uang Muka',
                                hideLabel: false,
                                //hidden:true,
                                name: 'uang_muka',
                                id: 'uang_mukareservasiid',
                                ref: '../../uang_muka',
                                maxLength: 15,
                                value: 0,
                                enableKeyEvents: true,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Cara Bayar',
                                store: jun.tipe_bayar,
                                hiddenName: 'tipe_bayar',
                                valueField: 'val',
                                displayField: 'label',
                                ref: '../tipe_bayar',
                                allowBlank: false,
                                anchor: '100%'
                            }
                        ]
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReservasiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.uang_muka.on('keyup', this.onUangMukaChange, this);
        this.uang_muka.on('change', this.onUangMukaChange, this);
        this.member_id.on('change', this.onMemberChange, this);
        this.member_id.on('select', this.onMemberSelect, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onMemberSelect: function (c, r, i) {
        this.nama.setValue(r.data.nama);
        this.no_telp.setValue(r.data.no_telp);
        this.alamat.setValue(r.data.alamat);
    },
    onMemberChange: function (t, n, o) {
        if (n == '') {
            this.nama.setValue('');
            this.no_telp.setValue('');
            this.alamat.setValue('');
            // this.nama.setReadOnly(false);
        } else {
            // this.nama.setReadOnly(true);
        }
    },
    onWinClose: function () {
        jun.rztReservasiDetil.removeAll();
        // this.mobil_id.lastQuery = null;
    },
    onUangMukaChange: function (a) {
        var uang_muka = parseFloat(a.getValue());
        var sub_total = parseFloat(Ext.getCmp('sub_totalreservasiid').getValue());
        var total = parseFloat(sub_total - uang_muka);
        Ext.getCmp('totalreservasiid').setValue(total);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Reservasi/create/';
        Ext.getCmp('form-Reservasi').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztReservasiDetil.data.items, "data")),
                mode: this.modez,
                id: this.id,
                // waktu_tour_time: Ext.Date.format(this.waktu_tour_time.getValue(), 'H:i')
            },
            success: function (f, a) {
                jun.rztReservasi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Reservasi').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});