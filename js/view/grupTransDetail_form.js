jun.GrupTransDetailWin = Ext.extend(Ext.Window, {
    title: 'Setting Grup',
    modez: 1,
    width: 700,
    height: 455,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-GrupTransDetail',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    new jun.GrupTransDetailGrid({
                        // colspan: 2,
                        height: 360,
                        // width: 700,
                        grup_trans_id: this.id,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetil"
                    }),
                    // {
                    //     xtype: 'numericfield',
                    //     fieldLabel: 'Max',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'sub_total',
                    //     id: 'sub_total_loketid',
                    //     ref: '../sub_total',
                    //     maxLength: 15,
                    //     //allowBlank: 1,
                    //     value: 0,
                    //     anchor: '100%'
                    // },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                // {
                //     xtype: 'button',
                //     text: 'Simpan',
                //     hidden: false,
                //     ref: '../btnSave'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Simpan & Tutup',
                //     ref: '../btnSaveClose'
                // },
                {
                    xtype: 'button',
                    text: 'Tutup',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.GrupTransDetailWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.on("close", this.onWinClose, this);
        // this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        // this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // if (this.modez == 1 || this.modez == 2) {
        //     this.btnSave.setVisible(false);
        // } else {
        //     this.btnSave.setVisible(true);
        // }
    },
    onWinClose: function () {
        jun.rztGrupTransDetail.removeAll();
        // this.mobil_id.lastQuery = null;
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'GrupTransDetail/update/id/' + this.id;
        } else {
            urlz = 'GrupTransDetail/create/';
        }
        Ext.getCmp('form-GrupTransDetail').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztGrupTransDetail.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-GrupTransDetail').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.GrupAddTiketWin = Ext.extend(Ext.Window, {
    title: 'Add Ticket',
    modez: 1,
    width: 600,
    height: 580,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-GrupTransDetail',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    new jun.TiketLoketGrid({
                        // colspan: 2,
                        height: 200,
                        // width: 700,
                        // frameHeader: !1,
                        // header: !1,
                        ref: "../gridDetilLoket"
                    }),
                    new jun.TiketTransGrid({
                        // colspan: 2,
                        height: 280,
                        // width: 700,
                        // frameHeader: !1,
                        // header: !1,
                        ref: "../griddetil"
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                // {
                //     xtype: 'button',
                //     text: 'Add Kwitansi',
                //     hidden: false,
                //     ref: '../btnSave'
                // },
                {
                    xtype: 'button',
                    text: 'Add Ticket',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Tutup',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.GrupAddTiketWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        // this.btnSave.on('click', this.saveForm, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        // if (this.modez == 1 || this.modez == 2) {
        //     this.btnSave.setVisible(false);
        // } else {
        //     this.btnSave.setVisible(true);
        // }
    },
    onWinClose: function () {
        jun.rztLoketTicket.removeAll();
        jun.rztListTicket.removeAll();
    },
    btnDisabled: function (status) {
        // this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.Ajax.request({
            url: 'TiketTrans/Voucher/',
            method: 'POST',
            scope: this,
            params: {
                vouchers: Ext.encode(Ext.pluck(this.griddetil.store.data.items, "data")),
                id: this.grup_trans_id
                // waktu_tour_time: Ext.Date.format(this.waktu_tour_time.getValue(), 'H:i')
            },
            success: function (f, a) {
                jun.rztLoketTicket.reload();
                jun.rztGrupTransDetail.reload();
                this.gridDetilLoket.store.reload();
                this.griddetil.store.removeAll();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.btnDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});