jun.Memberstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Memberstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MemberStoreId',
            url: 'Member',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'member_id'},
                {name: 'nama'},
                {name: 'alamat'},
                {name: 'tgl_lahir'},
                {name: 'email'},
                {name: 'status'},
                {name: 'no_telp'},
                {name: 'ktp'},
                {name: 'note'},
                {name: 'member_kategori_id'},
                {name: 'no_member'}
            ]
        }, cfg));
    }
});
jun.rztMember = new jun.Memberstore();
jun.rztMemberCmp = new jun.Memberstore();
jun.rztMemberLib = new jun.Memberstore();
//jun.rztMember.load();
