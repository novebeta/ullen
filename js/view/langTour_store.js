jun.LangTourstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.LangTourstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LangTourStoreId',
            url: 'LangTour',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'lang_tour_id'},
                {name: 'nama'},
                {name: 'status'},
                {name: 'default_'}
            ]
        }, cfg));
    }
});
jun.rztLangTour = new jun.LangTourstore();
jun.rztLangTourLib = new jun.LangTourstore();
jun.rztLangTourCmp = new jun.LangTourstore();
jun.rztLangTourLib.load();
