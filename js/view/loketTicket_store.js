jun.LoketTicketstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.LoketTicketstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LoketTicketStoreId',
            url: 'LoketTicket',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'loket_id'},
                {name: 'member_id'},
                {name: 'jenis_tour_id'},
                {name: 'lang_tour_id'},
                {name: 'note_'},
                {name: 'total'},
                {name: 'uang_muka'},
                {name: 'sub_total'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'kategori_cust_id'},
                {name: 'reservasi_id'},
                {name: 'waktu_tour_date'},
                {name: 'waktu_tour_time'},
                {name: 'tgl_voucher'},
                {name: 'nama'},
                {name: 'alamat'},
                {name: 'ref'},
                {name: 'nama_kat'}
            ]
        }, cfg));
    }
});
jun.rztLoketTicket = new jun.LoketTicketstore();
//jun.rztLoketTicket.load();
