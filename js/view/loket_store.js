jun.Loketstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Loketstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LoketStoreId',
            url: 'Loket',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'loket_id'},
                {name: 'member_id'},
                {name: 'jenis_tour_id'},
                {name: 'lang_tour_id'},
                {name: 'note_'},
                {name: 'total'},
                {name: 'uang_muka'},
                {name: 'sub_total'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'kategori_cust_id'},
                {name: 'reservasi_id'},
                {name: 'waktu_tour_date'},
                {name: 'waktu_tour_time'},
                {name: 'tgl_voucher'},
                {name: 'ref'},
                {name: 'nama'},
                {name: 'no_telp'},
                {name: 'alamat'},
                {name: 'parent_loket_id'},
                {name: 'tipe_tamu'},
                {name: 'bayar'},
                {name: 'kembalian'},
                {name:'total_point'}
            ]
        }, cfg));
    }
});
jun.rztLoket = new jun.Loketstore();
//jun.rztLoket.load();
