jun.KategoriCuststore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KategoriCuststore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KategoriCustStoreId',
            url: 'KategoriCust',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kategori_cust_id'},
                {name: 'nama'},
                {name: 'note_'},
                {name: 'status'}
            ]
        }, cfg));
    }
});
jun.rztKategoriCust = new jun.KategoriCuststore();
jun.rztKategoriCustLib = new jun.KategoriCuststore();
jun.rztKategoriCustCmp = new jun.KategoriCuststore();
//jun.rztKategoriCust.load();
