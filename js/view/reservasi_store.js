jun.Reservasistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Reservasistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReservasiStoreId',
            url: 'Reservasi',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'reservasi_id'},
                {name: 'member_id'},
                {name: 'jenis_tour_id'},
                {name: 'lang_tour_id'},
                {name: 'waktu_reservasi'},
                {name: 'waktu_tour_date'},
                {name: 'waktu_tour_time'},
                {name: 'note_'},
                {name: 'total'},
                {name: 'uang_muka'},
                {name: 'sub_total'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'kategori_cust_id'},
                {name: 'nama'},
                {name: 'ref'},
                {name: 'no_telp'},
                {name: 'alamat'},
                {name: 'status_bayar'},
            ]
        }, cfg));
    }
});
jun.rztReservasiCmp = new jun.Reservasistore();
//jun.rztReservasi.load();
