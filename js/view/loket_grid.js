jun.LoketGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Nota",
    id: 'docs-jun.LoketGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Nota',
            sortable: true,
            resizable: true,
            dataIndex: 'ref',
            width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'No. Telp',
            sortable: true,
            resizable: true,
            dataIndex: 'no_telp',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat',
            width: 100
        },
        {
            header: 'Jenis Tour',
            sortable: true,
            resizable: true,
            dataIndex: 'jenis_tour_id',
            width: 100,
            renderer: jun.renderJenisTour
        },
        {
            header: 'Bahasa',
            sortable: true,
            resizable: true,
            dataIndex: 'lang_tour_id',
            width: 100,
            renderer: jun.renderLangTour
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztLoketView.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglloketgrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglcashgrid').getValue();
                    b.params.mode = "nota";
                }
            }
        });
        this.store = jun.rztLoketView;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Nota',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Nota',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Nota',
                    ref: '../btnView'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Nota',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Nota Retur',
                    ref: '../btnRetur'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Create Ticket',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglloketgrid',
                    value: DATE_NOW
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.LoketGrid.superclass.initComponent.call(this);
        if (jun.rztMemberCmp.getTotalCount() === 0) {
            jun.rztMemberCmp.load();
        }
        if (jun.rztLangTourCmp.getTotalCount() === 0) {
            jun.rztLangTourCmp.load();
        }
        if (jun.rztJenisTourCmp.getTotalCount() === 0) {
            jun.rztJenisTourCmp.load();
        }
        if (jun.rztKategoriCustCmp.getTotalCount() === 0) {
            jun.rztKategoriCustCmp.load();
        }
        if (jun.rztProdukCmp.getTotalCount() === 0) {
            jun.rztProdukCmp.load();
        }
        this.on('activate', this.onActivate, this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnView.on('Click', this.loadViewForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnRetur.on('Click', this.notaReturForm, this);
        this.btnPrint.on('Click', this.notaPrint, this);
        this.tgl.on('select', function () {
            this.store.reload()
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onActivate: function () {
        jun.rztLoketView.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztLoketViewCmp.load({
            params: {
                loket_id: this.record.data.loket_id
            }
        });
        jun.rztReservasiViewLoketCmp.load({
            params: {
                reservasi_id: this.record.data.reservasi_id
            }
        });
    },
    loadForm: function () {
        var form = new jun.LoketNewWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih nota.");
            return;
        }
        if (selectedz.json.tgl_voucher != null) {
            Ext.MessageBox.alert("Error", "Tiket sudah dibuat!");
            return;
        }
        var idz = selectedz.json.loket_id;
        var form = new jun.LoketNewWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztLoketDetil.load({
            params: {
                loket_id: idz
            }
        });
    },
    notaPrint: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih nota.");
            return;
        }
        Ext.Ajax.request({
            url: 'Loket/print/id/' + this.record.json.loket_id,
            method: 'POST',
            success: function (f, a) {
                // jun.rztLoketView.reload();
                var response = Ext.decode(f.responseText);
                var msg = [{type: 'raw', data: response.msg, options: {language: 'ESCPOS', dotDensity: 'double'}}];
                // __printData = __getPrintData('tmt81');
                var __printDataTM82 = [{
                    type: 'raw', data: '\x1B' + '\x40' + //initial
                        '\x1B' + '\x33' + '\x32', //+ //space vertical
                    //chr(27) + chr(77) + chr(49),// font B
                    options: {language: 'ESCPOS', dotDensity: 'double'}
                }];
                var printData = __printDataTM82.concat(msg, __feedPaper, __cutPaper);
                var data = [
                    // { type: 'raw', format: 'image', data: 'assets/img/image_sample_bw.png', options: { language: "ESCPOS", dotDensity: 'double' } },
                    '\x1B' + '\x40',          // init
                    '\x1B' + '\x61' + '\x31', // center align
                    'Beverly Hills, CA  90210' + '\x0A',
                    '\x0A',                   // line break
                    'www.qz.io' + '\x0A',     // text and line break
                    '\x0A',                   // line break
                    '\x0A',                   // line break
                    'May 18, 2016 10:30 AM' + '\x0A',
                    '\x0A',                   // line break
                    '\x0A',                   // line break
                    '\x0A',
                    'Transaction # 123456 Register: 3' + '\x0A',
                    '\x0A',
                    '\x0A',
                    '\x0A',
                    '\x1B' + '\x61' + '\x30', // left align
                    'Baklava (Qty 4)       9.00' + '\x1B' + '\x74' + '\x13' + '\xAA', //print special char symbol after numeric
                    '\x0A',
                    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' + '\x0A',
                    '\x1B' + '\x45' + '\x0D', // bold on
                    'Here\'s some bold text!',
                    '\x0A',
                    '\x1B' + '\x45' + '\x0A', // bold off
                    '\x1D' + '\x21' + '\x11', // double font size
                    'Here\'s large text!',
                    '\x0A',
                    '\x1D' + '\x21' + '\x00', // standard font size
                    '\x1B' + '\x61' + '\x32', // right align
                    '\x1B' + '\x21' + '\x30', // em mode on
                    'DRINK ME',
                    '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
                    '\x0A' + '\x0A',
                    '\x1B' + '\x61' + '\x30', // left align
                    '------------------------------------------' + '\x0A',
                    '\x1B' + '\x4D' + '\x31', // small text
                    'EAT ME' + '\x0A',
                    '\x1B' + '\x4D' + '\x30', // normal text
                    '------------------------------------------' + '\x0A',
                    'normal text',
                    '\x1B' + '\x61' + '\x30', // left align
                    '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A',
                    '\x1B' + '\x69',          // cut paper (old syntax)
// '\x1D' + '\x56'  + '\x00' // full cut (new syntax)
// '\x1D' + '\x56'  + '\x30' // full cut (new syntax)
// '\x1D' + '\x56'  + '\x01' // partial cut (new syntax)
// '\x1D' + '\x56'  + '\x31' // partial cut (new syntax)
                    '\x10' + '\x14' + '\x01' + '\x00' + '\x05',  // Generate Pulse to kick-out cash drawer**
                                                                 // **for legacy drawer cable CD-005A.  Research before using.
                                                                 // see also http://keyhut.com/popopen4.htm
                ];
                data = [
                    '\x1B' + '\x40',          // init
                    '\x1B' + '\x61' + '\x31', // center align
                    '\x1B' + '\x21' + '\x20',
                    'Ullen Sentalu Museum' + '\x0A',
                    '\x1B' + '\x21' + '\x00',
                    response.doublebaris + '\x0A',
                    '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
                    '\x1B' + '\x61' + '\x30',
                    '\x1B' + '\x21' + '\x30' + '\x0A',
                    response.tour + '\x0A' + '\x0A',
                    '\x1B' + '\x21' + '\x01',
                    '\x1B' + '\x21' + '\x20',
                    response.tamu + '\x0A',
                    response.kat + '\x0A',
                    '\x1B' + '\x21' + '\x00',
                    response.singlebaris + '\x0A',
                    // '\x1D' + '\x21' + '\x11',
                    '\x1B' + '\x21' + '\x20',
                    response.msg + '\x0A',
                    '\x1B' + '\x21' + '\x00',
                    // '\x1D' + '\x21' + '\x00',
                    '\x1B' + '\x4D' + '\x31',
                    // '---------------------------' + '\x0A',
                    response.tgl + '\x0A',
                    response.masuk + '\x0A',
                    response.ref + '\x0A',
                    '\x0A',
                    '\x1B' + '\x64' + '\x06', // feed
                    '\x1B' + '\x69' // cut
                ];
                printRawQZ('default', data);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: 'Nota sedang di print...',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadViewForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih nota.");
            return;
        }
        var idz = selectedz.json.loket_id;
        var form = new jun.LoketWin({modez: 2, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztLoketDetil.load({
            params: {
                loket_id: idz
            }
        });
    },
    notaReturForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih nota.");
            return;
        }
        var idz = selectedz.json.loket_id;
        this.record.data.parent_loket_id = idz;
        this.record.data.note_ = null;
        var form = new jun.LoketReturWin({modez: 0, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztLoketDetil.load({
            params: {
                parent_loket_id: idz
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membuat voucher dari data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Nota");
            return;
        }
        // if (record.data.tgl_voucher != null) {
        //     Ext.MessageBox.alert("Error", "Tiket sudah dibuat!");
        //     return;
        // }
        Ext.Ajax.request({
            url: 'Loket/voucher/id/' + record.json.loket_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztLoketView.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.LoketReturGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Nota Retur",
    id: 'docs-jun.LoketReturGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Retur',
            sortable: true,
            resizable: true,
            dataIndex: 'ref',
            width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'No. Telp',
            sortable: true,
            resizable: true,
            dataIndex: 'no_telp',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat',
            width: 100
        },
        {
            header: 'Jenis Tour',
            sortable: true,
            resizable: true,
            dataIndex: 'jenis_tour_id',
            width: 100,
            renderer: jun.renderJenisTour
        },
        {
            header: 'Bahasa',
            sortable: true,
            resizable: true,
            dataIndex: 'lang_tour_id',
            width: 100,
            renderer: jun.renderLangTour
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        jun.rztLoketReturView.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglloketreturgrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglcashgrid').getValue();
                    b.params.mode = "retur";
                }
            }
        });
        this.store = jun.rztLoketReturView;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'View Retur',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Retur',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglloketreturgrid',
                    value: DATE_NOW
                }
            ]
        };
        jun.LoketReturGrid.superclass.initComponent.call(this);
        if (jun.rztMemberCmp.getTotalCount() === 0) {
            jun.rztMemberCmp.load();
        }
        if (jun.rztLangTourCmp.getTotalCount() === 0) {
            jun.rztLangTourCmp.load();
        }
        if (jun.rztJenisTourCmp.getTotalCount() === 0) {
            jun.rztJenisTourCmp.load();
        }
        if (jun.rztKategoriCustCmp.getTotalCount() === 0) {
            jun.rztKategoriCustCmp.load();
        }
        if (jun.rztProdukCmp.getTotalCount() === 0) {
            jun.rztProdukCmp.load();
        }

        this.on('activate', this.onActivate, this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', function () {
            this.store.reload()
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onActivate: function () {
        jun.rztLoketReturView.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih nota.");
            return;
        }
        var idz = selectedz.json.loket_id;
        var form = new jun.LoketReturWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztLoketDetil.load({
            params: {
                loket_id: idz
            }
        });
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih nota.");
            return;
        }
        var idz = selectedz.json.loket_id;
        var form = new jun.LoketReturWin({modez: 2, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztLoketDetil.load({
            params: {
                loket_id: idz
            }
        });
    }
});
