jun.GrupTransWin = Ext.extend(Ext.Window, {
    title: 'Setting Grup',
    modez: 1,
    width: 400,
    height: 225,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-GrupTrans',
                labelWidth: 150,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Guide',
                        store: jun.rztGuideCmp,
                        hiddenName: 'guide_id',
                        valueField: 'guide_id',
                        displayField: 'nama',
                        ref: '../guide_id',
                        // allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'timefield',
                        fieldLabel: 'Perkiraan Masuk',
                        ref: '../waktu_masuk',
                        format: 'H:i',
                        altFormats: 'H:i',
                        name: 'waktu_masuk'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kategori Jenis Tour',
                        store: jun.rztJenisTourCmp,
                        hiddenName: 'jenis_tour_id',
                        valueField: 'jenis_tour_id',
                        displayField: 'nama',
                        ref: '../lang_tour_id',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Bahasa',
                        store: jun.rztLangTourCmp,
                        hiddenName: 'lang_tour_id',
                        valueField: 'lang_tour_id',
                        displayField: 'nama',
                        ref: '../lang_tour_id',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kategori Pengunjung',
                        store: jun.rztKategoriCustCmp,
                        hiddenName: 'kategori_cust_id',
                        valueField: 'kategori_cust_id',
                        displayField: 'nama',
                        ref: '../kategori_cust_id',
                        allowBlank: false,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.GrupTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztReservasiDetil.removeAll();
        // this.mobil_id.lastQuery = null;
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'GrupTrans/update/id/' + this.id;
        } else {
            urlz = 'GrupTrans/create/';
        }
        Ext.getCmp('form-GrupTrans').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztGrupTrans.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-GrupTrans').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});