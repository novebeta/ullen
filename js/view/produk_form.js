jun.ProdukWin = Ext.extend(Ext.Window, {
    title: 'Produk',
    modez: 1,
    width: 400,
    height: 235,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Produk',
                labelWidth: 120,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama',
                        id: 'namaid',
                        ref: '../nama',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: "combo",
                        typeAhead: !0,
                        allowBlank: false,
                        triggerAction: "all",
                        lazyRender: !0,
                        mode: "local",
                        fieldLabel: "Kategori Jenis Tour",
                        store: jun.rztJenisTourCmp,
                        hiddenName: "jenis_tour_id",
                        valueField: "jenis_tour_id",
                        ref: "../cmbBankAsal",
                        forceSelection: !0,
                        displayField: "nama",
                        anchor: "100%",
                        lastQuery: ""
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode Depan',
                        hideLabel: false,
                        name: 'kode_depan',
                        id: 'kode_depanid',
                        ref: '../kode_depan',
                        maxLength: 10,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode Belakang',
                        hideLabel: false,
                        name: 'kode_belakang',
                        id: 'kode_belakangid',
                        ref: '../kode_belakang',
                        maxLength: 10,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Harga',
                        hideLabel: false,
                        //hidden:true,
                        name: 'harga',
                        id: 'hargaid',
                        ref: '../harga',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: "checkbox",
                        fieldLabel: 'Generate Voucher',
                        // boxLabel: "Country",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "voucher"
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ProdukWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Produk/update/id/' + this.id;
        } else {
            urlz = 'Produk/create/';
        }
        Ext.getCmp('form-Produk').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztProduk.reload();
                jun.rztProdukCmp.reload();
                jun.rztProdukLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Produk').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});