jun.LoketWin = Ext.extend(Ext.Window, {
    title: 'Nota',
    modez: 1,
    width: 740,
    height: 580,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Loket',
                layout: 'table',
                layoutConfig: {columns: 2},
                defaults: {width: 350},
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        layout: 'form',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 175,
                        items: [
                            {
                                xtype: 'combo',
                                ref: '../../reservasi',
                                fieldLabel: 'Reservasi',
                                triggerAction: 'query',
                                lazyRender: true,
                                mode: 'remote',
                                forceSelection: true,
                                autoSelect: false,
                                store: jun.rztReservasiViewLoketCmp,
                                id: "loket_reservasi_id",
                                hiddenName: 'reservasi_id',
                                valueField: 'reservasi_id',
                                displayField: 'ref',
                                hideTrigger: true,
                                minChars: 3,
                                matchFieldWidth: !1,
                                pageSize: 20,
                                itemSelector: 'div.search-item',
                                tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">',
                                    '<h3><span>{ref}<br />{no_telp}</span>{nama}</h3>',
                                    '{alamat}',
                                    '</div></tpl>'
                                ),
                                allowBlank: true,
                                listWidth: 550,
                                lastQuery: "",
                                anchor: '100%'
                            },
                            {
                                xtype: 'mfcombobox',
                                style: 'margin-bottom:2px',
                                searchFields: [
                                    'nama'
                                ],
                                mode: 'local',
                                store: jun.rztMemberCmp,
                                valueField: 'member_id',
                                itemSelector: 'tr.search-item',
                                tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                    '<th>Nama</th></tr></thead>',
                                    '<tbody><tpl for="."><tr class="search-item">',
                                    '<td>{nama}</td>',
                                    '</tr></tpl></tbody></table>'),
                                // width: 175,
                                allowBlank: false,
                                enableKeyEvents: true,
                                // listWidth: 400,
                                displayField: 'nama',
                                fieldLabel: 'Member',
                                hideLabel: false,
                                //hidden:true,
                                hiddenName: 'member_id',
                                ref: '../../member_id',
                                maxLength: 36,
                                allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama',
                                hideLabel: false,
                                //hidden:true,
                                name: 'nama',
                                id: 'namaid',
                                ref: '../../nama',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Telp',
                                hideLabel: false,
                                //hidden:true,
                                name: 'no_telp',
                                id: 'no_telpid',
                                ref: '../../no_telp',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Alamat',
                                hideLabel: false,
                                //hidden:true,
                                name: 'alamat',
                                id: 'alamatid',
                                ref: '../../alamat',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'INVD/GRUP',
                                store: jun.tipe_tamu,
                                hiddenName: 'tipe_tamu',
                                valueField: 'val',
                                displayField: 'label',
                                ref: '../tipe_tamu',
                                allowBlank: false,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 150,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 175,
                        items: [
                            {
                                xtype: 'compositefield',
                                fieldLabel: 'Tgl/Jam Tour',
                                msgTarget: 'side',
                                defaults: {
                                    flex: 1
                                },
                                items: [
                                    {
                                        xtype: 'xdatefield',
                                        ref: '../waktu_tour',
                                        fieldLabel: 'Tgl/Jam Tour',
                                        name: 'waktu_tour_date',
                                        format: 'd-m-Y',
                                        ref: '../../../waktu_tour_date',
                                        value: DATE_NOW,
                                        readOnly: true,
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'timefield',
                                        ref: '../../../waktu_tour_time',
                                        format: 'H:i',
                                        altFormats: 'H:i',
                                        name: 'waktu_tour_time'
                                    }
                                ]
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Bahasa',
                                store: jun.rztLangTourCmp,
                                hiddenName: 'lang_tour_id',
                                valueField: 'lang_tour_id',
                                displayField: 'nama',
                                value: LANG_DEFAULT,
                                ref: '../lang_tour_id',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Kategori Jenis Tour',
                                store: jun.rztJenisTourCmp,
                                hiddenName: 'jenis_tour_id',
                                valueField: 'jenis_tour_id',
                                displayField: 'nama',
                                ref: '../lang_tour_id',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Kategori Pengunjung',
                                store: jun.rztKategoriCustCmp,
                                hiddenName: 'kategori_cust_id',
                                valueField: 'kategori_cust_id',
                                displayField: 'nama',
                                ref: '../kategori_cust_id',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Note',
                                hideLabel: false,
                                // height: 76,
                                // hidden: true,
                                name: 'note_',
                                id: 'note_id',
                                ref: '../note_',
                                maxLength: 600,
                                //allowBlank: 1,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        colspan: 2,
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        height: 220,
                        width: 700,
                        layout: 'form',
                        border: false,
                        items: [
                            new jun.LoketDetilGrid({
                                // colspan: 2,
                                height: 210,
                                // width: 700,
                                frameHeader: !1,
                                header: !1,
                                ref: "../griddetil"
                            }),
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        border: false,
                        height: 90,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Sub Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'sub_total',
                                id: 'sub_total_loketid',
                                ref: '../sub_total',
                                maxLength: 15,
                                readOnly: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Uang Muka',
                                hideLabel: false,
                                //hidden:true,
                                name: 'uang_muka',
                                id: 'uang_muka_loketid',
                                ref: '../uang_muka',
                                maxLength: 15,
                                readOnly: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'total',
                                id: 'total_loketid',
                                ref: '../../total',
                                maxLength: 15,
                                readOnly: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        height: 90,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Bayar',
                                hideLabel: false,
                                //hidden:true,
                                name: 'bayar',
                                id: 'bayarid',
                                ref: '../../bayar',
                                maxLength: 15,
                                enableKeyEvents: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Kembalian',
                                hideLabel: false,
                                //hidden:true,
                                name: 'kembalian',
                                id: 'kembalianid',
                                ref: '../../kembalian',
                                maxLength: 15,
                                readOnly: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            },
                        ]
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.LoketWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.reservasi.on('select', this.onReservasiSelect, this);
        this.member_id.on('select', this.onMemberSelect, this);
        this.bayar.on('keyup', this.onBayarChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            if (this.modez == 2) {
                this.btnSaveClose.setVisible(false);
            }
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onBayarChange: function () {
        jun.rztLoketDetil.refreshData();
    },
    onMemberSelect: function (c, r, i) {
        this.nama.setValue(r.data.nama);
        this.no_telp.setValue(r.data.no_telp);
        this.alamat.setValue(r.data.alamat);
    },
    onMemberChange: function (t, n, o) {
        if (n == '') {
            this.nama.setValue('');
            this.no_telp.setValue('');
            this.alamat.setValue('');
            // this.nama.setReadOnly(false);
        } else {
            // this.nama.setReadOnly(true);
        }
    },
    onWinClose: function () {
        jun.rztLoketDetil.removeAll();
    },
    onReservasiSelect: function (c, r, i) {
        this.formz.getForm().loadRecord(r);
        jun.rztLoketDetil.load({
            params: {
                reservasi_id: r.data.reservasi_id
            }
        });
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var bayar = this.bayar.getValue();
        var total = this.total.getValue();
        var kembalian = bayar - total;
        if (kembalian < 0) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Pembayaran tidak mencukupi.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            this.btnDisabled(false);
            return;
        }
        var urlz = 'Loket/create/';
        // if (this.modez == 1 || this.modez == 2) {
        //     urlz = 'Loket/update/id/' + this.id;
        // } else {
        //     urlz = 'Loket/create/';
        // }
        Ext.getCmp('form-Loket').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztLoketDetil.data.items, "data")),
                mode: this.modez,
                id: this.id
            },
            success: function (f, a) {
                jun.rztLoketView.reload();
                var response = Ext.decode(a.response.responseText);
                // Ext.MessageBox.show({
                //     title: 'Info',
                //     msg: response.msg,
                //     buttons: Ext.MessageBox.OK,
                //     icon: Ext.MessageBox.INFO
                // });
                var msg = [{type: 'raw', data: response.msg}];
                var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                printRawQZ('default', printData);
                if (this.modez == 0) {
                    Ext.getCmp('form-Loket').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.LoketNewWin = Ext.extend(Ext.Window, {
    title: 'Nota',
    modez: 1,
    width: 740,
    height: 610,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-LoketNew',
                layout: 'table',
                layoutConfig: {columns: 2},
                defaults: {width: 350},
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 205,
                        items: [
                            {
                                xtype: 'combo',
                                ref: '../../reservasi',
                                fieldLabel: 'Reservasi',
                                triggerAction: 'query',
                                lazyRender: true,
                                mode: 'remote',
                                forceSelection: true,
                                autoSelect: false,
                                store: jun.rztReservasiViewLoketCmp,
                                id: "loket_reservasi_id",
                                hiddenName: 'reservasi_id',
                                valueField: 'reservasi_id',
                                displayField: 'ref',
                                hideTrigger: true,
                                minChars: 3,
                                matchFieldWidth: !1,
                                pageSize: 20,
                                itemSelector: 'div.search-item',
                                tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">',
                                    '<h3><span>{ref}<br />{no_telp}</span>{nama}</h3>',
                                    '{alamat}',
                                    '</div></tpl>'
                                ),
                                allowBlank: true,
                                listWidth: 550,
                                lastQuery: "",
                                anchor: '100%'
                            },
                            {
                                xtype: 'mfcombobox',
                                style: 'margin-bottom:2px',
                                searchFields: [
                                    'nama'
                                ],
                                mode: 'local',
                                store: jun.rztMemberCmp,
                                valueField: 'member_id',
                                itemSelector: 'tr.search-item',
                                tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                    '<th>Nama</th></tr></thead>',
                                    '<tbody><tpl for="."><tr class="search-item">',
                                    '<td>{nama}</td>',
                                    '</tr></tpl></tbody></table>'),
                                // width: 175,
                                allowBlank: false,
                                enableKeyEvents: true,
                                // listWidth: 400,
                                displayField: 'nama',
                                fieldLabel: 'Member',
                                hideLabel: false,
                                //hidden:true,
                                hiddenName: 'member_id',
                                ref: '../../member_id',
                                maxLength: 36,
                                allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama',
                                hideLabel: false,
                                //hidden:true,
                                name: 'nama',
                                id: 'namaid',
                                ref: '../../nama',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Telp',
                                hideLabel: false,
                                //hidden:true,
                                name: 'no_telp',
                                id: 'no_telpid',
                                ref: '../../no_telp',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Alamat',
                                hideLabel: false,
                                //hidden:true,
                                name: 'alamat',
                                id: 'alamatid',
                                ref: '../../alamat',
                                maxLength: 100,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'INVD/GRUP',
                                store: jun.tipe_tamu,
                                hiddenName: 'tipe_tamu',
                                valueField: 'val',
                                displayField: 'label',
                                ref: '../tipe_tamu',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Note',
                                hideLabel: false,
                                // height: 76,
                                // hidden: true,
                                name: 'note_',
                                id: 'note_id',
                                ref: '../note_',
                                maxLength: 600,
                                //allowBlank: 1,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 50,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 205,
                        items: [
                            {
                                xtype: 'compositefield',
                                fieldLabel: 'Tgl',
                                msgTarget: 'side',
                                defaults: {
                                    flex: 1
                                },
                                items: [
                                    {
                                        xtype: 'xdatefield',
                                        ref: '../waktu_tour',
                                        fieldLabel: 'Tgl/Jam Tour',
                                        name: 'waktu_tour_date',
                                        format: 'd-m-Y',
                                        ref: '../../../waktu_tour_date',
                                        value: DATE_NOW,
                                        readOnly: true,
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'timefield',
                                        ref: '../../../waktu_tour_time',
                                        format: 'H:i',
                                        altFormats: 'H:i',
                                        name: 'waktu_tour_time'
                                    }
                                ]
                            },
                            {
                                xtype: 'radiogroup',
                                fieldLabel: 'Bahasa',
                                items: [
                                    {boxLabel: 'Indonesia', name: 'lang_tour_id', inputValue: 'Indonesia', checked: true},
                                    {boxLabel: 'Inggris', name: 'lang_tour_id', inputValue: 'Inggris'},
                                ]
                            },
                            {
                                xtype: 'radiogroup',
                                fieldLabel: 'Tur',
                                items: [
                                    {boxLabel: 'Adiluhung Mataram', name: 'jenis_tour_id', inputValue: 'Adiluhung Mataram', checked: true},
                                    {boxLabel: 'Vorstenlanden', name: 'jenis_tour_id', inputValue: 'Vorstenlanden'},
                                ]
                            },
                            {
                                xtype: 'panel',
                                fieldLabel: 'Tiket',
                                hideLabel: false,
                                height: 85,
                                layout:'column',
                                bodyStyle: 'background-color: #E4E4E4;',
                                border: false,
                                // hidden: true,
                                // name: 'note_',
                                // id: 'note_id',
                                // ref: '../note_',
                                //allowBlank: 1,
                                anchor: '100%',
                                items: [
                                    {
                                        xtype: 'panel',
                                        hideLabel: false,
                                        height: 85,
                                        layout:'form',
                                        bodyStyle: 'background-color: #E4E4E4; padding: 5px',
                                        border: false,
                                        labelWidth: 25,
                                        labelAlign: 'left',
                                        columnWidth: .33,
                                        maxLength: 600,
                                        //allowBlank: 1,
                                        anchor: '100%',
                                        items: [
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'RA',
                                                hideLabel: false,
                                                value: 0,
                                                id: 'raid',
                                                ref: '../ra',
                                                maxLength: 11,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'RC',
                                                hideLabel: false,
                                                value: 0,
                                                id: 'rcid',
                                                ref: '../rc',
                                                maxLength: 11,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'RF',
                                                hideLabel: false,
                                                value: 0,
                                                id: 'rfid',
                                                ref: '../rf',
                                                maxLength: 11,
                                                anchor: '100%'
                                            },
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        hideLabel: false,
                                        height: 85,
                                        layout:'form',
                                        bodyStyle: 'background-color: #E4E4E4; padding: 5px',
                                        border: false,
                                        labelWidth: 25,
                                        labelAlign: 'left',
                                        columnWidth: .33,
                                        maxLength: 600,
                                        //allowBlank: 1,
                                        anchor: '100%',
                                        items: [
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'EA',
                                                hideLabel: false,
                                                value: 0,
                                                id: 'eaid',
                                                ref: '../ea',
                                                maxLength: 11,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'EC',
                                                hideLabel: false,
                                                value: 0,
                                                id: 'ecid',
                                                ref: '../ec',
                                                maxLength: 11,
                                                anchor: '100%'
                                            },
                                        ]
                                    },
                                    {
                                        xtype: 'panel',
                                        hideLabel: false,
                                        height: 85,
                                        layout:'form',
                                        bodyStyle: 'background-color: #E4E4E4; padding: 5px',
                                        border: false,
                                        labelWidth: 25,
                                        labelAlign: 'left',
                                        columnWidth: .33,
                                        maxLength: 600,
                                        //allowBlank: 1,
                                        anchor: '100%',
                                        items: [
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'VA',
                                                hideLabel: false,
                                                value: 0,
                                                id: 'vaid',
                                                ref: '../va',
                                                maxLength: 11,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'numericfield',
                                                fieldLabel: 'VC',
                                                hideLabel: false,
                                                value: 0,
                                                id: 'vcid',
                                                ref: '../vc',
                                                maxLength: 11,
                                                anchor: '100%'
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Kode',
                                hideLabel: false,
                                value: randomString(5,'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
                                // height: 76,
                                // hidden: true,
                                readOnly: true,
                                name: 'kode_',
                                id: 'kode_id',
                                ref: '../kode_',
                                maxLength: 5,
                                //allowBlank: 1,
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        colspan: 2,
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        height: 220,
                        width: 700,
                        layout: 'form',
                        border: false,
                        items: [
                            new jun.LoketDetilGrid({
                                // colspan: 2,
                                height: 210,
                                // width: 700,
                                frameHeader: !1,
                                header: !1,
                                ref: "../griddetil"
                            }),
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        border: false,
                        height: 90,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Sub Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'sub_total',
                                id: 'sub_total_loketid',
                                ref: '../sub_total',
                                maxLength: 15,
                                readOnly: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Uang Muka',
                                hideLabel: false,
                                //hidden:true,
                                name: 'uang_muka',
                                id: 'uang_muka_loketid',
                                ref: '../uang_muka',
                                maxLength: 15,
                                readOnly: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'total',
                                id: 'total_loketid',
                                ref: '../../total',
                                maxLength: 15,
                                readOnly: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        height: 90,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Bayar',
                                hideLabel: false,
                                //hidden:true,
                                name: 'bayar',
                                id: 'bayarid',
                                ref: '../../bayar',
                                maxLength: 15,
                                enableKeyEvents: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Kembalian',
                                hideLabel: false,
                                //hidden:true,
                                name: 'kembalian',
                                id: 'kembalianid',
                                ref: '../../kembalian',
                                maxLength: 15,
                                readOnly: true,
                                //allowBlank: 1,
                                value: 0,
                                anchor: '100%'
                            },
                        ]
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.LoketNewWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.reservasi.on('select', this.onReservasiSelect, this);
        this.member_id.on('select', this.onMemberSelect, this);
        this.bayar.on('keyup', this.onBayarChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            if (this.modez == 2) {
                this.btnSaveClose.setVisible(false);
            }
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onBayarChange: function () {
        jun.rztLoketDetil.refreshData();
    },
    onMemberSelect: function (c, r, i) {
        this.nama.setValue(r.data.nama);
        this.no_telp.setValue(r.data.no_telp);
        this.alamat.setValue(r.data.alamat);
    },
    onMemberChange: function (t, n, o) {
        if (n == '') {
            this.nama.setValue('');
            this.no_telp.setValue('');
            this.alamat.setValue('');
            // this.nama.setReadOnly(false);
        } else {
            // this.nama.setReadOnly(true);
        }
    },
    onWinClose: function () {
        jun.rztLoketDetil.removeAll();
    },
    onReservasiSelect: function (c, r, i) {
        this.formz.getForm().loadRecord(r);
        jun.rztLoketDetil.load({
            params: {
                reservasi_id: r.data.reservasi_id
            }
        });
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var bayar = this.bayar.getValue();
        var total = this.total.getValue();
        var kembalian = bayar - total;
        if (kembalian < 0) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Pembayaran tidak mencukupi.',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            this.btnDisabled(false);
            return;
        }
        var urlz = 'Loket/create/';
        // if (this.modez == 1 || this.modez == 2) {
        //     urlz = 'Loket/update/id/' + this.id;
        // } else {
        //     urlz = 'Loket/create/';
        // }
        Ext.getCmp('form-Loket').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztLoketDetil.data.items, "data")),
                mode: this.modez,
                id: this.id
            },
            success: function (f, a) {
                jun.rztLoketView.reload();
                var response = Ext.decode(a.response.responseText);
                // Ext.MessageBox.show({
                //     title: 'Info',
                //     msg: response.msg,
                //     buttons: Ext.MessageBox.OK,
                //     icon: Ext.MessageBox.INFO
                // });
                var msg = [{type: 'raw', data: response.msg}];
                var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                printRawQZ('default', printData);
                if (this.modez == 0) {
                    Ext.getCmp('form-LoketNew').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.LoketReturWin = Ext.extend(Ext.Window, {
    title: 'Nota Retur',
    modez: 1,
    width: 740,
    height: 540,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Loket',
                layout: 'table',
                layoutConfig: {columns: 2},
                defaults: {width: 350},
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        layout: 'form',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 175,
                        items: [
                            {
                                xtype: 'combo',
                                ref: '../../loket_id',
                                fieldLabel: 'Nota',
                                triggerAction: 'query',
                                lazyRender: true,
                                mode: 'remote',
                                forceSelection: true,
                                autoSelect: false,
                                store: jun.rztLoketViewCmp,
                                id: "loket_retur_id",
                                hiddenName: 'parent_loket_id',
                                valueField: 'loket_id',
                                displayField: 'ref',
                                hideTrigger: true,
                                minChars: 3,
                                matchFieldWidth: !1,
                                pageSize: 20,
                                itemSelector: 'div.search-item',
                                tpl: new Ext.XTemplate(
                                    '<tpl for="."><div class="search-item">',
                                    '<h3><span>{ref}<br />{no_telp}</span>{nama}</h3>',
                                    '{alamat}',
                                    '</div></tpl>'
                                ),
                                allowBlank: false,
                                listWidth: 450,
                                lastQuery: "",
                                anchor: '100%'
                            },
                            {
                                xtype: 'mfcombobox',
                                style: 'margin-bottom:2px',
                                searchFields: [
                                    'nama'
                                ],
                                mode: 'local',
                                store: jun.rztMemberCmp,
                                valueField: 'member_id',
                                itemSelector: 'tr.search-item',
                                tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                    '<th>Nama</th></tr></thead>',
                                    '<tbody><tpl for="."><tr class="search-item">',
                                    '<td>{nama}</td>',
                                    '</tr></tpl></tbody></table>'),
                                // width: 175,
                                // allowBlank: false,
                                enableKeyEvents: true,
                                // listWidth: 400,
                                displayField: 'nama',
                                fieldLabel: 'Member',
                                hideLabel: false,
                                //hidden:true,
                                hiddenName: 'member_id',
                                ref: '../member_id',
                                maxLength: 36,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama',
                                hideLabel: false,
                                //hidden:true,
                                name: 'nama',
                                id: 'namaid',
                                ref: '../../nama',
                                maxLength: 100,
                                // readOnly: true,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Telp',
                                hideLabel: false,
                                //hidden:true,
                                name: 'no_telp',
                                id: 'no_telpid',
                                ref: '../no_telp',
                                maxLength: 100,
                                // readOnly: true,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Alamat',
                                hideLabel: false,
                                //hidden:true,
                                name: 'alamat',
                                id: 'alamatid',
                                ref: '../alamat',
                                maxLength: 100,
                                // readOnly: true,
                                //allowBlank: 1,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'INVD/GRUP',
                                store: jun.tipe_tamu,
                                hiddenName: 'tipe_tamu',
                                valueField: 'val',
                                displayField: 'label',
                                ref: '../tipe_tamu',
                                allowBlank: false,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 120,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        height: 175,
                        items: [
                            {
                                xtype: 'compositefield',
                                fieldLabel: 'Tgl/Jam Tour',
                                msgTarget: 'side',
                                defaults: {
                                    flex: 1
                                },
                                items: [
                                    {
                                        xtype: 'xdatefield',
                                        ref: '../waktu_tour',
                                        fieldLabel: 'Tgl/Jam Tour',
                                        name: 'waktu_tour_date',
                                        format: 'd-m-Y',
                                        readOnly: true,
                                        ref: '../../../waktu_tour_date',
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'timefield',
                                        ref: '../../../waktu_tour_time',
                                        format: 'H:i',
                                        readOnly: true,
                                        altFormats: 'H:i',
                                        name: 'waktu_tour_time'
                                    }
                                ]
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Bahasa',
                                store: jun.rztLangTourCmp,
                                hiddenName: 'lang_tour_id',
                                valueField: 'lang_tour_id',
                                displayField: 'nama',
                                readOnly: true,
                                ref: '../lang_tour_id',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Kategori Jenis Tour',
                                store: jun.rztJenisTourCmp,
                                hiddenName: 'jenis_tour_id',
                                valueField: 'jenis_tour_id',
                                displayField: 'nama',
                                readOnly: true,
                                ref: '../lang_tour_id',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Kategori Pengunjung',
                                store: jun.rztKategoriCustCmp,
                                hiddenName: 'kategori_cust_id',
                                valueField: 'kategori_cust_id',
                                displayField: 'nama',
                                readOnly: true,
                                ref: '../kategori_cust_id',
                                allowBlank: false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Note',
                                hideLabel: false,
                                // height: 76,
                                // hidden: true,
                                name: 'note_',
                                // readOnly: true,
                                id: 'note_id',
                                ref: '../note_',
                                maxLength: 600,
                                //allowBlank: 1,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        colspan: 2,
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        height: 220,
                        width: 700,
                        layout: 'form',
                        border: false,
                        items: [
                            new jun.LoketDetilReturGrid({
                                // colspan: 2,
                                height: 210,
                                // width: 700,
                                frameHeader: !1,
                                header: !1,
                                ref: "../griddetil"
                            })
                        ]
                    },
                    {
                        xtype: 'panel',
                        bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                        frame: false,
                        border: false,
                        height: 75,
                        layout: {
                            type: 'vbox',
                            // padding: '5',
                            align: 'stretchmax'
                        },
                        items: [
                            {
                                xtype: 'label',
                                text: 'TOTAL'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'total',
                                hideLabel: false,
                                readOnly: true,
                                //hidden:true,
                                value: 0,
                                name: 'total',
                                id: 'total_loketid',
                                ref: '../total',
                                maxLength: 15,
                                //allowBlank: 1,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        frame: false,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        labelWidth: 100,
                        labelAlign: 'left',
                        layout: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Sub Total',
                                hideLabel: false,
                                //hidden:true,
                                name: 'sub_total',
                                id: 'sub_total_loketid',
                                ref: '../sub_total',
                                maxLength: 15,
                                value: 0,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'hidden',
                                id: 'bayarid',
                            },
                            {
                                xtype: 'hidden',
                                id: 'uang_muka_loketid',
                            },
                            {
                                xtype: 'hidden',
                                id: 'kembalianid',
                            }
                        ]
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: true,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.LoketReturWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.reservasi.on('select', this.onReservasiSelect, this);
        this.on("close", this.onWinClose, this);
        // if (this.modez == 1 || this.modez == 2) {
        //     this.btnSave.setVisible(false);
        // } else {
        //     this.btnSave.setVisible(true);
        // }
    },
    onActivate: function () {
        jun.rztLoketDetil.refreshData();
    },
    onWinClose: function () {
        jun.rztLoketDetil.removeAll();
    },
    onReservasiSelect: function (c, r, i) {
        this.formz.getForm().loadRecord(r);
        jun.rztLoketDetil.load({
            params: {
                reservasi_id: r.data.reservasi_id
            }
        });
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Loket/CreateRetur/';
        // if (this.modez == 1 || this.modez == 2) {
        //     urlz = 'Loket/update/id/' + this.id;
        // } else {
        //     urlz = 'Loket/CreateRetur/';
        // }
        Ext.getCmp('form-Loket').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztLoketDetil.data.items, "data")),
                mode: this.modez,
                id: this.id
            },
            success: function (f, a) {
                jun.rztLoketReturView.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Loket').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});