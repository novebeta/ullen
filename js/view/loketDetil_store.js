jun.LoketDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.LoketDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LoketDetilStoreId',
            url: 'LoketDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'loket_detil_id'},
                {name: 'loket_id'},
                {name: 'produk_id'},
                {name: 'qty', type: 'float'},
                {name: 'harga', type: 'float'},
                {name: 'sub_total', type: 'float'},
                {name: 'point', type: 'float'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
        // this.on('load', this.refreshData, this);
    },
    refreshData: function () {
        var sub_total = this.sum("sub_total");
        var uang_muka = Ext.getCmp('uang_muka_loketid').getValue();
        var bayar = Ext.getCmp('bayarid').getValue();
        var total = sub_total - uang_muka;
        var kembalian = bayar - total;
        if (kembalian < 0) {
            kembalian = 0;
        }
        Ext.getCmp('sub_total_loketid').setValue(sub_total);
        Ext.getCmp('total_loketid').setValue(total);
        Ext.getCmp('kembalianid').setValue(kembalian);
    }
});
jun.rztLoketDetil = new jun.LoketDetilstore();
jun.rztLoketDetil.on({
    scope: this,
    load: {
        fn: function (a, b) {
            a.refreshData();
        }
    }
});
//jun.rztLoketDetil.load();
