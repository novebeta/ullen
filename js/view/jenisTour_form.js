jun.JenisTourWin = Ext.extend(Ext.Window, {
    title: 'Kategori Jenis Tour',
    modez: 1,
    width: 400,
    height: 230,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-JenisTour',
                labelWidth: 165,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama',
                        id: 'namaid',
                        ref: '../nama',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode',
                        id: 'kodeid',
                        ref: '../kode',
                        maxLength: 5,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'compositefield',
                        fieldLabel: 'Free Ticket',
                        msgTarget: 'side',
                        defaults: {
                            flex: 1
                        },
                        items: [
                            {
                                xtype: 'checkbox',
                                fieldLabel: 'Free Ticket',
                                boxLabel: "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Qty:",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "free_ticket"
                            },
                            {
                                xtype: 'numericfield',
                                fieldLabel: 'Free Ticket',
                                hideLabel: false,
                                //hidden:true,
                                name: 'min_cust',
                                id: 'min_custid',
                                ref: '../min_cust',
                                width: 100,
                                maxLength: 11,
                                //allowBlank: ,
                                // anchor: '100%'
                            },
                        ]
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Maks Person in Grup',
                        hideLabel: false,
                        //hidden:true,
                        value: 0,
                        name: 'maks_grup',
                        id: 'maks_grupid',
                        ref: '../maks_grup',
                        // width: 100,
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Aktif',
                        boxLabel: "",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "status"
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.JenisTourWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'JenisTour/update/id/' + this.id;
        } else {
            urlz = 'JenisTour/create/';
        }
        Ext.getCmp('form-JenisTour').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztJenisTour.reload();
                jun.rztJenisTourCmp.reload();
                jun.rztJenisTourLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-JenisTour').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});