jun.ListTicketstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ListTicketstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ListTicketStoreId',
            url: 'TiketTrans/ListTicket',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'loket_detil_id'},
                {name: 'loket_id'},
                {name: 'produk_id'},
                {name: 'qty'},
                {name: 'nama'}
            ]
        }, cfg));
    }
});
jun.rztListTicket = new jun.ListTicketstore();
//jun.rztListTicket.load();
