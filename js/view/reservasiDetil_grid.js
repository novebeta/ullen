jun.ReservasiDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ReservasiDetil",
    id: 'docs-jun.ReservasiDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Produk',
            sortable: true,
            resizable: true,
            dataIndex: 'produk_id',
            width: 100,
            renderer: jun.renderProduk
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Harga',
            sortable: true,
            resizable: true,
            dataIndex: 'harga',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Sub Total',
            sortable: true,
            resizable: true,
            dataIndex: 'sub_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztReservasiDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Produk :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            colspan: 3,
                            forceSelection: true,
                            store: jun.rztProdukCmp,
                            // hiddenName: 'produk_id',
                            valueField: 'produk_id',
                            displayField: 'nama',
                            ref: '../../produk_id',
                            allowBlank: false,
                            width: 200
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Harga :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../harga',
                            width: 75,
                            readOnly: false,
                            value: 0,
                            minValue: 0
                        },
                        // {
                        //     xtype: 'label',
                        //     style: 'margin:5px',
                        //     text: 'Total :'
                        // },
                        // {
                        //     xtype: 'numericfield',
                        //     id: 'totalid',
                        //     ref: '../../total',
                        //     width: 100,
                        //     readOnly: true,
                        //     value: 0,
                        //     minValue: 0
                        // }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large',
                        width: 40
                        //height: 44
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.ReservasiDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.produk_id.on('select', this.onSelectProduk, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onSelectProduk: function(c,r,i){
        this.harga.setValue(r.data.harga);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var produk_id = this.produk_id.getValue();
        if (produk_id == "") {
            Ext.MessageBox.alert("Error", "Produk harus dipilih.");
            return
        }
        var harga = parseFloat(this.harga.getValue());
        var qty = parseFloat(this.qty.getValue());
        var sub_total = harga * qty;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('produk_id', produk_id);
            record.set('qty', qty);
            record.set('harga', harga);
            record.set('sub_total', sub_total);
            record.commit();
        } else {
            var c = jun.rztReservasiDetil.recordType,
                d = new c({
                    produk_id: produk_id,
                    qty: qty,
                    harga: harga,
                    sub_total: sub_total
                });
            jun.rztReservasiDetil.add(d);
        }
        this.produk_id.reset();
        this.qty.reset();
        this.harga.reset();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.reservasi_detil_id;
        var form = new jun.ReservasiDetilWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.produk_id.setValue(record.data.produk_id);
            // this.onChangeBarang();
            this.qty.setValue(record.data.qty);
            this.harga.setValue(record.data.harga);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus item ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
        this.store.refreshData()
    }
})
