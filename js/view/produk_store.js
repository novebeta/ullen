jun.Produkstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Produkstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ProdukStoreId',
            url: 'Produk',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'produk_id'},
                {name: 'nama'},
                {name: 'harga'},
                {name: 'jenis_tour_id'},
                {name: 'kode_depan'},
                {name: 'kode_belakang'},
                {name: 'voucher'}
            ]
        }, cfg));
    }
});
jun.rztProduk = new jun.Produkstore();
jun.rztProdukCmp = new jun.Produkstore();
jun.rztProdukLib = new jun.Produkstore();
jun.rztProdukLib.load();
