jun.GrupTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.GrupTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GrupTransStoreId',
            url: 'GrupTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'grup_trans_id'},
                {name: 'date_checkin'},
                {name: 'jenis_tour_id'},
                {name: 'lang_tour_id'},
                {name: 'qty'},
                {name: 'start'},
                {name: 'end_'},
                {name: 'waktu_masuk'},
                {name: 'time_checkin'},
                {name: 'guide_id'},
                {name: 'prepare'},
                {name: 'waiting'},
                {name: 'no_urut'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'nama'},
                {name: 'qty_real'},
                {name: 'nama_kat'}
            ]
        }, cfg));
    }
});
jun.rztGrupTrans = new jun.GrupTransstore();
//jun.rztGrupTrans.load();
