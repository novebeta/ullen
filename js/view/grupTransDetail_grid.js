jun.GrupTransDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "GrupTransDetail",
    id: 'docs-jun.GrupTransDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Nota',
            sortable: true,
            resizable: true,
            dataIndex: 'ref',
            width: 200
        },
        {
            header: 'Kategori',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_kat',
            width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat',
            width: 100
        },
        {
            header: 'Jml',
            sortable: true,
            resizable: true,
            dataIndex: 'jml',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
    ],
    initComponent: function () {
        this.store = jun.rztGrupTransDetail;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Ticket / Kwitansi',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'Ubah',
                //     ref: '../btnEdit'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Hapus Ticket / Kwitansi',
                    ref: '../btnDelete'
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.GrupTransDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.GrupAddTiketWin({modez: 0, grup_trans_id: this.grup_trans_id});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.grup_trans_detail_id;
        var form = new jun.GrupTransDetailWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'GrupTransDetail/delete/grup_trans_id/' + record.json.grup_trans_id + '/loket_id/' + record.json.loket_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztGrupTransDetail.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.TiketLoketGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Daftar Kwitansi",
    id: 'docs-jun.TiketLoketGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel(),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Nota',
            sortable: true,
            resizable: true,
            dataIndex: 'ref',
            width: 200
        },
        {
            header: 'Kategori',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_kat',
            width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat',
            width: 100
        },
        // {
        //     header: 'jenis_tour_id',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'jenis_tour_id',
        //     width: 100
        // },
        // {
        //     header: 'lang_tour_id',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'lang_tour_id',
        //     width: 100
        // },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztLoketTicket;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        // this.tbar = {
        //     xtype: 'toolbar',
        //     items: [
        //         {
        //             xtype: 'button',
        //             text: 'Tambah',
        //             ref: '../btnAdd'
        //         },
        //         {
        //             xtype: 'tbseparator'
        //         },
        //         {
        //             xtype: 'button',
        //             text: 'Ubah',
        //             ref: '../btnEdit'
        //         },
        //         {
        //             xtype: 'tbseparator'
        //         },
        //         {
        //             xtype: 'button',
        //             text: 'Buat Voucher',
        //             ref: '../btnDelete'
        //         }
        //     ]
        // };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.TiketLoketGrid.superclass.initComponent.call(this);
        if (jun.rztMemberCmp.getTotalCount() === 0) {
            jun.rztMemberCmp.load();
        }
        if (jun.rztLangTourCmp.getTotalCount() === 0) {
            jun.rztLangTourCmp.load();
        }
        if (jun.rztJenisTourCmp.getTotalCount() === 0) {
            jun.rztJenisTourCmp.load();
        }
        if (jun.rztKategoriCustCmp.getTotalCount() === 0) {
            jun.rztKategoriCustCmp.load();
        }
        if (jun.rztProdukCmp.getTotalCount() === 0) {
            jun.rztProdukCmp.load();
        }
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztListTicket.load({
            params: {
                loket_id: r.data.loket_id
            }
        });
    },
    loadForm: function () {
        var form = new jun.LoketWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.loket_id;
        var form = new jun.LoketWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztLoketDetil.load({
            params: {
                loket_id: idz
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membuat voucher dari data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Loket/voucher/id/' + record.json.loket_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztLoket.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
