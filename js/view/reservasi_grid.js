jun.ReservasiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Reservasi",
    id: 'docs-jun.ReservasiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Reservasi',
            sortable: true,
            resizable: true,
            dataIndex: 'ref',
            width: 100
        },
        {
            header: 'Tgl Kunjungan',
            sortable: true,
            resizable: true,
            dataIndex: 'waktu_tour_date',
            width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'No. Telp',
            sortable: true,
            resizable: true,
            dataIndex: 'no_telp',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat',
            width: 100
        },
        {
            header: 'Jenis Tour',
            sortable: true,
            resizable: true,
            dataIndex: 'jenis_tour_id',
            width: 100,
            renderer: jun.renderJenisTour
        },
        {
            header: 'Bahasa',
            sortable: true,
            resizable: true,
            dataIndex: 'lang_tour_id',
            width: 100,
            renderer: jun.renderLangTour
        },
        {
            header: 'Kategori Pengunjung',
            sortable: true,
            resizable: true,
            dataIndex: 'kat_cust'
        },
        {
            header: 'Status Bayar',
            sortable: true,
            resizable: true,
            dataIndex: 'status_bayar',
            width: 100
        },
        // {
        //     header: 'waktu_reservasi',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'waktu_reservasi',
        //     width: 100
        // },
        // {
        //     header: 'waktu_tour',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'waktu_tour',
        //     width: 100
        // },
// {
// header:'note_',
// sortable:true,
// resizable:true,
// dataIndex:'note_',
// width:100
// },
        {
            header: 'Sub Total',
            sortable: true,
            resizable: true,
            dataIndex: 'sub_total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Uang Muka',
            sortable: true,
            resizable: true,
            dataIndex: 'uang_muka',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztReservasi;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Reservasi',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Reservasi',
                    ref: '../btnEdit'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.ReservasiGrid.superclass.initComponent.call(this);
        if (jun.rztMemberCmp.getTotalCount() === 0) {
            jun.rztMemberCmp.load();
        }
        if (jun.rztLangTourCmp.getTotalCount() === 0) {
            jun.rztLangTourCmp.load();
        }
        if (jun.rztJenisTourCmp.getTotalCount() === 0) {
            jun.rztJenisTourCmp.load();
        }
        if (jun.rztKategoriCustCmp.getTotalCount() === 0) {
            jun.rztKategoriCustCmp.load();
        }
        if (jun.rztProdukCmp.getTotalCount() === 0) {
            jun.rztProdukCmp.load();
        }
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ReservasiWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih reservasi");
            return;
        }
        if (this.record.data.loket_id != null) {
            Ext.MessageBox.alert("Warning", "Reservasi tidak bisa di edit karena sudah di notakan.");
            return;
        }
        var idz = selectedz.json.reservasi_id;
        var form = new jun.ReservasiWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztReservasiDetil.load({
            params: {
                reservasi_id: idz
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Reservasi/delete/id/' + record.json.reservasi_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReservasi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
