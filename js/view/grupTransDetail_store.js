jun.GrupTransDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.GrupTransDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GrupTransDetailStoreId',
            url: 'GrupTransDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'grup_trans_id'},
                {name: 'member_id'},
                {name: 'nama'},
                {name: 'alamat'},
                {name: 'jml'},
                {name: 'loket_id'},
                {name: 'ref'},
                {name: 'nama_kat'}
            ]
        }, cfg));
    }
});
jun.rztGrupTransDetail = new jun.GrupTransDetailstore();
//jun.rztGrupTransDetail.load();
