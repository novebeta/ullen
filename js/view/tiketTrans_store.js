jun.TiketTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TiketTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TiketTransStoreId',
            url: 'TiketTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name:'tiket_trans_id'},
                {name:'loket_detil_id'},
                {name:'transfer_barang_detail_id'},
                {name:'tahun'},
                {name:'produk_id'},
                {name:'ref'}
            ]
        }, cfg));
    }
});
jun.rztTiketTrans = new jun.TiketTransstore();
//jun.rztTiketTrans.load();
