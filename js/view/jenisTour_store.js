jun.JenisTourstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.JenisTourstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JenisTourStoreId',
            url: 'JenisTour',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'jenis_tour_id'},
                {name: 'nama'},
                {name: 'free_ticket'},
                {name: 'min_cust'},
                {name: 'status'},
                {name: 'maks_grup'},
                {name: 'kode'}
            ]
        }, cfg));
    }
});
jun.rztJenisTour = new jun.JenisTourstore();
jun.rztJenisTourLib = new jun.JenisTourstore();
jun.rztJenisTourCmp = new jun.JenisTourstore();
jun.rztJenisTourLib.load();
