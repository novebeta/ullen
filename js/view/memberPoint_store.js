jun.MemberPointstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MemberPointstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MemberPointStoreId',
            url: 'MemberPoint',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'member_point_id'},
                {name: 'produk_id'},
                {name: 'member_kategori_id'},
                {name: 'point'}
            ]
        }, cfg));
    }
});
jun.rztMemberPoint = new jun.MemberPointstore();
//jun.rztMemberPoint.load();
