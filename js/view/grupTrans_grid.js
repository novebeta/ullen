jun.GrupTransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Setting Tour",
    id: 'docs-jun.GrupTransGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    params: null,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'no_urut',
            width: 100
        },
        {
            header: 'Guide',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Kategori',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_kat',
            width: 100
        },
        {
            header: 'Total Tamu',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_real',
            width: 100
        },
        {
            header: 'Perkiraan Masuk',
            sortable: true,
            resizable: true,
            dataIndex: 'waktu_masuk',
            width: 100
        },
        // {
        //     header: 'Jenis Tour',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'jenis_tour_id',
        //     width: 100,
        //     renderer: jun.renderJenisTour
        // },
        {
            header: 'Bahasa Tour',
            sortable: true,
            resizable: true,
            dataIndex: 'lang_tour_id',
            width: 100,
            renderer: jun.renderLangTour
        },
        {
            header: 'Maksimal',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100
        },
        {
            header: 'Start',
            sortable: true,
            resizable: true,
            dataIndex: 'start'
        },
        {
            header: 'End',
            sortable: true,
            resizable: true,
            dataIndex: 'end_',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztGrupTrans.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var btn = Ext.getCmp('toggleshowended');
                    b.params.showended = btn.pressed ? 1 : 0;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztGrupTrans;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Setting Grup',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Setting Grup',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Add Ticket',
                    ref: '../btnAddTicket'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Waiting',
                //     ref: '../btnWaiting'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Prepare',
                //     ref: '../btnPrepare'
                // },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Start',
                    ref: '../btnStart'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'End',
                    ref: '../btnEnd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Guide',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    text: 'Show Ended',
                    enableToggle: true,
                    id: 'toggleshowended',
                    ref: '../btnToggle',
                    // toggleHandler: onItemToggle,
                    pressed: false
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.GrupTransGrid.superclass.initComponent.call(this);
        // console.log(this.params.jenis);
        // this.title = this.params.jenis;
        if (jun.rztMemberCmp.getTotalCount() === 0) {
            jun.rztMemberCmp.load();
        }
        if (jun.rztLangTourCmp.getTotalCount() === 0) {
            jun.rztLangTourCmp.load();
        }
        if (jun.rztJenisTourCmp.getTotalCount() === 0) {
            jun.rztJenisTourCmp.load();
        }
        if (jun.rztKategoriCustCmp.getTotalCount() === 0) {
            jun.rztKategoriCustCmp.load();
        }
        if (jun.rztProdukCmp.getTotalCount() === 0) {
            jun.rztProdukCmp.load();
        }
        if (jun.rztGuideCmp.getTotalCount() === 0) {
            jun.rztGuideCmp.load();
        }
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnAddTicket.on('Click', this.addTicketForm, this);
        // this.btnWaiting.on('Click', this.waitingRec, this);
        // this.btnPrepare.on('Click', this.prepareRec, this);
        this.btnStart.on('Click', this.startRec, this);
        this.btnEnd.on('Click', this.endRec, this);
        this.btnPrint.on('Click', this.notaPrint, this);
        this.btnToggle.on('toggle', function () {
            jun.rztGrupTrans.reload();
        }, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        setTimeout(function () {
            jun.rztGrupTrans.load();
        }, 500);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    notaPrint: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih nota.");
            return;
        }
        Ext.Ajax.request({
            url: 'GrupTrans/print/id/' + this.record.json.grup_trans_id,
            method: 'POST',
            success: function (f, a) {
                // jun.rztLoketView.reload();
                var response = Ext.decode(f.responseText);
                var __printDataTM82 = [
                    '\x1B' + '\x40', //initial
                    '\x1B' + '\x21' + '\x30', // em mode on
                    'No Tour ' + response.no_urut,
                    '\x0A',
                    '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
                    '\x1D' + '\x21' + '\x11',
                    response.doublebaris,
                    '\x0A',
                    '\x0A',
                    '\x1B' + '\x21' + '\x01',
                    '\x1B' + '\x21' + '\x20',
                    response.msg + '\x0A',
                    '\x0A',
                    '\x1B' + '\x21' + '\x00',
                    // '\x1B' + '\x21' + '\x20',
                    response.addinfo + '\x0A',
                    '\x0A',
                    '\x0A',
                    '\x1B' + '\x21' + '\x00',
                    response.note
                ];
                var printData = __printDataTM82.concat(__feedPaper, __cutPaper);
                printRawQZ('ET', printData);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: 'Nota sedang di print...',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadForm: function () {
        var form = new jun.GrupTransWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.grup_trans_id;
        var form = new jun.GrupTransWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    addTicketForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih grup");
            return;
        }
        var idz = selectedz.json.grup_trans_id;
        var form = new jun.GrupTransDetailWin({modez: 1, id: idz});
        form.show(this);
        // form.formz.getForm().loadRecord(this.record);
        jun.rztGrupTransDetail.load({
            params: {
                grup_trans_id: idz
            }
        });
    },
    waitingRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin mengganti status jadi waiting?', this.waitingRecYes, this);
    },
    waitingRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (record.data.guide_id == null) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Guide");
            return;
        }
        Ext.Ajax.request({
            url: 'GrupTrans/waiting/id/' + record.json.grup_trans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztGuide.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    prepareRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin mengganti status jadi prepare?', this.prepareRecYes, this);
    },
    prepareRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (record.data.waiting == null) {
            Ext.MessageBox.alert("Warning", "Grup belum dalam status waiting");
            return;
        }
        Ext.Ajax.request({
            url: 'GrupTrans/prepare/id/' + record.json.grup_trans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztGuide.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    startRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin mengganti status jadi start?', this.startRecYes, this);
    },
    startRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        // if (record.data.prepare == null) {
        //     Ext.MessageBox.alert("Warning", "Grup belum dalam status prepare");
        //     return;
        // }
        Ext.Ajax.request({
            url: 'GrupTrans/start/id/' + record.json.grup_trans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztGrupTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    endRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin mengganti status jadi start?', this.endRecYes, this);
    },
    endRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (record.data.start == null) {
            Ext.MessageBox.alert("Warning", "Grup belum dalam status start");
            return;
        }
        Ext.Ajax.request({
            url: 'GrupTrans/end/id/' + record.json.grup_trans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztGrupTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.VorstenlandenAdultGrid = Ext.extend(jun.GrupTransGrid,{title: 'Vorstenlanden Adult',jenis:'VA'});
jun.VorstenlandenChildGrid = Ext.extend(jun.GrupTransGrid,{title: 'Vorstenlanden Child',jenis:'VC'});
jun.RegularAdultGrid = Ext.extend(jun.GrupTransGrid,{title: 'Regular Adult',jenis:'RA'});
jun.RegularChildGrid = Ext.extend(jun.GrupTransGrid,{title: 'Regular Child',jenis:'RC'});
jun.RegularFreeGrid = Ext.extend(jun.GrupTransGrid,{title: 'Regular Free',jenis:'RF'});
jun.ExclusiveAdultGrid = Ext.extend(jun.GrupTransGrid,{title: 'Exclusive Adult',jenis:'EA'});
jun.ExclusiveChildGrid = Ext.extend(jun.GrupTransGrid,{title: 'Exclusive Child',jenis:'EC'});
jun.AntrianGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Setting Grup",
    id: 'docs-jun.AntrianGrid',
    // iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    // sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Grup',
            xtype: 'templatecolumn',
            height: '24%',
            // dataIndex: 'nama_grup',
            tpl: '<section class="parent">' +
                '    <div class="child grup"><span class="grid-tour">Tour {no_urut}</span></br>' +
                '<span class="grid-jenis">{nama_tour}</span></div>' +
                '</section>'
        }
    ],
    initComponent: function () {
        this.store = jun.rztAntrian;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    hidden: true,
                    pageSize: 4
                }]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.AntrianGrid.superclass.initComponent.call(this);
        // this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    // afterrender: function (grid) {
    //     var viewEl = grid.getView().getEl();
    //     if (viewEl.getStyle('overflowY') === 'hidden') {
    //         viewEl.on('mousewheel', function (e) {
    //             viewEl.setScrollTop(viewEl.getScrollTop() + e.browserEvent.deltaY);
    //         });
    //     }
    // },
    // listeners: {
    //     afterrender: function (me) {
    //         var viewEl = me.getView().getEl();
    //         viewEl.on('mousewheel', function (e) {
    //             viewEl.setScrollTop(viewEl.getScrollTop() + e.browserEvent.deltaY);
    //         });
    //     }
    // }
});

