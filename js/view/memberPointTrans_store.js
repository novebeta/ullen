jun.MemberPointTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MemberPointTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MemberPointTransStoreId',
            url: 'MemberPointTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'member_point_trans_id'},
                {name: 'ref'},
                {name: 'tgl'},
                {name: 'trans_no'},
                {name: 'trans_tipe'},
                {name: 'point'}
            ]
        }, cfg));
    }
});
jun.rztMemberPointTrans = new jun.MemberPointTransstore();
//jun.rztMemberPointTrans.load();
