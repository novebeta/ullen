jun.MemberKategoristore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MemberKategoristore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MemberKategoriStoreId',
            url: 'MemberKategori',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'member_kategori_id'},
                {name: 'nama'}
            ]
        }, cfg));
    }
});
jun.rztMemberKategori = new jun.MemberKategoristore();
jun.rztMemberKategoriLib = new jun.MemberKategoristore();
jun.rztMemberKategoriCmp = new jun.MemberKategoristore();
jun.rztMemberKategoriLib.load();
