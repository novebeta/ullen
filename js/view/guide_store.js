jun.Guidestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Guidestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GuideStoreId',
            url: 'Guide',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'guide_id'},
                {name: 'nama'},
                {name: 'alamat'},
                {name: 'email'},
                {name: 'status'},
                {name: 'no_telp'},
                {name: 'ktp'},
                {name: 'note'},
                {name: 'tgl_lahir'}
            ]
        }, cfg));
    }
});
jun.rztGuide = new jun.Guidestore();
jun.rztGuideCmp = new jun.Guidestore();
jun.rztGuideLib = new jun.Guidestore();
jun.rztGuideLib.load();
