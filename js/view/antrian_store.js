jun.Antrianstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Antrianstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AntrianStoreId',
            url: 'site/AntrianIndex',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'no_urut'},
                {name: 'nama_tour'},
                {name: 'grup_trans_id'}
            ]
        }, cfg));
    }
});
jun.rztAntrian = new jun.Antrianstore();
//jun.rztAntrian.load();
